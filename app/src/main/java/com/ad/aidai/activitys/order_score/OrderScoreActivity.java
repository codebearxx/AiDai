package com.ad.aidai.activitys.order_score;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseActivity;
import com.ad.aidai.common.widgets.imageview.CircleImageView;
import com.ad.aidai.common.widgets.loading_dialog.LoadingDialog;
import com.ad.aidai.common.widgets.shine_button.ShineButton;
import com.bumptech.glide.Glide;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideTopExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 评分页面
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public class OrderScoreActivity extends BaseActivity implements OrderScoreContract.View {

    @BindView(R.id.civ_user_head)
    CircleImageView civUserHead;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_sex)
    ImageView ivSex;
    @BindView(R.id.tv_order_content)
    TextView tvOrderContent;
    @BindViews({R.id.shineBtn_order_start_1, R.id.shineBtn_order_start_2, R.id.shineBtn_order_start_3, R.id
            .shineBtn_order_start_4, R.id.shineBtn_order_start_5})
    ShineButton[] shineBtnOrderStart;
    @BindViews({R.id.shineBtn_fast_start_1, R.id.shineBtn_fast_start_2, R.id.shineBtn_fast_start_3, R.id
            .shineBtn_fast_start_4, R.id.shineBtn_fast_start_5})
    ShineButton[] shineBtnFastStart;
    @BindViews({R.id.shineBtn_attitude_start_1, R.id.shineBtn_attitude_start_2, R.id.shineBtn_attitude_start_3, R.id
            .shineBtn_attitude_start_4, R.id.shineBtn_attitude_start_5})
    ShineButton[] shineBtnAttitudeStart;
    @BindViews({R.id.tv_score_tip1, R.id.tv_score_tip2, R.id.tv_score_tip3})
    TextView[] tvScoreTips;
    @BindViews({R.id.tv_score_name1, R.id.tv_score_name2, R.id.tv_score_name3})
    TextView[] tvScoreNames;
    @BindView(R.id.tv_score_who)
    TextView tvScoreWho;

    private OrderScoreContract.Presenter mPresenter;

    private boolean clickStar = false;

    /**
     * 三个评分各自星星个数
     */
    private int[] score = {0, 0, 0};

    private NormalDialog dialog;
    private BaseAnimatorSet mBasIn;
    private BaseAnimatorSet mBasOut;

    private boolean[] is = {true, true, true};

    private LoadingDialog loadingDialog;

    private String[] tips = {"非常糟糕", "糟糕", "一般", "不错", "非常不错"};

    private String[][] names = {{"订单评分", "对方信誉", "酬劳合理度"}, {"订单评分", "完成速度", "服务态度"}};

    private String[] who = {"接单人评分", "发单人评分"};

    @Override
    protected void initView() {

        setContentView(R.layout.activity_order_score);
        ButterKnife.bind(this);

        for (int i = 0; i < 5; ++i) {
            shineBtnOrderStart[i].init(this);
            shineBtnFastStart[i].init(this);
            shineBtnAttitudeStart[i].init(this);
        }
        for (int i = 0; i < 5; ++i) {
            shineBtnOrderStart[i].setOnCheckStateChangeListener(new ShineButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(View view, boolean checked) {
                    if (is[0]) {
                        clickStar = true;
                        is[0] = false;
                        for (int j = 0; j < 5; ++j) {
                            if (view.getId() == shineBtnOrderStart[j].getId()) {
                                score[0] = j + 1;
                            }
                            shineBtnOrderStart[j].setChecked(false, true);
                        }
                        for (int j = 0; j < score[0]; ++j) {
                            shineBtnOrderStart[j].setChecked(true, true);
                        }
                        tvScoreTips[0].setText(tips[score[0] - 1]);
                        is[0] = true;
                    }
                }
            });
            shineBtnFastStart[i].setOnCheckStateChangeListener(new ShineButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(View view, boolean checked) {
                    if (is[1]) {
                        clickStar = true;
                        is[1] = false;
                        for (int j = 0; j < 5; ++j) {
                            if (view.getId() == shineBtnFastStart[j].getId()) {
                                score[1] = j + 1;
                            }
                            shineBtnFastStart[j].setChecked(false, true);
                        }
                        for (int j = 0; j < score[1]; ++j) {
                            shineBtnFastStart[j].setChecked(true, true);
                        }
                        tvScoreTips[1].setText(tips[score[1] - 1]);
                        is[1] = true;
                    }
                }
            });
            shineBtnAttitudeStart[i].setOnCheckStateChangeListener(new ShineButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(View view, boolean checked) {
                    if (is[2]) {
                        clickStar = true;
                        is[2] = false;
                        for (int j = 0; j < 5; ++j) {
                            if (view.getId() == shineBtnAttitudeStart[j].getId()) {
                                score[2] = j + 1;
                            }
                            shineBtnAttitudeStart[j].setChecked(false, true);
                        }
                        for (int j = 0; j < score[2]; ++j) {
                            shineBtnAttitudeStart[j].setChecked(true, true);
                        }
                        tvScoreTips[2].setText(tips[score[2] - 1]);
                        is[2] = true;
                    }
                }
            });
        }
    }

    @Override
    protected void initData() {
        new OrderScorePresenter(this, this).subscribe();
        mBasIn = new BounceTopEnter();
        mBasOut = new SlideTopExit();

        mPresenter.loadUser();
        mPresenter.loadOrder();

        int scorer = mPresenter.getScorer();
        for (int i = 0; i < 3; ++i) {
            tvScoreNames[i].setText(names[scorer][i]);
        }
        tvScoreWho.setText(who[scorer]);
    }

    @Override
    public void setPresenter(OrderScoreContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back, R.id.btn_submit_score})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                back();
                break;
            case R.id.btn_submit_score:
                mPresenter.submit();
                break;
        }
    }

    @Override
    public void showUserHead(String path) {
        Glide.with(this)
                .load(path)
                .placeholder(R.mipmap.default_image)
                .dontAnimate()
                .into(civUserHead);
    }

    @Override
    public void showUserName(String name) {
        tvName.setText(name);
    }

    @Override
    public void showUserSex(int sex) {
        ivSex.setImageResource(sex == 0 ? R.mipmap.icon_male : R.mipmap.icon_female);
    }

    @Override
    public void showOrderContent(String orderContent) {
        tvOrderContent.setText(orderContent);
    }

    @Override
    public int[] getScore() {
        return score;
    }

    @Override
    public void showLoading() {
        //显示加载对话框
        loadingDialog = new LoadingDialog(this);
        loadingDialog.setLoadingText("评分中...")
                .setFailedText("评分失败")
                .setSuccessText("评分成功")
                .show();
    }

    @Override
    public void closeLoading(boolean success) {
        if (loadingDialog != null) {
            if (success) {
                loadingDialog.loadSuccess();
            } else {
                loadingDialog.loadFailed();
            }
        }
    }

    @Override
    public void exit() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (loadingDialog != null && loadingDialog.isShowing()) {

                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        OrderScoreActivity.this.finish();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        back();
    }

    private void back() {
        if (!clickStar) {
            OrderScoreActivity.this.finish();
            return;
        }
        if (dialog == null) {
            dialog = new NormalDialog(this);
            dialog.content("评分未完成，确认离开？")
                    .style(NormalDialog.STYLE_TWO)
                    .titleTextSize(20)
                    .btnText("确定", "取消")
                    .showAnim(mBasIn)
                    .dismissAnim(mBasOut);
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while (dialog.isShowing()) {
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        OrderScoreActivity.this.finish();
                                    }
                                });
                            }
                        }).start();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                });
    }
}
