package com.ad.aidai.activitys.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseActivity;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.fragments.look_profile.LookProfileFragment;
import com.orhanobut.logger.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * 聊天界面
 * <p>
 * Created by Yusxon on 16/12/22.
 */

public class ConversationActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;

    private String title;

    @Override
    protected void initView() {
        setContentView(R.layout.conversation);
        ButterKnife.bind(this);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        title = intent.getData().getQueryParameter("title");

        tvTitle.setText(title);


    }

    @OnClick(R.id.iv_back)
    public void onClick() {
        ConversationActivity.this.finish();
    }

    private void clickHead(long userId) {

        try {
            LookProfileFragment fragment = new LookProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("show", true);
            bundle.putBoolean("showButton", false);
            bundle.putLong("userId", userId);
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                            .fragment_slide_left_exit)
                    .add(android.R.id.content, fragment, "lookProfile")
                    .addToBackStack("lookProfile")
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void subscribeEvents() {
        //订阅事件
        addSubscription(RxBus.getInstance()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        Logger.i("----");
                        if (o instanceof CommonEvent) {
                            CommonEvent event = (CommonEvent) o;
                            switch (event.getEvent()) {
                                case CommonEvent.USER_CLICK_HEAD_IN_CHAT:
                                    clickHead((long) event.getData());
                                    break;
                            }
                        }
                    }
                })
                .subscribe(RxBus.defaultSubscriber()));
    }
}
