package com.ad.aidai.activitys.main;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * MainContract
 * <p>
 * Created by Yusxon on 17/2/28.
 */

public interface MainContract {

    interface View extends BaseView<Presenter> {
        /**
         * 显示更新提示框
         *
         * @param version
         * @param updateMessage
         */
        void showUpdateDialog(String version, String updateMessage);
    }

    interface Presenter extends BasePresenter {
        /**
         * 加载一些数据
         */
        void loadData();

        /**
         * 更新app
         */
        void updateApp();
    }
}
