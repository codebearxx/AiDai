package com.ad.aidai.activitys.login;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * LoginContract
 * <p>
 * Created by Yusxon on 16/11/27.
 */

public interface LoginContract {

    interface View extends BaseView<Presenter> {
        /**
         * 显示手机号码
         *
         * @param phoneNumber 手机号码
         */
        void setPhoneNumber(String phoneNumber);

        /**
         * 显示密码
         *
         * @param password 密码
         */
        void setPassword(String password);

        /**
         * 显示提示信息
         *
         * @param message 要显示的信息
         */
        void toast(String message);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 登录成功
         */
        void loginSuccess();
    }

    interface Presenter extends BasePresenter {
        /**
         * 登录
         *
         * @param phoneNumber 手机号码
         * @param password    密码
         */
        void login(String phoneNumber, String password);

        /**
         * 加载手机号码
         * 进入登录界面先调用该方法,如果有用户手机号码信息,则会自动加载到输入框中
         */
        void loadPhone();

    }

}
