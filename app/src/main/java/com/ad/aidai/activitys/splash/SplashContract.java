package com.ad.aidai.activitys.splash;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * SplashContract
 * <p>
 * Created by Yusxon on 17/2/18.
 */

public interface SplashContract {

    interface View extends BaseView<Presenter> {

        /**
         * 跳转到下一个界面
         *
         * @param ui
         */
        void goNextUI(SplashActivity.UI ui);
    }

    interface Presenter extends BasePresenter {

        /**
         * 显示界面
         */
        void loadUI();

        /**
         * 获取加载时间
         *
         * @return
         */
        long getLoadTime();
    }
}
