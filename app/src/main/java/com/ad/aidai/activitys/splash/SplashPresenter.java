package com.ad.aidai.activitys.splash;

import android.content.Context;
import android.content.SharedPreferences;

import com.ad.aidai.bean.UserWithSchool;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.LoginResultFromNet;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.content.Context.MODE_PRIVATE;

/**
 * SplashPresenter
 * <p>
 * Created by Yusxon on 17/2/18.
 */

public class SplashPresenter implements SplashContract.Presenter {

    private Context mContext;
    private SplashContract.View mView;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences aidaiSP;

    private long startTime;

    public SplashPresenter(Context context, SplashContract.View view) {
        startTime = System.currentTimeMillis();
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        aidaiSP = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void loadUI() {
        if (aidaiSP.getBoolean("IS_FIRST_IN", true)) {
            aidaiSP.edit()
                    .putBoolean("IS_FIRST_IN", false)
                    .apply();
//            mView.goNextUI(SplashActivity.UI.GUIDE);
            mView.goNextUI(SplashActivity.UI.LOGIN);
        } else {
            if (aidaiSP.getBoolean("IS_LOGIN", false)) {
                login();
            } else {
                mView.goNextUI(SplashActivity.UI.LOGIN);
            }
        }
    }

    private void login() {
        String phoneNumber = aidaiSP.getString("USER_PHONE", "");
        String password = aidaiSP.getString("USER_PASSWORD", "");
        NetWork.getAccountOperationApi()
                .login(phoneNumber, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LoginResultFromNet>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("LoginPresenter").e(e.getMessage());
                        mView.goNextUI(SplashActivity.UI.HOME);
                    }

                    @Override
                    public void onNext(LoginResultFromNet loginResultFromNet) {
                        switch (loginResultFromNet.getCode()) {
                            case StatusCodeFromNet.CODE_SESSIONID_ERROR:
                            case StatusCodeFromNet.CODE_PHONE_ERROR:
                            case StatusCodeFromNet.CODE_PASSWORD_ERROR:
                                mView.goNextUI(SplashActivity.UI.LOGIN);
                                break;
                            case StatusCodeFromNet.CODE_SUCCESS:
                                UserWithSchool user = loginResultFromNet.getData();
                                user.setSchoolName();
                                MyUserDao.getUserDao().save(user);
                                ADApp.setUser();
                                Logger.t("SplashPresenter").i(user.toString());
                            default:
                                mView.goNextUI(SplashActivity.UI.HOME);
                                break;

                        }
                    }
                });
    }

    @Override
    public long getLoadTime() {
        return System.currentTimeMillis() - startTime;
    }
}
