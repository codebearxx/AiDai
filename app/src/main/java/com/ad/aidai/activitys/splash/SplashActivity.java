package com.ad.aidai.activitys.splash;

import android.content.Intent;

import com.ad.aidai.R;
import com.ad.aidai.activitys.login.LoginActivity;
import com.ad.aidai.activitys.main.MainActivity;
import com.ad.aidai.base.BaseActivity;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.fragments.guide.GuideFragment;

import java.util.Timer;
import java.util.TimerTask;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * 启动页面
 * <p>
 * Created by Yusxon on 16/12/26.
 */

public class SplashActivity extends BaseActivity implements SplashContract.View {

    private SplashContract.Presenter mPresenter;

    enum UI {
        /**
         * 主页
         */
        HOME,
        /**
         * 导航页
         */
        GUIDE,
        /**
         * 登录页
         */
        LOGIN
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_welcome);
    }

    @Override
    protected void initData() {
        new SplashPresenter(this, this).subscribe();
        ADApp.init();
        mPresenter.loadUI();
    }

    private void goHome() {
        Intent in = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(in);
        finish();
        overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
    }

    private void goGuide() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(android.R.id.content, new GuideFragment())
                .commit();
    }

    private void goLogin() {
        Intent in = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(in);
        finish();
        overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
    }

    @Override
    public void goNextUI(final UI ui) {
        long times = 800 - mPresenter.getLoadTime();
        if(times < 0) {
            times = 0;
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (ui) {
                            case HOME:
                                goHome();
                                break;
                            case GUIDE:
                                //                goGuide();
                                goLogin();
                                break;
                            case LOGIN:
                                goLogin();
                                break;
                            default:
                                break;
                        }
                    }
                });
            }
        }, times);

    }

    @Override
    protected void subscribeEvents() {
        addSubscription(RxBus.getInstance()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        if (o instanceof CommonEvent && ((CommonEvent) o).getEvent().equals(CommonEvent
                                .GUIDE_BUTTON_CLICK)) {
                            goLogin();
                        }
                    }
                })
                .subscribe(RxBus.defaultSubscriber())
        );
    }

    @Override
    public void setPresenter(SplashContract.Presenter presenter) {
        this.mPresenter = presenter;
    }
}
