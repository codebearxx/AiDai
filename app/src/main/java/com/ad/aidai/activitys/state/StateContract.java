package com.ad.aidai.activitys.state;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

import java.util.List;

/**
 * StateContract
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public interface StateContract {

    interface View extends BaseView<Presenter> {
        /**
         * 显示订单类型
         *
         * @param orderType
         */
        void showOrderType(String orderType);

        /**
         * 显示订单时间
         *
         * @param orderTime
         */
        void showOrderTime(String orderTime);

        /**
         * 显示订单状态
         *
         * @param orderStatus
         */
        void showOrderStatus(String orderStatus);

        /**
         * 显示订单内容
         *
         * @param orderContent
         */
        void showOrderContent(String orderContent);

        /**
         * 显示订单酬劳
         *
         * @param orderMoney
         */
        void showOrderMoney(String orderMoney);

        /**
         * 显示附带图片
         *
         * @param urls
         */
        void showStateImage(String... urls);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 获取申述内容
         *
         * @return
         */
        String getStateContent();

        /**
         * 退出界面
         */
        void exit();
    }

    interface Presenter extends BasePresenter {
        /**
         * 加载订单信息
         */
        void loadOrder();

        /**
         * 删除图片
         *
         * @param position
         */
        void deleteImage(int position);

        /**
         * 添加图片
         *
         * @param paths
         */
        void addImage(List<String> paths);

        /**
         * 获取已显示的图片数量
         *
         * @return
         */
        int getImageCount();

        /**
         * 获取点击的图片的url
         *
         * @param position
         * @return
         */
        String getImageUrl(int position);

        /**
         * 提交订单
         */
        void submit();
    }
}
