package com.ad.aidai.activitys.order_score;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * StateContract
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public interface OrderScoreContract {

    interface View extends BaseView<Presenter> {
        /**
         * 显示用户头像
         *
         * @param path
         */
        void showUserHead(String path);

        /**
         * 显示用户昵称
         *
         * @param name
         */
        void showUserName(String name);

        /**
         * 显示用户性别
         *
         * @param sex
         */
        void showUserSex(int sex);

        /**
         * 显示订单内容
         *
         * @param orderContent
         */
        void showOrderContent(String orderContent);

        /**
         * 获取评分
         *
         * @return
         */
        int[] getScore();

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 退出界面
         */
        void exit();
    }

    interface Presenter extends BasePresenter {
        /**
         * 加载用户信息
         */
        void loadUser();

        /**
         * 加载订单内容
         */
        void loadOrder();

        /**
         * 提交评分
         */
        void submit();

        /**
         * 获取评分者
         * 0:发单人评分
         * 1:接单人评分
         *
         * @return
         */
        int getScorer();
    }
}
