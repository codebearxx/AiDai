package com.ad.aidai.activitys.feedback;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

import java.util.List;

/**
 * Created by pure on 2017/2/14.
 */

public interface FeedbackContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示附带图片
         *
         * @param urls
         */
        void showFeedbackImage(String... urls);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 关闭Activity
         */
        void closeActivity();
    }

    interface Presenter extends BasePresenter {

        /**
         * 获取添加的图片url列表
         */
        List<String> getAddedImage();

        /**
         * 提交反馈内容
         */
        void submit(String etFeedbackContent,String etContact,List<String> imageUrlsList);

        /**
         * 删除图片
         *
         * @param position
         */
        void deleteImage(int position);

        /**
         * 添加图片
         *
         * @param paths
         */
        void addImage(List<String> paths);

        /**
         * 获取已显示的图片数量
         *
         * @return
         */
        int getImageCount();

        /**
         * 获取点击的图片的url
         *
         * @param position
         * @return
         */
        String getImageUrl(int position);

    }
}
