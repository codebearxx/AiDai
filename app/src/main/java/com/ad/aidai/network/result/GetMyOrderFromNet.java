package com.ad.aidai.network.result;

import com.ad.aidai.bean.Order;

import java.util.List;

/**
 * 查询我的订单返回的结果
 * <p>
 * Created by Yusxon on 16/12/7.
 */

public class GetMyOrderFromNet extends NetResultWithData<List<Order>> {


    public GetMyOrderFromNet(int code, List<Order> data) {
        super(code, data);
    }
}
