package com.ad.aidai.network.api;

import com.ad.aidai.bean.UserWithSchool;
import com.ad.aidai.network.ADUrl;
import com.ad.aidai.network.result.BindSchoolFromNet;
import com.ad.aidai.network.result.GetMyOrderFromNet;
import com.ad.aidai.network.result.GetMyStateFromNet;
import com.ad.aidai.network.result.GetSchoolFromNet;
import com.ad.aidai.network.result.LoginResultFromNet;
import com.ad.aidai.network.result.NetResult;
import com.ad.aidai.network.result.NetResultWithData;
import com.ad.aidai.network.result.RegisterResultFromNet;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * 用户操作接口
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public interface AccountOperationApi {

    /**
     * 登录
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.LOGIN_URL)
    Observable<LoginResultFromNet> login(@Field("tel") String phoneNumber, @Field("password") String password);

    /**
     * 注册
     *
     * @return
     */
    @GET(ADUrl.REGISTER_URL)
    Observable<RegisterResultFromNet> register(@Query("userTel") String phoneNumber, @Query("userPassword") String
            password, @Query("checkcode") String verficationCode);

    /**
     * 忘记密码
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.FORGET_PASSWORD)
    Observable<NetResultWithData> forgetPassword(@Field("userTel") String userTel,
                                                 @Field("checkCode") String checkCode);

    /**
     * 重置密码
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.RESET_PASSWORD)
    Observable<NetResult> resetPassword(@Field("userTel") String userTel,
                                        @Field("userPassword") String userPassword,
                                        @Field("token") String token);

    /**
     * 修改用户昵称
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.UPDATE_USER_NAME)
    Observable<NetResult> updateUserName(@Field("userId") long userId, @Field("userName") String userName);

    /**
     * 修改用户性别
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.UPDATE_USER_SEX)
    Observable<NetResult> updateUserSex(@Field("userId") long userId, @Field("userSex") int userSex);

    /**
     * 绑定学校
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.BIND_SCHOOL)
    Observable<BindSchoolFromNet> bindSchool(@Field("userId") long userId,
                                             @Field("userStuNumber") String userStuNumber,
                                             @Field("userPassword") String userPassword,
                                             @Field("schoolId") int schoolId);

    /**
     * 查看个人资料
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.LOOK_PROFILE)
    Observable<NetResultWithData<UserWithSchool>> lookProfile(@Field("userId") long userId);

    /**
     * 获取学校
     *
     * @return
     */
    @GET(ADUrl.GET_SCHOOL)
    Observable<GetSchoolFromNet> getSchool();

    /**
     * 修改用户支付宝
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.UPDATE_USER_PAY_NUM)
    Observable<NetResult> updateUserPayNum(@Field("userId") long userId,
                                           @Field("userPayNum") String userPayNum);

    /**
     * 修改用户个性签名
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.UPDATE_USER_PERSONAL_NOTE)
    Observable<NetResult> updateUserPersonalNote(@Field("userId") long userId,
                                                 @Field("userPersonalNote") String userPersonalNote);

    /**
     * 获取我发的单
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_MY_SEND_ORDER_URL)
    Observable<GetMyOrderFromNet> getMySendOrder(@Field("userId") long userId, @Field("pageSize") int pageSize,
                                                 @Field("currentPage") int currentPage);

    /**
     * 获取我发的单
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_MY_RECEIVE_ORDER_URL)
    Observable<GetMyOrderFromNet> getMyReceiveOrder(@Field("userId") long userId, @Field("pageSize") int pageSize,
                                                    @Field("currentPage") int currPage);

    /**
     * 修改用户头像
     *
     * @param maps
     * @param file
     * @return
     */
    @Multipart
    @POST(ADUrl.UPDATE_USER_INFOS_URL)
    Observable<NetResult> updateUserIcon(@QueryMap Map<String, String> maps, @Part MultipartBody.Part file);


    /**
     * 获取申诉单
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.SELECT_STATE_BY_USERID_URL)
    Observable<GetMyStateFromNet> getMyStateOrder(@Field("userId") long userId);

    /**
     * 提交反馈接口(图文)
     *
     * @param maps
     * @param parts
     * @return
     */
    @Multipart
    @POST(ADUrl.FEEDBACK_IMAGE_URL)
    Observable<NetResult> submitFeedbackImage(@Part MultipartBody.Part[] parts,
                                              @PartMap Map<String, RequestBody> maps);

    /**
     * 提交反馈接口
     *
     * @param maps
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.FEEDBACK_URL)
    Observable<NetResult> submitFeedback(@FieldMap Map<String, String> maps);


    /**
     * 获取用户积分
     * @param userId
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_USER_CREDITS_URL)
    Observable<NetResultWithData<Integer>> getUserCredits(@Field("userId") long userId);

}
