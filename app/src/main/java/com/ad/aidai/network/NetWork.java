package com.ad.aidai.network;

import com.ad.aidai.common.ADApp;
import com.ad.aidai.network.api.AccountOperationApi;
import com.ad.aidai.network.api.AppApi;
import com.ad.aidai.network.api.OrderOperationApi;
import com.ad.aidai.network.cookies.CookieManger;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * 网络请求操作
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public class NetWork {

    private static OkHttpClient okHttpClient;
    /**
     * 用户将获取的数据自动转为json格式
     */
    private static Converter.Factory gsonConverterFactory = GsonConverterFactory.create();
    /**
     * 用户接收字符串格式数据(不自动转为json数据)
     */
    private static ScalarsConverterFactory scalarsConverterFactory = ScalarsConverterFactory.create();
    /**
     * rxjava
     */
    private static CallAdapter.Factory rxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create();

    /**
     * 用户操作接口
     */
    private static AccountOperationApi accountOperationApi = null;

    /**
     * 订单操作接口
     */
    private static OrderOperationApi orderOperationApi = null;

    /**
     * app相关操作接口
     */
    private static AppApi appApi = null;


    /**
     * 用户操作(登录、注册、修改密码)
     *
     * @return 用户操作的接口
     */
    public static AccountOperationApi getAccountOperationApi() {

        if (accountOperationApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(getOkHttpClient())
                    .baseUrl(ADUrl.BASE_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(rxJavaCallAdapterFactory)
                    .build();
            accountOperationApi = retrofit.create(AccountOperationApi.class);
        }

        return accountOperationApi;
    }

    /**
     * 订单操作
     *
     * @return 订单操作接口
     */
    public static OrderOperationApi getOrderOperationApi() {

        if (orderOperationApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(getOkHttpClient())
                    .baseUrl(ADUrl.BASE_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(rxJavaCallAdapterFactory)
                    .build();
            orderOperationApi = retrofit.create(OrderOperationApi.class);
        }

        return orderOperationApi;
    }

    /**
     * app操作
     *
     * @return
     */
    public static AppApi getAppApi() {

        if (appApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .client(getOkHttpClient())
                    .baseUrl(ADUrl.BASE_URL)
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(rxJavaCallAdapterFactory)
                    .build();
            appApi = retrofit.create(AppApi.class);
        }

        return appApi;
    }

    private static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            //日志显示级别
            HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
            //新建log拦截器
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor
                    .Logger() {
                @Override
                public void log(String message) {
                    if ((message.startsWith("-->") && !message.startsWith("--> END")) || (message.startsWith("<--")
                            && !message.startsWith("<-- END"))) {
                        System.out.println
                                ("╔════════════════════════════════════════════════════════════════════════════════════════");
                    }
                    System.out.println("║ " + message);
                    if (message.startsWith("--> END") || message.startsWith("<-- END")) {
                        System.out.println
                                ("╚════════════════════════════════════════════════════════════════════════════════════════");
                    }
                }

            });
            loggingInterceptor.setLevel(level);

            okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    //cookie管理
                    .cookieJar(new CookieManger(ADApp.getContext()))
                    //超时时间
                    .connectTimeout(10, TimeUnit.HOURS)
                    //超时读取时间
                    .readTimeout(10, TimeUnit.HOURS)
                    //超时写入时间
                    .writeTimeout(10, TimeUnit.HOURS)
                    .build();
        }

        return okHttpClient;
    }

}
