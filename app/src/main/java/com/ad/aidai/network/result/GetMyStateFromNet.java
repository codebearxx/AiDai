package com.ad.aidai.network.result;

import com.ad.aidai.bean.State;

import java.util.List;

/**
 * 查询我的申述单返回的结果
 * <p>
 * Created by Yusxon on 17/2/20.
 */

public class GetMyStateFromNet extends NetResultWithData<List<State>> {
    public GetMyStateFromNet(int code, List<State> data) {
        super(code, data);
    }
}
