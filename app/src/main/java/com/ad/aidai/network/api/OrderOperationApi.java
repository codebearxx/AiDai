package com.ad.aidai.network.api;

import com.ad.aidai.bean.Order;
import com.ad.aidai.bean.Place;
import com.ad.aidai.network.ADUrl;
import com.ad.aidai.network.result.GetOrderListFromNet;
import com.ad.aidai.network.result.NetResult;
import com.ad.aidai.network.result.NetResultWithData;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import rx.Observable;

/**
 * 订单操作接口
 * <p>
 * Created by Yusxon on 17/2/14.
 */

public interface OrderOperationApi {

    /**
     * 发送订单接口
     *
     * @param maps
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.SEND_ORDER_URL)
    Observable<NetResult> sendOrder(@FieldMap Map<String, String> maps);

    /**
     * 获取全校订单接口
     *
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_ALL_SCHOOL_ORDER_URL)
    Observable<GetOrderListFromNet> getAllSchoolOrder(@Field("pageSize") int pageSize, @Field("currentPage") int
            currentPage, @Field("schoolId") long schoolId);

    /**
     * 接单接口
     *
     * @param orderId        订单id
     * @param orderReceiveId 接单者id
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.ACCEPT_THE_ORDER_URL)
    Observable<NetResult> acceptOrder(@Field("orderId") long orderId, @Field("orderReceiveId") long orderReceiveId);

    /**
     * 撤销订单接口
     *
     * @param orderId 订单id
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.WITHDRAW_ORDER_URL)
    Observable<NetResult> withdrawOrder(@Field("orderId") long orderId);

    /**
     * 完成订单接口
     *
     * @param orderId 订单id
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.FINISH_ORDER_URL)
    Observable<NetResult> finishOrder(@Field("orderId") long orderId);


    /**
     * 评分接口
     *
     * @param maps
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.MUTUAL_COMMENT_URL)
    Observable<NetResult> mutualComment(@FieldMap Map<String, String> maps);


    /**
     * 提交申述订单接口(图文)
     *
     * @param maps
     * @param parts
     * @return
     */
    @Multipart
    @POST(ADUrl.SUBMIT_STATE_IMAGE_URL)
    Observable<NetResult> submitState(@PartMap Map<String, RequestBody> maps, @Part MultipartBody.Part[] parts);

    /**
     * 提交申述订单接口
     *
     * @param maps
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.SUBMIT_STATE_URL)
    Observable<NetResult> submitState(@FieldMap Map<String, String> maps);

    /**
     * 通过订单id获取订单详情
     *
     * @param orderId
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_ORDER_BY_ORDERID_URL)
    Observable<NetResultWithData<Order>> getOrderByOrderId(@Field("orderId") long orderId);

    /**
     * 获取附近订单
     *
     * @param maps
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_NEARBY_ORDERLIST_URL)
    Observable<GetOrderListFromNet> getNearByOrderList(@FieldMap Map<String, String> maps);

    /**
     * 获取推送订单
     *
     * @param maps
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.PUSH_ORDER_LIST_URL)
    Observable<GetOrderListFromNet> pushOrderList(@FieldMap Map<String, String> maps);


    /**
     * 通过学校id获取学校附近地址信息
     *
     * @param schoolId
     * @return
     */
    @FormUrlEncoded
    @POST(ADUrl.GET_PLACE_BY_SCHOOL_ID_URL)
    Observable<NetResultWithData<List<Place>>> getPlaceBySchoolId(@Field("schoolId") long schoolId);

}
