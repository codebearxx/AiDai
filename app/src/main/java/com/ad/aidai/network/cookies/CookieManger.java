package com.ad.aidai.network.cookies;

import android.content.Context;
import android.util.Log;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * CookieManger
 *
 * Created by Yusxon on 16/12/21.
 */

public class CookieManger implements CookieJar {

    private static PersistentCookieStore cookieStore;

    public CookieManger(Context context) {
        if (cookieStore == null ) {
            cookieStore = new PersistentCookieStore(context);
        }

    }

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        if (cookies != null && cookies.size() > 0) {
            for (Cookie item : cookies) {
                cookieStore.add(url, item);
                Log.i("cookie", item.value());
            }
        }
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        return cookieStore.get(url);
    }
}
