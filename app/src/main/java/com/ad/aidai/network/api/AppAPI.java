package com.ad.aidai.network.api;

import com.ad.aidai.bean.Version;
import com.ad.aidai.network.ADUrl;
import com.ad.aidai.network.result.NetResultWithData;

import retrofit2.http.GET;
import rx.Observable;

/**
 * app相关操作接口
 * <p>
 * Created by Yusxon on 17/2/22.
 */

public interface AppApi {

    /**
     * 获取app最新版本信息
     *
     * @return
     */
    @GET(ADUrl.GET_VERSION_URL)
    Observable<NetResultWithData<Version>> getVersion();
}
