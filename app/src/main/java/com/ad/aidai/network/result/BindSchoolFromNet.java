package com.ad.aidai.network.result;

import com.ad.aidai.bean.User;

/**
 * 绑定学校返回的结果
 * <p>
 * Created by pure on 2017/2/18.
 */

public class BindSchoolFromNet extends NetResultWithData<User> {

    public BindSchoolFromNet(int code, User data) {
        super(code, data);
    }
}
