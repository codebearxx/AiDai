package com.ad.aidai.network.result;

import com.ad.aidai.bean.School;

import java.util.List;

/**
 * 获取学校返回的结果
 * <p>
 * Created by pure on 2017/2/18.
 */

public class GetSchoolFromNet extends NetResultWithData<List<School>> {

    public GetSchoolFromNet(int code, List<School> data) {
        super(code, data);
    }
}
