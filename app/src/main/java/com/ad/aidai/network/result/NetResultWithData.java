package com.ad.aidai.network.result;

import com.google.gson.Gson;

/**
 * 网络请求返回的结果(返回码和返回数据)
 * <p>
 * Created by Yusxon on 16/12/7.
 */

public class NetResultWithData<T> extends NetResult {
    private T data;

    public NetResultWithData(int code, T data) {
        super(code);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
