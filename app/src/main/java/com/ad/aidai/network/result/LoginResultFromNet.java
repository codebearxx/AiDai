package com.ad.aidai.network.result;

import com.ad.aidai.bean.UserWithSchool;

/**
 * 登录返回的结果
 * <p>
 * Created by Yusxon on 16/12/7.
 */

public class LoginResultFromNet extends NetResultWithData<UserWithSchool> {

    public LoginResultFromNet(int code, UserWithSchool data) {
        super(code, data);
    }
}
