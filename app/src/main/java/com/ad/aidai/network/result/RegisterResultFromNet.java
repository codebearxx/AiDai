package com.ad.aidai.network.result;

import com.ad.aidai.bean.User;

/**
 * 注册返回的结果
 * <p>
 * Created by Yusxon on 16/12/7.
 */

public class RegisterResultFromNet extends NetResultWithData<User> {

    public RegisterResultFromNet(int code, User data) {
        super(code, data);
    }
}
