package com.ad.aidai.network.result;

import com.ad.aidai.bean.Order;

import java.util.List;

/**
 * 查询订单列表返回的结果
 * <p>
 * Created by Yusxon on 16/12/7.
 */

public class GetOrderListFromNet extends NetResultWithData<List<Order>> {

    public GetOrderListFromNet(int code, List<Order> data) {
        super(code, data);
    }
}
