package com.ad.aidai.network;

/**
 * 请求接口
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public class ADUrl {

    //    public static final String BASE_URL = "http://www.zjh.ac.cn/aidai/";
    //    public static final String BASE_URL = "http://rofe.tunnel.qydev.com/aidai/";
    //    public static final String BASE_URL = "http://lixifan.tunnel.qydev.com/aidai/";
    public static final String BASE_URL = "http://itrice.ac.cn/aidai/";
    /**
     * 头像接口
     */
    public static final String HEAD_ICON_URL = BASE_URL + "userIcon/{schoolId}/{photoName}";

    /**
     * 登录接口
     */
    public static final String LOGIN_URL = "userController/login";
    /**
     * 注册接口
     */
    public static final String REGISTER_URL = "userController/register";
    /**
     * 忘记密码
     */
    public static final String FORGET_PASSWORD = "userController/verifyToPass";
    /**
     * 重置密码
     */
    public static final String RESET_PASSWORD = "userController/updateUserPassword";
    /**
     * 修改用户昵称
     */
    public static final String UPDATE_USER_NAME = "userController/updateUserInfo";
    /**
     * 修改用户性别
     */
    public static final String UPDATE_USER_SEX = "userController/updateUserInfo";
    /**
     * 绑定教务系统
     */
    public static final String BIND_SCHOOL = "bindSystemController/bindmysise";
    /**
     * 查看个人资料
     */
    public static final String LOOK_PROFILE = "userController/getUserInfoById";
    /**
     * 获取学校
     */
    public static final String GET_SCHOOL = "schoolController/selectSchool";
    /**
     * 修改用户支付宝
     */
    public static final String UPDATE_USER_PAY_NUM = "userController/updateUserInfo";
    /**
     * 修改用户个性签名
     */
    public static final String UPDATE_USER_PERSONAL_NOTE = "userController/updateUserInfo";

    /**
     * 发单接口
     */
    public static final String SEND_ORDER_URL = "orderController/sendOrder";

    /**
     * 获取全校订单
     */
    public static final String GET_ALL_SCHOOL_ORDER_URL = "orderController/getSchoolOrderList";

    /**
     * 获取我发的单
     */
    public static final String GET_MY_SEND_ORDER_URL = "userController/getSendOrderListByUserId";

    /**
     * 获取我接的单
     */
    public static final String GET_MY_RECEIVE_ORDER_URL = "userController/getReceiveOrderListByUserId";

    /**
     * 修改用户信息，包括修改头像
     */
    public static final String UPDATE_USER_INFOS_URL = "userController/updateUserInfos";

    /**
     * 接单接口
     */
    public static final String ACCEPT_THE_ORDER_URL = "orderController/acceptTheOrder";

    /**
     * 撤单接口
     */
    public static final String WITHDRAW_ORDER_URL = "orderController/withdrawOrder";

    /**
     * 完成订单接口
     */
    public static final String FINISH_ORDER_URL = "orderController/finishOrder";

    /**
     * 评分接口
     */
    public static final String MUTUAL_COMMENT_URL = "userController/mutualComment";

    /**
     * 申述订单接口(有带图)
     */
    public static final String SUBMIT_STATE_IMAGE_URL = "stateController/submitState";

    /**
     * 申述订单接口(无带图)
     */
    public static final String SUBMIT_STATE_URL = "stateController/submitStateNoFile";

    /**
     * 获取我发的申述单
     */
    public static final String SELECT_STATE_BY_USERID_URL = "stateController/selectStateByUserId";

    /**
     * 申述单图片接口
     */
    public static final String STATE_IMAGE_URL = BASE_URL + "/stateImage/{photoName}";

    /**
     * 通过订单id获取订单详情
     */
    public static final String GET_ORDER_BY_ORDERID_URL = "orderController/getOrderByOrderId";

    /**
     * 获取附近订单
     */
    public static final String GET_NEARBY_ORDERLIST_URL = "orderController/getNearByOrderList";

    /**
     * 获取推送订单
     */
    public static final String PUSH_ORDER_LIST_URL = "orderController/pushOrdersList";

    /**
     * 获取app最新版本信息
     */
    public static final String GET_VERSION_URL = "versionController/getLastVersion";

    /**
     * 积分说明网址
     */
    public static final String STAR_EXPLAIN = "page/app/StarExplain.html";

    /**
     * 服务条款网址
     */
    public static final String SERVICE_ITEM = "page/app/ServiceItem.html";

    /**
     * 反馈接口(有带图)
     */
    public static final String FEEDBACK_IMAGE_URL = "feedbackController/addFeedback";

    /**
     * 反馈接口
     */
    public static final String FEEDBACK_URL = "feedbackController/addFeedbackNoFile";

    /**
     * 获取学校周边的地址
     */
    public static final String GET_PLACE_BY_SCHOOL_ID_URL = "orderController/getPlaceBySchoolId";

    /**
     * 获取用户最新积分
     */
    public static final String GET_USER_CREDITS_URL = "userController/selectUserCreditsByUserId";
}
