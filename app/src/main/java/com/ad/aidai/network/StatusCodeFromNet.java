package com.ad.aidai.network;

/**
 * 跟服务器请求返回的转态码
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public class StatusCodeFromNet {
    //-----------------------------success-------------------------------
    /**
     * 成功
     */
    public final static int CODE_SUCCESS = 100;

    //-------------------------------------------------------------------

    //-----------------------------error---------------------------------
    /**
     * 服务器异常
     */
    public final static int CODE_SERVER_EXCEPTION = 200;
    /**
     * 用户名错误或不存在
     */
    public final static int CODE_PHONE_ERROR = 201;
    /**
     * 密码错误
     */
    public final static int CODE_PASSWORD_ERROR = 202;
    /**
     * 更新数据失败
     */
    public final static int CODE_UPDATE_DATA_ERROR = 203;
    /**
     * 插入数据失败
     */
    public final static int CODE_INSERT_DATA_ERROR = 204;
    /**
     * 上传图片出错
     */
    public final static int CODE_UPLOAD_IMAGE_ERROR = 205;
    /**
     * sessionid过期
     */
    public final static int CODE_SESSIONID_ERROR = 206;
    /**
     * 订单已被接
     */
    public final static int CODE_ORDER_HAD_RECEIVER = 207;
    /**
     * 手机号已存在
     */
    public final static int CODE_PHONE_EXIST = 302;
    /**
     * 验证码错误
     */
    public final static int CODE_VC_ERROR = 305;
    /**
     * 绑定教务系统时，账号或密码错误
     */
    public final static int CODE_STU_ERROR = 307;
    /**
     * 数据库存在该用户
     */
    public final static int CODE_USER_EXIST_ERROR = 308;
    /**
     * 数据库不存在该用户
     */
    public final static int CODE_USER_NO_EXIST_ERROR = 309;
    /**
     * 上传图片失败
     */
    public final static int CODE_IMAGE_ERROT = 310;
    /**
     * 订单已经被接
     */
    public final static int CODE_ORDER_GOT_ERROR = 501;
    /**
     * 查不到此学校
     */
    public final static int CODE_SCHOOL_NO_SEARCH_ERROR = 505;

    /**
     * 该学生已经绑定
     */
    public final static int CODE_SCHOOL_HAVE_BIND = 208;

    /**
     * 已经申述过
     */
    public final static int CODE_HAD_STATE = 211;

    /**
     * 已经评分过
     */
    public final static int CODE_HAD_SCORE = 210;
    //-------------------------------------------------------------------
}
