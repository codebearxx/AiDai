package com.ad.aidai.network.result;

import com.google.gson.Gson;

/**
 * 网络请求返回的结果(只有返回码)
 * <p>
 * Created by pure on 2017/2/16.
 */

public class NetResult {
    private int code;

    public NetResult(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
