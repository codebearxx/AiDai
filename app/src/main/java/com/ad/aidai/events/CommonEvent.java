package com.ad.aidai.events;

/**
 * 多种事件,只是触发事件而不用发送信息，或发送单个信息的写在这里面就行
 * <p>
 * Created by Yusxon on 16/12/26.
 */

public class CommonEvent<T> {
    /**
     * 导航页面的按钮点击事件
     */
    public final static String GUIDE_BUTTON_CLICK = "guide_button_click";
    /**
     * 推送功能设置事件
     */
    public final static String PUSH_SETTING_CHANGE = "push_setting_change";
    /**
     * 定位功能设置事件
     */
    public final static String LOCATION_SETTING_CHANGE = "location_setting_change";
    /**
     * 绑定学校信息
     */
    public final static String SCHOOL_INFO_BIND = "school_info_bind";
    /**
     * 修改头像
     */
    public final static String USER_HEAD_CHANGE = "user_head_change";
    /**
     * 上传头像
     */
    public final static String USER_HEAD_UPLOAD = "user_head_upload";
    /**
     * 修改昵称
     */
    public final static String USER_NAME = "user_name";
    /**
     * 修改性别
     */
    public final static String USER_SEX = "user_sex";
    /**
     * 修改支付宝
     */
    public final static String USER_PAY_NUM = "user_pay_num";
    /**
     * 修改个性签名
     */
    public final static String USER_PERSONAL_NOTE = "user_personal_note";
    /**
     * 在绑定学校页面选中了学校
     */
    public final static String SELECT_SCHOOL_BIND = "select_school_bind";
    /**
     * 订单页面的滑动触摸事件
     */
    public final static String ON_TOUCH_ORDERFRAGMENT = "on_touch_orderFragment";
    /**
     * 注册结果事件
     */
    public final static String REGISTER_FINISHED = "register_finished";
    /**
     * 重置密码结果事件
     */
    public final static String RESET_PASSWORD_FINISHED = "reset_password_finished";
    /**
     * 重置密码后重置手机号结果事件
     */
    public final static String RESET_PHONE_FINISHED = "reset_phone_finished";
    /**
     * 创建订单退出事件
     */
    public final static String IS_EXIT_CREATE_ORDER = "is_exit_create_order";

    /**
     * 用户点击聊天界面里的头像
     */
    public final static String USER_CLICK_HEAD_IN_CHAT = "user_click_head_in_chat";

    /**
     * 订单状态改变
     */
    public final static String ORDER_STATUS_CHANGE = "order_status_change";

    /**
     * 用户更新积分
     */
    public final static String USER_UPDATE_CREDITS = "user_update_credits";

    private String event;

    private T data;

    public CommonEvent(String event) {
        this.event = event;
    }

    public CommonEvent(String event, T data) {
        this.event = event;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
