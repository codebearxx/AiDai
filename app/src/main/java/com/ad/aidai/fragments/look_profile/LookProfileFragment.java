package com.ad.aidai.fragments.look_profile;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.listener.RecyclerViewListener;
import com.ad.aidai.common.widgets.loading_dialog.LoadingDialog;
import com.ad.aidai.common.widgets.photoview.DragPhotoView;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideTopExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.jude.easyrecyclerview.decoration.DividerDecoration;
import com.orhanobut.logger.Logger;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.RongIM;

/**
 * 查看别人个人资料页面
 * <p>
 * Created by pure on 2017/1/12.
 */

public class LookProfileFragment extends BaseFragment implements LookProfileContract.View {

    @BindView(R.id.rlv_profile)
    RecyclerView rlvProfileBase;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    private LookProfileContract.Presenter mPresenter;
    private LookProfileAdapter mAdapter;
    private LoadingDialog loadingDialog;

    private BaseAnimatorSet mBasIn;
    private BaseAnimatorSet mBasOut;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_me_profile, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件
    }

    @Override
    protected void initView() {

        //创建默认的线性LayoutManager
        rlvProfileBase.setLayoutManager(new LinearLayoutManager(getActivity()));

        //设置分割线
        DividerDecoration itemDecoration = new DividerDecoration(Color.parseColor("#E6E6E6"), 3, 0, 0);
        itemDecoration.setDrawLastItem(true);
        rlvProfileBase.addItemDecoration(itemDecoration);

        mBasIn = new BounceTopEnter();
        mBasOut = new SlideTopExit();
    }

    @Override
    protected void initData() {
        new LookProfilePresenter(mActivity, this).subscribe();

        mAdapter = new LookProfileAdapter(mContext, mPresenter.getDatas());
        mPresenter.setAdapter();
        rlvProfileBase.setAdapter(mAdapter);
        listener();
        mPresenter.loadUserData();
    }

    private void listener() {
        mAdapter.setOnItemLongClickListener(new RecyclerViewListener.OnRecyclerViewItemLongClickListener() {
            @Override
            public void onItemLongClick(View view, int position) {
                mPresenter.onItemLongClick(position);
            }
        });
        mAdapter.setmOnItemSubClickListener(new RecyclerViewListener.OnRecyclerViewItemSubClickListener() {
            @Override
            public void onItemSubClick(View view, int position) {
                mPresenter.onItemSubClick(view, position);
            }
        });
    }

    @Override
    public Bundle getBundle() {
        return getArguments();
    }

    @Override
    public void setPresenter(LookProfileContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
        }
    }

    @Override
    protected void subscribeEvents() {
    }

    @Override
    public LookProfileAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void showLoading() {
        //显示加载对话框
        loadingDialog = new LoadingDialog(mActivity);
        loadingDialog.setLoadingText("加载中...")
                .setFailedText("加载失败")
                .setSuccessText("加载成功")
                .show();
    }

    @Override
    public void closeLoading(boolean success) {
        if (loadingDialog != null) {
            if (success) {
                loadingDialog.loadSuccess();
            } else {
                loadingDialog.loadFailed();
            }
        }
    }

    @Override
    public void startDragPhotoActivity(ImageView imageView, String url) {
        DragPhotoView.startDragPhotoActivity(mActivity, imageView, url);
    }

    @Override
    public void startMessageActivity(String userId, String title) {
        if (RongIM.getInstance() != null) {
            try {
                RongIM.getInstance().startPrivateChat(mActivity, userId, title);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Logger.t(this.getClass().getSimpleName()).e("rongim null");
        }
    }

    public void showDialog() {
        final MaterialDialog dialog = new MaterialDialog(mContext);
        dialog.btnNum(1)
                .content("接单成功才可以查看更多信息及发送信息")
                .btnText("确定")
                .showAnim(mBasIn)
                .dismissAnim(mBasOut)
                .show();

        dialog.setOnBtnClickL(new OnBtnClickL() {
            @Override
            public void onBtnClick() {
                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onActivityBackPressed() {
        getFragmentManager().popBackStack();
        return false;
    }
}
