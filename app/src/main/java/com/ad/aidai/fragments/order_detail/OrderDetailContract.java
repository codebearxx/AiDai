package com.ad.aidai.fragments.order_detail;

import android.support.annotation.NonNull;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.OrderDetailedBean;

import java.util.List;

/**
 * OrderDetailContract
 * <p>
 * Created by Yusxon on 17/1/25.
 */

public interface OrderDetailContract {

    interface View extends BaseView<Presenter> {

        /**
         * 加载订单信息完成
         */
        void loadOrderDataComplete();

        /**
         * 跳转到申述界面
         */
        void gotoState();

        /**
         * 显示加载对话框
         *
         * @param text
         */
        void showLoading(@NonNull String text);

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 打开查看个人资料界面
         *
         * @param userId
         * @param show
         * @param showButton
         */
        void openLookProfile(long userId, boolean show, boolean showButton);

        /**
         * 跳转到评分界面
         */
        void gotoComment();

        /**
         * 显示提示框
         *
         * @param message
         * @param btnText
         */
        void showDialog(String message, String btnText);

    }

    interface Presenter extends BasePresenter {
        /**
         * 加载订单信息
         */
        void loadOrderData();

        /**
         * 获取订单信息
         *
         * @return
         */
        List<OrderDetailedBean> getOrderData();

        /**
         * 点击了按钮
         *
         * @param view
         * @param position
         */
        void clickBtnOrder(android.view.View view, int position);

        /**
         * 点击
         *
         * @param view
         * @param position
         */
        void clickItem(android.view.View view, int position);

        /**
         * 点击提示框的确定按钮
         *
         * @param btnText
         */
        void clickDialogSure(String btnText);

    }
}
