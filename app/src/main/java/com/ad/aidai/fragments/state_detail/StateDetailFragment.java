package com.ad.aidai.fragments.state_detail;

import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.common.widgets.loading_dialog.LoadingDialog;
import com.ad.aidai.common.widgets.photoview.DragPhotoView;
import com.ad.aidai.fragments.order_detail.OrderDetailFragment;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

/**
 * 申述单详情
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public class StateDetailFragment extends BaseFragment implements StateDetailContract.View {

    @BindView(R.id.tv_state_number)
    TextView tvStateNumber;
    @BindView(R.id.tv_state_time)
    TextView tvStateTime;
    @BindView(R.id.tv_order_type)
    TextView tvOrderType;
    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;
    @BindView(R.id.tv_order_status)
    TextView tvOrderStatus;
    @BindView(R.id.tv_order_content)
    TextView tvOrderContent;
    @BindView(R.id.tv_order_money)
    TextView tvOrderMoney;
    @BindView(R.id.tv_state_content)
    TextView tvStateContent;
    @BindViews({R.id.iv_state_image_1, R.id.iv_state_image_2, R.id.iv_state_image_3})
    ImageView[] ivStateImage;
    @BindView(R.id.tv_state_status)
    TextView tvStateStatus;

    private StateDetailContract.Presenter mPresenter;

    private LoadingDialog loadingDialog;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_state_detail, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        new StateDetailPresenter(mContext, this).subscribe();

        mPresenter.loadState();
        mPresenter.loadOrderDetail(true);
    }

    @Override
    public void setPresenter(StateDetailContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back, R.id.iv_state_image_1, R.id.iv_state_image_2, R.id.iv_state_image_3, R.id.ll_order})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
            case R.id.iv_state_image_1:
                DragPhotoView.startDragPhotoActivity(mActivity, ivStateImage[0], mPresenter.getStateImageUrl(0));
                break;
            case R.id.iv_state_image_2:
                DragPhotoView.startDragPhotoActivity(mActivity, ivStateImage[1], mPresenter.getStateImageUrl(1));
                break;
            case R.id.iv_state_image_3:
                DragPhotoView.startDragPhotoActivity(mActivity, ivStateImage[2], mPresenter.getStateImageUrl(2));
                break;
            case R.id.ll_order:
                if(mPresenter.loadOrderDetail(false)) {
                    loadOrderSuccess();
                }
                break;
        }
    }

    @Override
    public void showLoading() {
        //显示加载对话框
        loadingDialog = new LoadingDialog(mActivity);
        loadingDialog.setLoadingText("加载中...")
                .setFailedText("加载失败")
                .setSuccessText("加载成功")
                .show();
    }

    @Override
    public void closeLoading(boolean success) {
        if (loadingDialog != null) {
            if (success) {
                loadingDialog.loadSuccess();
            } else {
                loadingDialog.loadFailed();
            }
        }
    }

    @Override
    public void loadOrderSuccess() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(loadingDialog != null && loadingDialog.isShowing()) {

                }
                loadingDialog = null;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //跳转进item详细信息页面
                        getActivity()
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                                        .fragment_slide_left_exit)
                                .add(android.R.id.content, new OrderDetailFragment(), "orderDetailed")
                                .addToBackStack("orderDetailed")
                                .commit();
                    }
                });
            }
        }).start();
    }

    @Override
    public void showStateNumber(String stateNumber) {
        tvStateNumber.setText(stateNumber);
    }

    @Override
    public void showStateTime(String stateTime) {
        tvStateTime.setText(stateTime);
    }

    @Override
    public void showOrderType(String orderType) {
        tvOrderType.setText(orderType);
    }

    @Override
    public void showOrderTime(String orderTime) {
        tvOrderTime.setText(orderTime);
    }

    @Override
    public void showOrderStatus(String orderStatus) {
        //        tvOrderStatus.setText(orderStatus);
    }

    @Override
    public void showOrderContent(String orderContent) {
        tvOrderContent.setText(orderContent);
    }

    @Override
    public void showOrderMoney(String orderMoney) {
        tvOrderMoney.setText(orderMoney);
    }

    @Override
    public void showStateContent(String stateContent) {
        tvStateContent.setText(stateContent);
    }

    @Override
    public void showImage(String... urls) {
        int position = 0;
        for (String url : urls) {
            if (!CheckUtil.checkStrNull(url)) {
                Glide.with(mActivity)
                        .load(url)
                        .placeholder(R.mipmap.default_image)
                        .dontAnimate()
                        .into(ivStateImage[position]);
                ivStateImage[position].setVisibility(View.VISIBLE);
                position++;
                if (position >= 3) {
                    break;
                }
            }
        }
    }

    @Override
    public void showStateStatus(String stateStatus, @ColorRes int textColor) {
        tvStateStatus.setText(stateStatus);
        tvStateStatus.setTextColor(ContextCompat.getColor(mContext, textColor));
    }

    @Override
    public boolean onActivityBackPressed() {
        ADApp.setState(null);
        ADApp.setOrder(null);
        getFragmentManager().popBackStack();
        return false;
    }
}
