package com.ad.aidai.fragments.reset_password;

import android.content.Context;
import android.content.SharedPreferences;

import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.common.utils.EncryptUtils;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResult;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.content.Context.MODE_PRIVATE;

/**
 * ResetPasswordPresenter
 * <p>
 * Created by Yusxon on 16/12/18.
 */

public class ResetPasswordPresenter implements ResetPasswordContract.Presenter {

    private Context mContext;
    private ResetPasswordContract.View mView;
    private CompositeSubscription mSubscriptions;
    private Gson gson;
    private SharedPreferences aidaiSP;

    public ResetPasswordPresenter(Context context, ResetPasswordContract.View view) {
        this.mContext = context;
        this.mView = view;
        gson = new Gson();
        mView.setPresenter(this);
        aidaiSP = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
        mSubscriptions = null;
    }
    private String passwordMD5;

    @Override
    public void resetPassword(final String phone, String newPassword, String token) {
        if (mView != null) {
            if (CheckUtil.checkStrNull(newPassword)) {
                mView.toast("密码不能为空");
            } else {
                //显示加载对话框
                mView.showLoading();
                passwordMD5 = EncryptUtils.GetMD5Code(newPassword);
                NetWork.getAccountOperationApi()
                        .resetPassword(phone, passwordMD5, token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<NetResult>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                Logger.t("ResetPasswordPresenter").e(e.getMessage());
                                //关闭加载对话框
                                mView.closeLoading(false);
                            }

                            @Override
                            public void onNext(NetResult netResult) {
                                Logger.t("ResetPasswordPresenter").i(netResult.toString());
                                if (netResult.getCode() != StatusCodeFromNet.CODE_SUCCESS) {
                                    mView.closeLoading(false);
                                } else {
                                    mView.closeLoading(true);
                                    savePhoneAndPwd(phone, passwordMD5);
                                    mView.resetSuccess();
                                }
                            }
                        });
            }
        }
    }

    /**
     * 保存用户手机号跟密码,以便下次登录自动加载到输入框中
     *
     * @param phone    手机号
     * @param password 密码
     */
    private void savePhoneAndPwd(String phone, String password) {
        aidaiSP.edit()
                .putBoolean("IS_LOGIN", false)
                .putString("USER_PHONE", phone)
                .putString("USER_PASSWORD", password)
                .apply();
    }

}
