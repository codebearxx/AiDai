package com.ad.aidai.fragments.select_school;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * SelectSchoolContract
 * <p>
 * Created by Yusoxn on 2017/1/23.
 */

public interface SelectSchoolContract {

    interface View extends BaseView<Presenter> {

        /**
         * 搜索完毕关闭软键盘
         */
        void searchFinish();

        /**
         * 点击item后退出
         */
        void itemClickExit();

    }

    interface Presenter extends BasePresenter {

        /**
         * 获取adapter
         *
         * @return
         */
        SelectSchoolAdapter getAdapter();

        /**
         * 加载学校信息
         */
        void loadSchool();

        /**
         * 获取选中的字符在列表用所在的位置
         *
         * @param index
         * @param text
         * @return
         */
        int getSelectItemPosition(int index, String text);

        /**
         * 查找学校
         *
         * @param inputStr 输入的字符串
         */
        void searchSchool(String inputStr);

    }
}