package com.ad.aidai.fragments.me.bell_setting;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * Created by pure on 2017/2/10.
 */

public interface BellSettingContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示新消息提醒是否有声音
         */
        void showIsHaveVoice();

        /**
         * 显示新消息提醒是否有振动
         */
        void showIsHaveVibrate();

    }

    interface Presenter extends BasePresenter {

        /**
         * 转换新消息提醒是否有声音
         */
        void switchIsHaveVoice();

        /**
         * 获取上一次设置的新消息是否有声音
         * @return
         */
        boolean getIsHaveVoiceLast();

        /**
         * 转换新消息提醒是否有振动
         */
        void switchIsHaveVibrate();

        /**
         * 获取上一次设置的新消息是否有振动
         * @return
         */
        boolean getIsHaveVibrateLast();

    }
}
