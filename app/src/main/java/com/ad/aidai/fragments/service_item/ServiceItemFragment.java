package com.ad.aidai.fragments.service_item;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by pure on 2017/2/15.
 */

public class ServiceItemFragment extends BaseFragment implements ServiceItemContract.View {
    @BindView(R.id.wv_item_content)
    WebView wvItemContent;

    private ServiceItemContract.Presenter mPresenter;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_service_item, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        new ServiceItemPresenter(mContext, this).subscribe();
        mPresenter.setItemContent();

    }

    @Override
    public void setPresenter(ServiceItemContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
        }
    }

    @Override
    public void showItemContent(String url) {
        wvItemContent.loadUrl(url);
    }

    @Override
    public boolean onActivityBackPressed() {
        getFragmentManager().popBackStack();
        return false;
    }
}
