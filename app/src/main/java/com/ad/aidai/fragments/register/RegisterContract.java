package com.ad.aidai.fragments.register;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * RegisterContract
 * <p>
 * Created by Yusxon on 16/12/7.
 */

public interface RegisterContract {

    interface View extends BaseView<Presenter> {
        /**
         * 显示验证码输入倒计时
         *
         * @param time 倒计时时间
         */
        void showVerficationCodeCountDown(String time);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 注册成功
         */
        void registerSuccess();

        /**
         * 验证码按钮是否可以点击
         *
         * @param isClick
         */
        void setVCButtonClickable(boolean isClick);
    }

    interface Presenter extends BasePresenter {
        /**
         * 发送验证码
         *
         * @param phoneNumber 手机号码
         */
        void sendVerificationCode(String phoneNumber);

        /**
         * 注册
         *
         * @param phoneNumber     手机号码
         * @param password        密码
         * @param verficationCode 验证码
         */
        void register(String phoneNumber, String password, String verficationCode);
    }
}
