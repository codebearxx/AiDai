package com.ad.aidai.fragments.service_item;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * Created by pure on 2017/2/15.
 */

public interface ServiceItemContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示条款内容
         */
        void showItemContent(String url);

    }

    interface Presenter extends BasePresenter {

        /**
         * 设置条款内容
         */
        void setItemContent();

    }
}
