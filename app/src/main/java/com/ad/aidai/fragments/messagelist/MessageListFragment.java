package com.ad.aidai.fragments.messagelist;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;

import butterknife.BindView;
import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imlib.model.Conversation;

/**
 * 聊天列表的fragment
 * <p>
 * Created by pure on 2016/12/8.
 */

public class MessageListFragment extends BaseFragment {

    @BindView(R.id.fl_message_list)
    FrameLayout flMessageList;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    protected void initView() {
        ConversationListFragment listFragment = new ConversationListFragment();
        Uri uri = Uri.parse("rong://" + mContext.getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist")
                .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话是否聚合显示
                .appendQueryParameter(Conversation.ConversationType.GROUP.getName(), "false")//群组
                .appendQueryParameter(Conversation.ConversationType.DISCUSSION.getName(), "false")//讨论组
                .appendQueryParameter(Conversation.ConversationType.PUBLIC_SERVICE.getName(), "false")//公共服务号
                .appendQueryParameter(Conversation.ConversationType.APP_PUBLIC_SERVICE.getName(), "false")//订阅号
                .appendQueryParameter(Conversation.ConversationType.SYSTEM.getName(), "false")//系统
                .build();
        listFragment.setUri(uri);

        getFragmentManager()
                .beginTransaction()
                .add(R.id.fl_message_list, listFragment)
                .commit();
    }

    @Override
    protected void initData() {
        //        Logger.t(this.getClass().getSimpleName()).i("message");
    }

    @Override
    protected void subscribeEvents() {
        super.subscribeEvents();
    }

    @Override
    public boolean onActivityBackPressed() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
