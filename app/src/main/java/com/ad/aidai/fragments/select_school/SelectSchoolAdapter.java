package com.ad.aidai.fragments.select_school;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseViewHolder;
import com.ad.aidai.bean.ItemSchool;
import com.ad.aidai.bean.School;
import com.ad.aidai.common.listener.RecyclerViewListener;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.common.utils.MultiStylesTextViewUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

import butterknife.BindView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 选择学校页面的adapter
 * <p>
 * Created by Yusoxn on 2017/1/23.
 */

public class SelectSchoolAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private Context mContext;
    private List<ItemSchool> datas;

    private RecyclerViewListener.OnRecyclerViewItemClickListener mOnItemClickListener = null;

    public void setOnItemClickListener(RecyclerViewListener.OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public SelectSchoolAdapter(Context context, List<ItemSchool> schools) {
        this.mContext = context;
        datas = schools;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder;
        switch (viewType) {
            case ItemSchool.ITEM_DATA:
                holder = new SchoolItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_select_school, parent, false));
                break;
            case ItemSchool.ITEM_CHARACTER:
                holder = new CharItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_select_school_char, parent, false));
                break;
            default:
                holder = new EmptyItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_select_school_empty, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final BaseViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ItemSchool.ITEM_DATA:
                final School school = (School) datas.get(position).getItemMessage();
                SchoolItemViewHolder schoolItemViewHolder = (SchoolItemViewHolder) holder;
                final String matchStr = datas.get(position).getMatchStr();
                if (!CheckUtil.checkStrNull(matchStr)) {
                    final MultiStylesTextViewUtil textViewUtil = new MultiStylesTextViewUtil(mContext).initTextView
                            (schoolItemViewHolder.tvSelectSchool);
                    Observable
                            .create(new Observable.OnSubscribe<Object>() {
                                @Override
                                public void call(Subscriber<? super Object> subscriber) {
                                    int index = 0;
                                    String schoolName = school.getSchoolName();
                                    for (int i = 0; i < matchStr.length(); ++i) {
                                        for (; index < schoolName.length(); ++index) {
                                            if (schoolName.charAt(index) == matchStr.charAt(i)) {
                                                textViewUtil.appendColorText(schoolName.charAt(index) + "", R.color
                                                        .colorTheme);
                                                ++index;
                                                break;
                                            } else {
                                                textViewUtil.appendText(schoolName.charAt(index) + "");
                                            }
                                        }
                                    }
                                    if (index < schoolName.length()) {
                                        textViewUtil.appendText(schoolName.substring(index));
                                    }
                                    subscriber.onCompleted();
                                }
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<Object>() {
                                @Override
                                public void onCompleted() {
                                    textViewUtil.create();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    MobclickAgent.reportError(mContext, e);
                                }

                                @Override
                                public void onNext(Object o) {
                                }
                            });
                } else {
                    schoolItemViewHolder.tvSelectSchool.setText(school.getSchoolName());
                }
                schoolItemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            int pos = holder.getLayoutPosition();
                            mOnItemClickListener.onItemClick(v, pos);
                        }
                    }
                });
                break;
            case ItemSchool.ITEM_CHARACTER:
                CharItemViewHolder charItemViewHolder = (CharItemViewHolder) holder;
                charItemViewHolder.tvSelectSchoolChar.setText((String) datas.get(position).getItemMessage());
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position).getItemType();
    }

    class CharItemViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_select_school_char)
        TextView tvSelectSchoolChar;

        public CharItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    class SchoolItemViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_select_school)
        TextView tvSelectSchool;

        public SchoolItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    class EmptyItemViewHolder extends BaseViewHolder {

        public EmptyItemViewHolder(View itemView) {
            super(itemView);
        }
    }

}
