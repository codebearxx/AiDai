package com.ad.aidai.fragments.me.profile.update_personal_note;

import android.content.Context;

import com.ad.aidai.bean.User;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResult;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by pure on 2017/2/4.
 */

public class UpPersonalNotePresenter implements UpPersonalNoteContract.Presenter{

    private Context mContext;
    private UpPersonalNoteContract.View mView;
    private CompositeSubscription mSubscriptions;

    public UpPersonalNotePresenter(Context context, UpPersonalNoteContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        initData();

    }

    private void initData() {

    }

    @Override
    public void savePerSignature(final String savePerSignature) {
        mView.showLoading();
        NetWork.getAccountOperationApi()
                .updateUserPersonalNote(ADApp.getUser().getUserId(), savePerSignature)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResult>() {
                    @Override
                    public void onCompleted() {

                    }
                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("updateUserPersonalNote").e(e.getMessage());
                        //关闭加载对话框
                        mView.closeLoading(false);
                    }
                    @Override
                    public void onNext(NetResult netResult) {
                        if (netResult.getCode() != StatusCodeFromNet.CODE_SUCCESS) {
                            Logger.t("updateUserPersonalNote").e(netResult.toString());
                            mView.closeLoading(false);
                        } else {
                            Logger.t("updateUserPersonalNote").i("updateUserPersonalNote:" + savePerSignature);
                            User user = ADApp.getUser();
                            user.setUserPersonalNote(savePerSignature);
                            MyUserDao.getUserDao().save(user);
                            RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.USER_PERSONAL_NOTE));
                            mView.closeLoading(true);
                            mView.modifySuccess();
                        }
                    }
                });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }
}
