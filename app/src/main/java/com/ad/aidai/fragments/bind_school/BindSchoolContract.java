package com.ad.aidai.fragments.bind_school;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.School;

/**
 * BindSchoolContract
 * <p>
 * Created by pure on 2017/1/18.
 */

public interface BindSchoolContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示已经选择的学校
         */
        void showSelectedSchool(String schoolName);

        /**
         * 绑定成功
         */
        void bindSuccess();

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

    }

    interface Presenter extends BasePresenter {
        /**
         * 绑定学校
         */
        void bindSchool(long schoolId, String schoolName, String stuNum, String pass);

        /**
         * 选择学校
         *
         * @param school
         */
        void selectSchool(School school);
    }
}
