package com.ad.aidai.fragments.state_detail;

import android.content.Context;
import android.support.annotation.ColorRes;

import com.ad.aidai.R;
import com.ad.aidai.bean.Order;
import com.ad.aidai.bean.State;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.network.ADUrl;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResultWithData;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * StateDetailPresenter
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public class StateDetailPresenter implements StateDetailContract.Presenter {
    private Context mContext;
    private StateDetailContract.View mView;
    private CompositeSubscription mSubscriptions;

    private boolean loadOrderSuccess = false;
    private Order order;
    private boolean loading = false;
    private String[] urls = new String[3];

    public StateDetailPresenter(Context context, StateDetailContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void loadState() {

        State state = ADApp.getState();
        Order order = state.getOrder();
        String[] orderType = {"代拿", "代买", "代办"};
        mView.showOrderType(orderType[order.getOrderType()]);
        mView.showOrderTime(order.getOrderSendTime());
        mView.showOrderContent(order.getOrderContent());
        mView.showOrderMoney(String.valueOf(order.getOrderReward()));

        if (!CheckUtil.checkStrNull(state.getStateImage1())) {
            urls[0] = ADUrl.STATE_IMAGE_URL.replace("{photoName}", state.getStateImage1());
            if (!CheckUtil.checkStrNull(state.getStateImage2())) {
                urls[1] = ADUrl.STATE_IMAGE_URL.replace("{photoName}", state.getStateImage2());
                if (!CheckUtil.checkStrNull(state.getStateImage3())) {
                    urls[2] = ADUrl.STATE_IMAGE_URL.replace("{photoName}", state.getStateImage3());
                }
            }
        }
        mView.showImage(urls[0], urls[1], urls[2]);

        mView.showStateNumber(String.valueOf(state.getStateId()));
        mView.showStateTime(state.getStateSubmitTime());
        mView.showStateContent(state.getStateCause());
        String[] stateStatus = {"待完成", "已完成"};
        @ColorRes int[] color = {R.color.main_home_item_final_color, R.color.colorTheme};
        mView.showStateStatus(stateStatus[state.getStateStatus()], color[state.getStateStatus()]);
    }

    private boolean autoLoadOrder = false;

    @Override
    public boolean loadOrderDetail(boolean auto) {
        autoLoadOrder = auto;
        if(loadOrderSuccess) {
            ADApp.setOrder(order);
            return true;
        }
        if(!autoLoadOrder) {
            autoLoadOrder = false;
            mView.showLoading();
        }
        if(!loading) {
            loading = true;
            NetWork.getOrderOperationApi()
                    .getOrderByOrderId(ADApp.getState().getOrderId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<NetResultWithData<Order>>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            MobclickAgent.reportError(mContext, e);
                            Logger.t("StateDetailPresenter").e(e.getMessage());
                            if (!autoLoadOrder) {
                                mView.closeLoading(false);
                            }
                            loading = false;
                        }

                        @Override
                        public void onNext(NetResultWithData<Order> result) {
                            Logger.t("StateDetailPresenter").i("Order:" + result.toString());
                            if (result.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                                order = result.getData();
                                ADApp.setOrder(result.getData());
                                loadOrderSuccess = true;
                                if (!autoLoadOrder) {
                                    mView.closeLoading(true);
                                    mView.loadOrderSuccess();
                                }
                            } else {
                                if (!autoLoadOrder) {
                                    mView.closeLoading(false);
                                }
                                Logger.t("StateDetailPresenter").e(result.toString());
                            }
                            loading = false;
                        }
                    });
        }

        return false;
    }

    @Override
    public String getStateImageUrl(int position) {
        return urls[position];
    }
}
