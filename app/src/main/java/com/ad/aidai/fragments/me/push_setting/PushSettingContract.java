package com.ad.aidai.fragments.me.push_setting;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.mingle.entity.MenuEntity;

import java.util.List;

/**
 * Created by pure on 2017/2/11.
 */

public interface PushSettingContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示是否推送
         */
        void showIsPush();

        /**
         * 推送时间线
         */
        void postEventPush();

        /**
         * 定位时间线
         */
        void postEventLocation();

        /**
         * 显示推送间隔
         */
        void showPushInterval(String interval);

        /**
         * 是否显示推送间隔
         * @param isShow
         */
        void isShowPushInterval(boolean isShow);

    }

    interface Presenter extends BasePresenter {

        /**
         * 转换是否推送
         */
        void switchIsPush();

        /**
         * 获取上一次设置的是否推送
         * @return
         */
        boolean getIsPushLast();

        /**
         * 设置推送间隔
         */
        void setPushInterval(int interval);

        /**
         * 获取推送时间间隔列表
         *
         * @return
         */
        List<MenuEntity> getPushIntervalMenuEntity();

        /**
         * 获取上一次设置的推送间隔
         * @return
         */
        int getPushIntervalLast();

        /**
         * 转换是否定位
         */
        void switchIsLocation();

        /**
         * 获取上一次设置的是否定位
         * @return
         */
        boolean getIsLocationLast();

    }
}
