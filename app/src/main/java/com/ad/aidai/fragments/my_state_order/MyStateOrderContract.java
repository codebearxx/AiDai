package com.ad.aidai.fragments.my_state_order;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.State;

import java.util.List;

/**
 * MyReceiveOrderContract
 * <p>
 * Created by Yusxon on 17/2/1.
 */

public interface MyStateOrderContract {

    interface View extends BaseView<Presenter> {

        /**
         * 设置是否在刷新
         *
         * @param refreshing
         */
        void setOnRefresh(boolean refreshing);

        /**
         * 获取adapter
         *
         * @return
         */
        MyStateOrderAdapter getAdapter();

    }

    interface Presenter extends BasePresenter {

        /**
         * 获取订单信息
         *
         * @return
         */
        List<State> getOrderData();

        /**
         * 加载更多
         */
        void loadMoreOrders();

        /**
         * 刷新
         */
        void refresh();

        /**
         * 设置adapter
         */
        void setAdapter();

        /**
         * 点击item中的某个view
         *
         * @param view
         * @param position
         */
        void onItemSubClick(android.view.View view, int position);

        /**
         * 点击item
         *
         * @param position
         */
        void onItemClick(int position);
    }
}
