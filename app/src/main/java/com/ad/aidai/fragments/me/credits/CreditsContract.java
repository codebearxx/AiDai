package com.ad.aidai.fragments.me.credits;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * Created by pure on 2017/2/7.
 */

public interface CreditsContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示积分
         */
        void showStar(String star);

        /**
         * 显示积分说明
         */
        void showStarExplainContent(String url);

    }

    interface Presenter extends BasePresenter {

        /**
         * 设置积分说明内容
         */
        void setStarExplainContent();
    }
}
