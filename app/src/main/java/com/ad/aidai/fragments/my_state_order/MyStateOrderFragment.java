package com.ad.aidai.fragments.my_state_order;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.fragments.state_detail.StateDetailFragment;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.jude.easyrecyclerview.decoration.DividerDecoration;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我发的单页面
 * <p>
 * Created by Yusxon on 17/2/2.
 */

public class MyStateOrderFragment extends BaseFragment implements MyStateOrderContract.View, SwipeRefreshLayout
        .OnRefreshListener {

    @BindView(R.id.ercv_my_state)
    EasyRecyclerView ercvMyState;

    private MyStateOrderContract.Presenter mPresenter;
    private MyStateOrderAdapter mAdapter;
    /**
     * 当前是否在刷新
     */
    private boolean isRefreshing = false;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_my_state_order, container, false);
        mView.setClickable(true);
    }

    @Override
    protected void initView() {
        //创建默认的线性LayoutManager
        ercvMyState.setLayoutManager(new LinearLayoutManager(getActivity()));

        //设置分割线
        DividerDecoration itemDecoration = new DividerDecoration(Color.parseColor("#F2F2F2"), 30, 0, 0);
        itemDecoration.setDrawLastItem(true);
        ercvMyState.addItemDecoration(itemDecoration);
        //监听下拉刷新
        ercvMyState.setRefreshListener(this);
        //设置下拉刷新控件的颜色,可以有颜色变化,下面的方法传入的参数可以多个的,一个就只显示一种颜色
        ercvMyState.setRefreshingColor(ContextCompat.getColor(mContext, R.color.colorTheme));
    }

    @Override
    protected void initData() {
        new MyStateOrderPresenter(mContext, this).subscribe();

        mAdapter = new MyStateOrderAdapter(mContext, mPresenter.getOrderData());
        mPresenter.setAdapter();
        ercvMyState.setAdapter(mAdapter);
        listener();
        //首次进界面,自动刷新
        ercvMyState.setRefreshing(true, true);
    }

    @Override
    public void setPresenter(MyStateOrderContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick(R.id.iv_back)
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onRefresh() {
        mPresenter.refresh();
    }

    private void listener() {
        //加载更多
        mAdapter.setMore(R.layout.view_more, new RecyclerArrayAdapter.OnMoreListener() {
            @Override
            public void onMoreShow() {
                if (!isRefreshing) {
                    mPresenter.loadMoreOrders();
                } else {
                    mAdapter.stopMore();
                }
            }

            @Override
            public void onMoreClick() {

            }

        });
        //没有更多
        mAdapter.setNoMore(R.layout.view_clickmore, new RecyclerArrayAdapter.OnNoMoreListener() {
            @Override
            public void onNoMoreShow() {
                mAdapter.resumeMore();
            }

            @Override
            public void onNoMoreClick() {
                mAdapter.resumeMore();
            }
        });
        //加载错误(原来的逻辑有问题,暂时只能用没有更多来代替)
        mAdapter.setError(R.layout.view_nomore);

        mAdapter.setOnItemClickListener(new RecyclerArrayAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mPresenter.onItemClick(position);
                //跳转进item详细信息页面
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                                .fragment_slide_left_exit)
                        .add(android.R.id.content, new StateDetailFragment(), "stateDetail")
                        .addToBackStack("stateDetail")
                        .commit();
            }
        });
    }

    @Override
    public void setOnRefresh(boolean refreshing) {
        this.isRefreshing = refreshing;
    }

    @Override
    public MyStateOrderAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean onActivityBackPressed() {
        getFragmentManager().popBackStack();
        return false;
    }
}
