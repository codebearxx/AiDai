package com.ad.aidai.fragments.order.order_push;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.bean.Order;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.common.utils.MultiStylesTextViewUtil;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.fragments.me.push_setting.PushSettingFragment;
import com.ad.aidai.fragments.order_detail.OrderDetailFragment;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.decoration.DividerDecoration;

import butterknife.BindView;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * 推送的订单的页面
 * <p>
 * Created by Yusxon on 16/12/17.
 */

public class OrderPushFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        OrderPushContract.View {

    @BindView(R.id.ercv_orders_push)
    EasyRecyclerView ercvOrdersPush;
    @BindView(R.id.tv_go_set_push)
    TextView tvGoSetPush;
    @BindView(R.id.ll_no_set_push)
    LinearLayout llNoSetPush;

    private OrderPushContract.Presenter mPresenter;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_order_push, container, false);
    }

    @Override
    protected void initView() {

        new MultiStylesTextViewUtil(getContext())
                .initTextView(tvGoSetPush)
                .appendText("推送功能未开启!\n      前往")
                .appendColorText("设置", R.color.colorTheme, new MultiStylesTextViewUtil.OnTextClickListener() {
                    @Override
                    public void textClick(View v, String text) {
                        getActivity()
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                                        .fragment_slide_left_exit)
                                .add(android.R.id.content, new PushSettingFragment(), "pushSetting")
                                .addToBackStack("pushSetting")
                                .commit();
                    }
                })
                .create();

        //设置为线性布局
        ercvOrdersPush.setLayoutManager(new LinearLayoutManager(mContext));
        //两个item之间的分隔线
        DividerDecoration itemDecoration = new DividerDecoration(Color.parseColor("#F2F2F2"), 30, 0, 0);
        itemDecoration.setDrawLastItem(false);
        ercvOrdersPush.addItemDecoration(itemDecoration);
        //监听下拉刷新
        ercvOrdersPush.setRefreshListener(this);
        //设置下拉刷新控件的颜色,可以有颜色变化,下面的方法传入的参数可以多个的,一个就只显示一种颜色
        ercvOrdersPush.setRefreshingColor(ContextCompat.getColor(mContext, R.color.colorTheme));

        ercvOrdersPush.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                RxBus.getInstance().postEvent(new CommonEvent<>(CommonEvent.ON_TOUCH_ORDERFRAGMENT, e));
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    @Override
    protected void initData() {
        new OrderPushPresenter(mContext, this).subscribe();
        ercvOrdersPush.setAdapter(mPresenter.getAdapter());
        mPresenter.pushSettingChange();
        if (mPresenter.isSetPush()) {
            isSetPush(true);
            //首次进界面,自动刷新
            ercvOrdersPush.setRefreshing(true, true);
        }
    }

    @Override
    protected void subscribeEvents() {
        //订阅事件,推送设置
        addSubscription(RxBus.getInstance()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Object>() {

                    @Override
                    public void call(Object event) {
                        if (event instanceof CommonEvent) {
                            CommonEvent commonEvent = (CommonEvent) event;
                            switch (commonEvent.getEvent()) {
                                case CommonEvent.PUSH_SETTING_CHANGE:
                                    mPresenter.pushSettingChange();
                                    break;
                                case CommonEvent.ORDER_STATUS_CHANGE:
                                    mPresenter.orderStatusChange((Order) commonEvent.getData());
                                    break;
                            }
                        }
                    }
                })
                .subscribe(RxBus.defaultSubscriber()));
    }

    @Override
    public void onRefresh() {
        mPresenter.refresh();
    }

    @Override
    public void intoItem() {
        //跳转进item详细信息页面
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new OrderDetailFragment(), "orderDetailed")
                .addToBackStack("orderDetailed")
                .commit();
    }

    @Override
    public void isSetPush(boolean is) {
        if (is) {
            llNoSetPush.setVisibility(View.GONE);
        } else {
            llNoSetPush.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setPresenter(OrderPushContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean onActivityBackPressed() {
        return true;
    }
}
