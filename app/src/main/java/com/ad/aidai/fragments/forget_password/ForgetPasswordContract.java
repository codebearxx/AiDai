package com.ad.aidai.fragments.forget_password;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * ForgetPasswordContract
 * <p>
 * Created by Yusxon on 16/12/18.
 */

public interface ForgetPasswordContract {


    interface View extends BaseView<Presenter> {
        /**
         * 显示手机号码
         *
         * @param phone
         */
        void setPhoneNumber(String phone);

        /**
         * 验证码按钮是否可以点击
         *
         * @param isClick
         */
        void setVCButtonClickable(boolean isClick);

        /**
         * 显示验证码输入倒计时
         *
         * @param time 倒计时时间
         */
        void showVerficationCodeCountDown(String time);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 验证成功
         */
        void verifySuccess(String token);
    }

    interface Presenter extends BasePresenter {

        /**
         * 加载手机号码
         * 进入登录界面先调用该方法,如果有用户手机号码信息,则会自动加载到输入框中
         */
        void loadPhone();

        /**
         * 发送验证码
         *
         * @param phoneNumber 手机号码
         */
        void sendVerificationCode(String phoneNumber);

        /**
         * 验证手机号
         *
         * @param phoneNumber     手机号
         * @param verficationCode 验证码
         */
        void verifyPhone(String phoneNumber, String verficationCode);

        /**
         * 重置密码成功后保存新的手机号,以便下次登录自动加载到输入框中
         *
         * @param phone 手机号
         */
        void savePhone(String phone);
    }

}
