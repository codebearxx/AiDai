package com.ad.aidai.fragments.me;

import android.graphics.Bitmap;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * MeContract
 * <p>
 * Created by Yusxon on 16/12/22.
 */

public interface MeContract {

    interface View extends BaseView<Presenter> {
        /**
         * 将bitmap显示到界面中
         * (顶部的模糊处理的图片)
         *
         * @param bitmap
         */
        void setBlurHeadImage(Bitmap bitmap);

        /**
         * 将bitmap显示到界面中
         * (顶部的圆形头像的图片)
         *
         * @param bitmap
         */
        void setHeadImage(Bitmap bitmap);

        /**
         * 显示积分
         *
         * @param star
         */
        void showStar(String star);

        /**
         * 显示用户昵称
         *
         * @param name
         */
        void showUsername(String name);

        /**
         * 显示用户性别
         *
         * @param which
         */
        void showUserSex(int which);

        /**
         * 显示个性签名
         *
         * @param profile
         */
        void showProfile(String profile);
    }

    interface Presenter extends BasePresenter {
        /**
         * 加载用户信息
         */
        void loadUserData();

        /**
         * 用户修改昵称
         */
        void userModifyName();

        /**
         * 用户修改性别
         */
        void userModifySex();

        /**
         * 用户修改个性签名
         */
        void userModifyPersonalNote();

        /**
         * 用户修改头像
         */
        void userModifyHeadImage();

        /**
         * 更新积分
         */
        void updateStar();
    }
}
