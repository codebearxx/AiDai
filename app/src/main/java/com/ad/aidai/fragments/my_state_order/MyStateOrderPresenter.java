package com.ad.aidai.fragments.my_state_order;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.ad.aidai.R;
import com.ad.aidai.bean.State;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.GetMyStateFromNet;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * MyReceiveOrderPresenter
 * <p>
 * Created by Yusxon on 17/2/2.
 */

public class MyStateOrderPresenter implements MyStateOrderContract.Presenter {

    private Context mContext;
    private MyStateOrderContract.View mView;
    private CompositeSubscription mSubscriptions;

    private List<State> mOrderData;
    private MyStateOrderAdapter mAdapter;

    /**
     * 最多显示一次加载多少数据
     */
    private static final int PAGE_SIZE = 10;
    /**
     * 当前加载第几页
     */
    private int currentPage = 1;

    public MyStateOrderPresenter(Context context, MyStateOrderContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        mOrderData = new ArrayList<>();
    }

    @Override
    public List<State> getOrderData() {
        return mOrderData;
    }

    @Override
    public void loadMoreOrders() {

    }

    @Override
    public void refresh() {
        mView.setOnRefresh(true);
        NetWork.getAccountOperationApi()
                .getMyStateOrder(ADApp.getUser().getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetMyStateFromNet>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("MyStateOrderPresenter").e(e.getMessage());
                        mView.toast("刷新失败");
                        mView.setOnRefresh(false);
                        mAdapter.add(null);
                        mAdapter.pauseMore();
                    }

                    @Override
                    public void onNext(GetMyStateFromNet result) {
                        Logger.t("MyStateOrderPresenter").i("refreshOrders:" + result.toString());
                        mView.setOnRefresh(false);
                        if (result.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            List<State> states = result.getData();
                            Logger.t("MyStateOrderPresenter").i(states.size() + "--");
                            currentPage = 2;
                            mAdapter.clear();
                            mAdapter.addAll(states);
                            mAdapter.pauseMore();
                        } else {
                            mView.toast("刷新失败");
                            mAdapter.add(null);
                            Logger.t("MyStateOrderPresenter").e(result.toString());
                        }
                    }
                });
    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
        mSubscriptions = null;
    }

    @Override
    public void setAdapter() {
        this.mAdapter = mView.getAdapter();
    }

    @Override
    public void onItemSubClick(View view, int position) {
        switch (view.getId()) {
            case R.id.btn_my_order_button:
                Button btn = (Button) view;
                mView.toast(btn.getText().toString());
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        ADApp.setState(mAdapter.getItem(position));
    }
}
