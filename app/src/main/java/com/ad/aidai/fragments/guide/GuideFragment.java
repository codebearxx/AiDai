package com.ad.aidai.fragments.guide;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.events.CommonEvent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;

import static android.content.Context.MODE_PRIVATE;

/**
 * 导航页
 * <p>
 * Created by Yusxon on 16/12/26.
 */

public class GuideFragment extends BaseFragment {

    @BindView(R.id.vp_guide)
    ViewPager vpGuide;
    @BindViews({R.id.iv_point1, R.id.iv_point2, R.id.iv_point3})
    List<ImageView> points;

    private List<View> views;
    /**
     * 上一个选中的点
     */
    private int lastPoint = 0;
    /**
     * 是否是手动点击按钮进来的
     */
    private boolean clickInto = false;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_guide, container, false);
        views = new ArrayList<>();
        views.add(inflater.inflate(R.layout.view_guide_one, null));
        views.add(inflater.inflate(R.layout.view_guide_two, null));
        views.add(inflater.inflate(R.layout.view_guide_three, null));
    }

    @Override
    protected void initView() {

        vpGuide.setAdapter(new PagerAdapter() {

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView(views.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(views.get(position));

                return views.get(position);
            }

            @Override
            public int getCount() {
                return views.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return (view == object);
            }
        });
        vpGuide.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                points.get(lastPoint).setImageResource(R.mipmap.icon_point_light);
                points.get(position).setImageResource(R.mipmap.icon_point_dark);
                lastPoint = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        views.get(2).findViewById(R.id.btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickInto) {
                    getFragmentManager().popBackStack();
                } else {
                    RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.GUIDE_BUTTON_CLICK));
                }
            }
        });
    }

    @Override
    protected void initData() {
        SharedPreferences push = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
        push.edit()
                .putBoolean("ISSETPUSH", false)
                .apply();
        push.edit()
                .putInt("pushInterval", 0)
                .apply();
        push.edit()
                .putBoolean("isLocation", false)
                .apply();

        SharedPreferences bell = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
        bell.edit()
                .putBoolean("new_message_voice", true)
                .apply();
        bell.edit()
                .putBoolean("new_message_vibrate", true)
                .apply();
    }

    public boolean isClickInto() {
        return clickInto;
    }

    public void setClickInto(boolean clickInto) {
        this.clickInto = clickInto;
    }

    @Override
    public boolean onActivityBackPressed() {
        return true;
    }
}
