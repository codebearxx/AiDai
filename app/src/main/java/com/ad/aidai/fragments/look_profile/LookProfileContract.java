package com.ad.aidai.fragments.look_profile;

import android.os.Bundle;
import android.widget.ImageView;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.ProfileBean;

import java.util.List;

/**
 * LookProfileContract
 * <p>
 * Created by pure on 2017/1/12.
 */

public interface LookProfileContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示图片
         *
         * @param imageView
         * @param url
         */
        void startDragPhotoActivity(ImageView imageView, String url);

        /**
         * 获取adapter
         *
         * @return
         */
        LookProfileAdapter getAdapter();

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 获取fragment传来的数据
         *
         * @return
         */
        Bundle getBundle();

        /**
         * 显示标题
         *
         * @param title
         */
        void setTitle(String title);

        /**
         * 打开聊天信息页面
         *
         * @param userId
         * @param title
         */
        void startMessageActivity(String userId, String title);

        /**
         * 显示提示框
         */
        void showDialog();

    }

    interface Presenter extends BasePresenter {
        /**
         * 加载用户信息
         */
        void loadUserData();

        /**
         * 获取需要加载的数据源
         *
         * @return
         */
        List<ProfileBean> getDatas();

        /**
         * 长按了某一项
         *
         * @param position
         */
        void onItemLongClick(int position);

        /**
         * 点击了某一项中的某个view
         *
         * @param view
         * @param position
         */
        void onItemSubClick(android.view.View view, int position);

        /**
         * 设置adapter
         */
        void setAdapter();
    }
}
