package com.ad.aidai.fragments.order_detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseViewHolder;
import com.ad.aidai.bean.OrderDetailedBean;
import com.ad.aidai.bean.ProfileBean;
import com.ad.aidai.bean.User;
import com.ad.aidai.common.listener.RecyclerViewListener;
import com.ad.aidai.common.widgets.imageview.CircleImageView;
import com.ad.aidai.network.ADUrl;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * OrderDetailAdapter
 * <p>
 * Created by Yusxon on 17/1/26.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<OrderDetailedBean> datas;
    private Context mContext;

    private RecyclerViewListener.OnRecyclerViewItemClickListener mOnItemClickListener = null;

    private RecyclerViewListener.OnRecyclerViewItemSubClickListener mOnItemSubClickListener = null;

    public void setOnItemClickListener(RecyclerViewListener.OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void setmOnItemSubClickListener(RecyclerViewListener.OnRecyclerViewItemSubClickListener
                                                   mOnItemSubClickListener) {
        this.mOnItemSubClickListener = mOnItemSubClickListener;
    }

    public OrderDetailAdapter(Context context, List<OrderDetailedBean> datas) {
        this.mContext = context;
        this.datas = datas;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder;
        switch (viewType) {
            case OrderDetailedBean.VIEWTYPE_HEAD:
                holder = new HeadViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_order_detail_head, parent, false));
                break;
            case OrderDetailedBean.VIEWTYPE_ONE_LINE:
                holder = new LineViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_order_detail_one_line, parent, false));
                break;
            case OrderDetailedBean.VIEWTYPE_TWO_LINE:
                holder = new LineViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_order_detail_two_line, parent, false));
                break;
            case OrderDetailedBean.VIEWTYPE_BUTTON:
                holder = new ButtonViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_order_detail_button, parent, false));
                break;
            default:
                holder = new BaseViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_profile_space, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case OrderDetailedBean.VIEWTYPE_HEAD:
                HeadViewHolder headViewHolder = (HeadViewHolder) holder;
                User user = (User) datas.get(position).getValue();

                headViewHolder.ivIcon.setImageResource(datas.get(position).getIcon());
                headViewHolder.tvTitle.setText(datas.get(position).getName());
                headViewHolder.tvName.setText(user.getUserName());
                headViewHolder.ivSexIcon.setImageResource(user.getUserSex() == 0 ? R.mipmap.icon_male : R.mipmap
                        .icon_female);

                String url = ADUrl.HEAD_ICON_URL.replace("{schoolId}", user.getSchoolId() + "").replace("{photoName}", user
                        .getUserIcon());
                Glide.with(mContext)
                        .load(url)
                        .placeholder(R.mipmap.default_image)
                        .dontAnimate()
                        .into(headViewHolder.civHead);

                break;
            case OrderDetailedBean.VIEWTYPE_ONE_LINE:
            case OrderDetailedBean.VIEWTYPE_TWO_LINE:
                LineViewHolder lineViewHolder = (LineViewHolder) holder;

                lineViewHolder.ivIcon.setImageResource(datas.get(position).getIcon());
                lineViewHolder.tvTitle.setText(datas.get(position).getName());
                lineViewHolder.tvValue.setText(String.valueOf(datas.get(position).getValue()));

                break;
            case OrderDetailedBean.VIEWTYPE_BUTTON:
                ButtonViewHolder buttonViewHolder = (ButtonViewHolder) holder;
                buttonViewHolder.btnOrder.setText((String) datas.get(position).getValue());
                break;
            case ProfileBean.VIEWTYPE_SPACE:

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class HeadViewHolder extends BaseViewHolder {

        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.iv_sex_icon)
        ImageView ivSexIcon;
        @BindView(R.id.civ_head)
        CircleImageView civHead;

        public HeadViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        int pos = getAdapterPosition();
                        mOnItemClickListener.onItemClick(v, pos);
                    }
                }
            });
        }
    }

    class LineViewHolder extends BaseViewHolder {

        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_value)
        TextView tvValue;

        public LineViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ButtonViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_order)
        Button btnOrder;

        public ButtonViewHolder(View itemView) {
            super(itemView);
        }

        @OnClick(R.id.btn_order)
        public void onClick() {
            if (mOnItemSubClickListener != null) {
                mOnItemSubClickListener.onItemSubClick(btnOrder, getAdapterPosition());
            }
        }
    }
}
