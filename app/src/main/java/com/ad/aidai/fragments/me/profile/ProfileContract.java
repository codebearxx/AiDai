package com.ad.aidai.fragments.me.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * LookProfileContract
 * <p>
 * Created by pure on 2017/1/12.
 */

public interface ProfileContract {

    interface View extends BaseView<Presenter> {

        /**
         * 修改昵称
         */
        void modifyName(Bundle bundle);

        /**
         * 绑定学校
         */
        void bindSchool();

        /**
         * 修改支付宝
         */
        void modifyPayNum(Bundle bundle);

        /**
         * 修改个性签名
         */
        void modifyPersonalNote(Bundle bundle);

        /**
         * 选择性别
         */
        void selectSex();

        /**
         * 选择头像
         */
        void chooseHeadImage();

        /**
         * 复制文字到剪切板
         */
        void copyWord(String content);

        /**
         * 显示加载对话框
         *
         * @param text
         */
        void showLoading(@NonNull String text);

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 显示标题
         *
         * @param title
         */
        void setTitle(String title);

    }

    interface Presenter extends BasePresenter {
        /**
         * 加载用户信息
         */
        void loadUserData();

        /**
         * 获取adapter
         *
         * @return
         */
        ProfileAdapter getAdapter();

        /**
         * 用户修改昵称
         */
        void userModifyName();

        /**
         * 用户绑定学校
         */
        void userBindSchool();

        /**
         * 用户修改支付宝
         */
        void userModifyPayNum();

        /**
         * 用户修改个性签名
         */
        void userModifyPersonalNote();

        /**
         * 用户修改头像
         */
        void userChangeHead();

        /**
         * 用户选择了那个性别
         *
         * @param sex
         */
        void userChangeSex(final int sex);

        /**
         * 上传头像
         *
         * @param path
         */
        void uploadHead(String path);

    }
}
