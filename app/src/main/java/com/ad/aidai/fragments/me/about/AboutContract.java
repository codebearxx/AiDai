package com.ad.aidai.fragments.me.about;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * Created by pure on 2017/2/10.
 */

public interface AboutContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示版本
         */
        void showVersion(String version);

        /**
         * 显示跨越的年
         */
        void showYear(String nowYear);

        /**
         * 显示加载提示框
         */
        void showLoading();

        /**
         * 关闭加载提示框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 显示更新提示框
         *
         * @param version
         * @param updateMessage
         */
        void showUpdateDialog(String version, String updateMessage);

    }

    interface Presenter extends BasePresenter {

        /**
         * 设置版本
         */
        void setVersion();

        /**
         * 设置当前年
         */
        void setNowYear();

        /**
         * 检查更新
         */
        void checkUpdate();

        /**
         * 更新app
         */
        void updateApp();

    }
}
