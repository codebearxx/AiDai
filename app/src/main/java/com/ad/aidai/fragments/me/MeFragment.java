package com.ad.aidai.fragments.me;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.activitys.login.LoginActivity;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.bean.User;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.common.widgets.imageview.CircleImageView;
import com.ad.aidai.common.widgets.photoview.DragPhotoView;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.fragments.me.about.AboutFragment;
import com.ad.aidai.fragments.me.bell_setting.BellSettingFragment;
import com.ad.aidai.fragments.me.profile.ProfileFragment;
import com.ad.aidai.fragments.me.push_setting.PushSettingFragment;
import com.ad.aidai.fragments.me.credits.CreditsFragment;
import com.ad.aidai.fragments.my_order.my_recieve_order.MyReceiveOrderFragment;
import com.ad.aidai.fragments.my_order.my_send_order.MySendOrderFragment;
import com.ad.aidai.fragments.my_state_order.MyStateOrderFragment;
import com.ad.aidai.network.ADUrl;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.NormalListDialog;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static android.content.Context.MODE_PRIVATE;

/**
 * 我的 fragment
 * <p>
 * Created by pure on 2016/12/8.
 */

public class MeFragment extends BaseFragment implements MeContract.View {

    @BindView(R.id.iv_flur_head)
    ImageView ivFlurHead;
    @BindView(R.id.civ_head)
    CircleImageView civHead;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.iv_sex)
    ImageView ivSex;
    @BindView(R.id.tv_star)
    TextView tvStar;
    @BindView(R.id.tv_profile)
    TextView tvProfile;

    private MeContract.Presenter mPresenter;
    private PushSettingFragment pushSettingFragment;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_me, container, false);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        new MePresenter(mContext, this).subscribe();

        mPresenter.loadUserData();
    }

    @Override
    protected void subscribeEvents() {
        //添加用户信息更新事件
        addSubscription(RxBus.getInstance()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        if (o instanceof CommonEvent) {
                            CommonEvent event = (CommonEvent) o;
                            switch (event.getEvent()) {
                                case CommonEvent.USER_SEX:
                                    mPresenter.userModifySex();
                                    break;
                                case CommonEvent.USER_NAME:
                                    mPresenter.userModifyName();
                                    break;
                                case CommonEvent.USER_PERSONAL_NOTE:
                                    mPresenter.userModifyPersonalNote();
                                    break;
                                case CommonEvent.USER_HEAD_CHANGE:
                                    mPresenter.userModifyHeadImage();
                                    break;
                                case CommonEvent.USER_UPDATE_CREDITS:
                                    mPresenter.updateStar();
                                    break;
                            }
                        }
                    }
                })
                .subscribe(RxBus.defaultSubscriber())
        );
    }

    /**
     * 点击头像
     */
    @OnClick(R.id.civ_head)
    public void onClickCivHead() {
        User user = ADApp.getUser();
        Long schoolId = user.getSchoolId();
        if (schoolId == null) {
            schoolId = 0L;
        }

        String url = ADUrl.HEAD_ICON_URL.replace("{schoolId}", schoolId + "").replace("{photoName}", user.getUserIcon());
        DragPhotoView.startDragPhotoActivity(mActivity, civHead, url);
    }

    /**
     * 点击我发的单
     */
    @OnClick(R.id.tv_my_sent_order)
    public void onClickTvMySentOrder() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new MySendOrderFragment(), "mySendOrder")
                .addToBackStack("mySendOrder")
                .commit();
    }

    /**
     * 点击我接的单
     */
    @OnClick(R.id.tv_my_received_order)
    public void onClickTvMyReceivedOrder() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new MyReceiveOrderFragment(), "myRecievedOrder")
                .addToBackStack("myRecievedOrder")
                .commit();
    }

    /**
     * 点击我申诉的单
     */
    @OnClick(R.id.tv_my_alleged_order)
    public void onClickTvMyAllegedOrder() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new MyStateOrderFragment(), "myStateOrder")
                .addToBackStack("myStateOrder")
                .commit();
    }

    /**
     * 点击我的积分
     */
    @OnClick(R.id.ll_star)
    public void onClickStar() {
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new CreditsFragment(), "star")
                .addToBackStack("star")
                .commit();
        //        Logger.t(this.getClass().getSimpleName()).i("click star");
    }

    /**
     * 点击我的资料
     */
    @OnClick(R.id.ll_personal_profile)
    public void onClickPersonalProfile() {
        MobclickAgent.onEvent(mActivity, "profile");
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new ProfileFragment(), "profile")
                .addToBackStack("profile")
                .commit();
    }

    /**
     * 点击推送设置
     */
    @OnClick(R.id.ll_push)
    public void onClickPush() {
        //        Logger.t(this.getClass().getSimpleName()).i("click push");
        pushSettingFragment = new PushSettingFragment();
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, pushSettingFragment, "push")
                .addToBackStack("push")
                .commit();
        //        mContext.getSharedPreferences("AIDAI", MODE_PRIVATE).edit().putBoolean("ISSETPUSH", false)
        //                .apply();
        //        RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.PUSH_SETTING_CHANGE));
    }

    /**
     * 点击消息与提醒
     */
    @OnClick(R.id.ll_bell)
    public void onClickBell() {
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new BellSettingFragment(), "bell")
                .addToBackStack("bell")
                .commit();
        //        Logger.t(this.getClass().getSimpleName()).i("click bell");
    }

    /**
     * 点击关于软件
     */
    @OnClick(R.id.ll_about)
    public void onClickAbout() {
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, new AboutFragment(), "about")
                .addToBackStack("about")
                .commit();
        //        Logger.t(this.getClass().getSimpleName()).i("click about");
    }

    /**
     * 点击退出
     */
    @OnClick(R.id.ll_exit)
    public void onClickExit() {
        final NormalListDialog dialog = new NormalListDialog(getContext(), new String[]{"退出登录", "退出程序"});
        dialog.title("请选择")
                .titleTextSize_SP(18)
                .titleBgColor(ContextCompat.getColor(getContext(), R.color.colorTheme))
                .itemPressColor(Color.parseColor("#85D3EF"))
                .itemTextColor(Color.parseColor("#303030"))
                .itemTextSize(14)
                .cornerRadius(0)
                .widthScale(0.8f)
                .setItemExtraPadding(6, 6, 6, 6)
                .show(R.style.myDialogAnim);

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mActivity.getSharedPreferences("AIDAI", MODE_PRIVATE).edit().putBoolean("IS_LOGIN",
                                false).apply();
                        mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
                    case 1:
                        mActivity.finish();
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
            }
        });
    }

    @Override
    public void setPresenter(MeContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setBlurHeadImage(Bitmap bitmap) {
        ivFlurHead.setImageBitmap(bitmap);
    }

    @Override
    public void setHeadImage(Bitmap bitmap) {
        civHead.setImageBitmap(bitmap);
    }

    @Override
    public void showStar(String star) {
        tvStar.setText(star);
    }

    @Override
    public void showUsername(String name) {
        tvUserName.setText(name);
    }

    @Override
    public void showUserSex(int which) {
        ivSex.setImageResource(which == 0 ? R.mipmap.icon_male : R.mipmap.icon_female);
    }

    @Override
    public void showProfile(String profile) {
        tvProfile.setText(profile);
    }

    @Override
    public boolean onActivityBackPressed() {
        return true;
    }
}
