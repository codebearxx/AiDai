package com.ad.aidai.fragments.me.profile.update_name;

import android.content.Context;

import com.ad.aidai.bean.User;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResult;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by pure on 2017/2/5.
 */

public class UpNamePresenter implements UpNameContract.Presenter {

    private Context mContext;
    private UpNameContract.View mView;
    private CompositeSubscription mSubscriptions;
    private boolean updateStatus = true;

    public UpNamePresenter(Context context, UpNameContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        initData();

    }

    private void initData() {

    }

    @Override
    public void saveName(final String name) {
        if ("".equals(name)) {
            mView.toast("请输入昵称");
            return;
        }
        mView.showLoading();
        NetWork.getAccountOperationApi()
                .updateUserName(ADApp.getUser().getUserId(), name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResult>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("updateUserName").e(e.getMessage());
                        //关闭加载对话框
                        mView.closeLoading(false);
                    }

                    @Override
                    public void onNext(NetResult netResult) {
                        if (netResult.getCode() != StatusCodeFromNet.CODE_SUCCESS) {
                            Logger.t("updateUserName").e(netResult.toString());
                            mView.closeLoading(false);
                        } else {
                            Logger.t("updateUserName").i("userName:" + name);
                            User user = ADApp.getUser();
                            user.setUserName(name);
                            MyUserDao.getUserDao().save(user);
                            RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.USER_NAME));
                            mView.closeLoading(true);
                            mView.modifySuccess();
                        }
                    }
                });
    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }
}
