package com.ad.aidai.fragments.my_order.my_send_order;

import android.support.annotation.NonNull;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.MyOrderBean;
import com.ad.aidai.bean.Order;
import com.ad.aidai.fragments.my_order.MyOrderAdapter;

import java.util.List;

/**
 * MyReceiveOrderContract
 * <p>
 * Created by Yusxon on 17/2/1.
 */

public interface MySendOrderContract {

    interface View extends BaseView<Presenter> {

        /**
         * 设置是否在刷新
         *
         * @param refreshing
         */
        void setOnRefresh(boolean refreshing);

        /**
         * 获取adapter
         *
         * @return
         */
        MyOrderAdapter getAdapter();

        /**
         * 跳转到评分界面
         */
        void gotoComment();

        /**
         * 显示提示框
         *
         * @param message
         * @param btnText
         */
        void showDialog(String message, String btnText);

        /**
         * 显示加载对话框
         *
         * @param text
         */
        void showLoading(@NonNull String text);

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

    }

    interface Presenter extends BasePresenter {

        /**
         * 获取订单信息
         *
         * @return
         */
        List<MyOrderBean> getOrderData();

        /**
         * 加载更多
         */
        void loadMoreOrders();

        /**
         * 刷新
         */
        void refresh();

        /**
         * 设置adapter
         */
        void setAdapter();

        /**
         * 点击item中的某个view
         *
         * @param view
         * @param position
         */
        void onItemSubClick(android.view.View view, int position);

        /**
         * 点击item
         *
         * @param position
         */
        void onItemClick(int position);

        /**
         * 点击提示框的确定按钮
         *
         * @param btnText
         */
        void clickDialogSure(String btnText);

        /**
         * 订单状态改变
         *
         * @param order
         */
        void orderStatusChange(Order order);
    }
}
