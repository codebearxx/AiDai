package com.ad.aidai.fragments.me.bell_setting;

import android.content.Context;
import android.content.SharedPreferences;

import rx.subscriptions.CompositeSubscription;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by pure on 2017/2/10.
 */

public class BellSettingPresenter implements BellSettingContract.Presenter {

    private Context mContext;
    private BellSettingContract.View mView;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences bell;

    public BellSettingPresenter(Context context, BellSettingContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        bell = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
        initData();

    }

    private void initData() {
        mView.showIsHaveVoice();
        mView.showIsHaveVibrate();

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void switchIsHaveVoice() {
        bell.edit()
                .putBoolean("new_message_voice",!bell.getBoolean("new_message_voice",true))
                .apply();
    }

    @Override
    public boolean getIsHaveVoiceLast() {
        return bell.getBoolean("new_message_voice",true);
    }

    @Override
    public void switchIsHaveVibrate() {
        bell.edit()
                .putBoolean("new_message_vibrate",!bell.getBoolean("new_message_vibrate",true))
                .apply();
    }

    @Override
    public boolean getIsHaveVibrateLast() {
        return bell.getBoolean("new_message_vibrate",true);
    }
}
