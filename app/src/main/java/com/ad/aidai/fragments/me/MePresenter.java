package com.ad.aidai.fragments.me;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.ad.aidai.R;
import com.ad.aidai.bean.User;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.utils.BitmapUtils;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.common.utils.FastBlur;
import com.ad.aidai.network.ADUrl;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.content.Context.MODE_PRIVATE;

/**
 * MePresenter
 * <p>
 * Created by Yusxon on 16/12/22.
 */

public class MePresenter implements MeContract.Presenter {

    private Context mContext;
    private MeContract.View mView;
    private CompositeSubscription mSubscriptions;
    private Gson gson;
    private SharedPreferences aidaiSP;
    private User user;

    public MePresenter(Context context, MeContract.View view) {
        this.mContext = context;
        this.mView = view;
        gson = new Gson();
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        aidaiSP = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void loadUserData() {
        user = ADApp.getUser();
        Long schoolId = user.getSchoolId();
        if (schoolId == null) {
            schoolId = 0L;
        }

        String url = ADUrl.HEAD_ICON_URL.replace("{schoolId}", schoolId + "").replace("{photoName}", user.getUserIcon());
        loadHeadImage(url);
        mView.showUsername(user.getUserName());
        mView.showUserSex(user.getUserSex());
        String note = user.getUserPersonalNote();
        if(CheckUtil.checkStrNull(note)){
            note = "太懒了，什么都不写!";
        }
        mView.showProfile(note);
        mView.showStar(user.getUserCredits() + "");
    }

    @Override
    public void userModifyName() {
        mView.showUsername(user.getUserName());
    }

    @Override
    public void userModifySex() {
        mView.showUserSex(user.getUserSex());
    }

    @Override
    public void userModifyPersonalNote() {
        String note = user.getUserPersonalNote();
        if(CheckUtil.checkStrNull(note)){
            note = "太懒了，什么都不写!";
        }
        mView.showProfile(note);
    }

    @Override
    public void userModifyHeadImage() {

        Long schoolId = user.getSchoolId();
        if (schoolId == null) {
            schoolId = 0L;
        }

        String url = ADUrl.HEAD_ICON_URL.replace("{schoolId}", schoolId + "").replace("{photoName}", user.getUserIcon());
        loadHeadImage(url);
    }

    @Override
    public void updateStar() {
        mView.showStar(user.getUserCredits().toString());
    }

    /**
     * 加载头像图片
     */
    private void loadHeadImage(String url) {
        Glide.with(mContext)
                .load(url)
                .asBitmap()
                .placeholder(R.mipmap.default_image)
                .error(R.mipmap.default_image)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(final Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        mView.setHeadImage(resource);
                        Observable
                                .create(new Observable.OnSubscribe<Bitmap>() {
                                    @Override
                                    public void call(Subscriber<? super Bitmap> subscriber) {
                                        float widthScale = resource.getWidth() / 200;
                                        float heightScale = resource.getHeight() / 200;
                                        float scale = widthScale > heightScale ? widthScale : heightScale;
                                        Bitmap temp = BitmapUtils.zoomByScale(resource, scale, scale);
                                        Bitmap newBitmap = FastBlur.doBlur(temp, 10, false);
                                        subscriber.onNext(newBitmap);
                                    }
                                })
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Subscriber<Bitmap>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        MobclickAgent.reportError(mContext, e);
                                        Logger.t("RegisterPresenter").e(e.getMessage());
                                    }

                                    @Override
                                    public void onNext(Bitmap o) {
                                        mView.setBlurHeadImage(o);
                                    }
                                });
                    }
                });
    }
}
