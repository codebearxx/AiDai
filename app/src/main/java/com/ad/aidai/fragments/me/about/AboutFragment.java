package com.ad.aidai.fragments.me.about;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.activitys.feedback.FeedbackActivity;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.widgets.loading_dialog.LoadingDialog;
import com.ad.aidai.fragments.service_item.ServiceItemFragment;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideTopExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 关于软件页面
 * <p>
 * Created by pure on 2017/2/10.
 */

public class AboutFragment extends BaseFragment implements AboutContract.View {
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.tv_copyright)
    TextView tvCopyright;

    private AboutContract.Presenter mPresenter;
    private LoadingDialog loadingDialog;
    private BaseAnimatorSet mBasIn;
    private BaseAnimatorSet mBasOut;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_about, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        new AboutPresenter(mContext, this).subscribe();
        mBasIn = new BounceTopEnter();
        mBasOut = new SlideTopExit();

    }

    @Override
    public void setPresenter(AboutContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back, R.id.ll_service_item, R.id.ll_software_feedback, R.id.ll_check_update})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
            case R.id.ll_service_item:
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                                .fragment_slide_left_exit)
                        .add(android.R.id.content, new ServiceItemFragment(), "service_item")
                        .addToBackStack("service_item")
                        .commit();
                break;
            case R.id.ll_software_feedback:
                mActivity.startActivity(new Intent(mContext, FeedbackActivity.class));
                break;
            case R.id.ll_check_update:
                mPresenter.checkUpdate();
                break;
        }
    }

    @Override
    public void showVersion(String version) {
        tvVersion.setText(version);
    }

    @Override
    public void showYear(String nowYear) {
        tvCopyright.setText("爱代团队 版权所有\nCopyRight @ 2016-" + nowYear + " All Rights Reserved");
    }

    @Override
    public void showLoading() {
        //显示加载对话框
        loadingDialog = new LoadingDialog(mActivity);
        loadingDialog.setLoadingText("正在检测...")
                .setFailedText("检测失败")
                .setSuccessText("检测成功")
                .setShowTime(200)
                .show();
    }

    @Override
    public void closeLoading(boolean success) {
        if (loadingDialog != null) {
            if (success) {
                loadingDialog.loadSuccess();
            } else {
                loadingDialog.loadFailed();
            }
        }
    }

    @Override
    public void showUpdateDialog(String version, String updateMessage) {
        final NormalDialog dialog = new NormalDialog(mContext);
        dialog.title("检测到最新版本: " + version)
                .titleTextSize(18)
                .content(updateMessage)
                .btnText("稍后更新", "立即更新")
                .showAnim(mBasIn)
                .dismissAnim(mBasOut)
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        mPresenter.updateApp();
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public boolean onActivityBackPressed() {
        getFragmentManager().popBackStack();
        return false;
    }
}
