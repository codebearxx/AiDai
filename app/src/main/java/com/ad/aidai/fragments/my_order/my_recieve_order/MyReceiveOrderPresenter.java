package com.ad.aidai.fragments.my_order.my_recieve_order;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.ad.aidai.R;
import com.ad.aidai.bean.MyOrderBean;
import com.ad.aidai.bean.Order;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.fragments.my_order.MyOrderAdapter;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.GetMyOrderFromNet;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * MyReceiveOrderPresenter
 * <p>
 * Created by Yusxon on 17/2/2.
 */

public class MyReceiveOrderPresenter implements MyReceiveOrderContract.Presenter {

    private Context mContext;
    private MyReceiveOrderContract.View mView;
    private CompositeSubscription mSubscriptions;

    private List<MyOrderBean> mOrderData;
    private MyOrderAdapter mAdapter;

    /**
     * 最多显示一次加载多少数据
     */
    private static final int PAGE_SIZE = 10;
    /**
     * 当前加载第几页
     */
    private int currentPage = 1;

    public MyReceiveOrderPresenter(Context context, MyReceiveOrderContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        mOrderData = new ArrayList<>();
    }

    @Override
    public List<MyOrderBean> getOrderData() {
        return mOrderData;
    }

    @Override
    public void loadMoreOrders() {
        NetWork.getAccountOperationApi()
                .getMyReceiveOrder(ADApp.getUser().getUserId(), PAGE_SIZE, currentPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetMyOrderFromNet>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("MySendOrderPresenter").e(e.getMessage());
                        mView.toast("加载失败");
                        mAdapter.stopMore();
                    }

                    @Override
                    public void onNext(GetMyOrderFromNet result) {
                        Logger.t("MySendOrderPresenter").i("loadMoreOrders:" + result.toString());
                        if (result.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            List<Order> orders = result.getData();
                            Logger.t("MySendOrderPresenter").i(orders.size() + "--");
                            currentPage++;
                            List<MyOrderBean> myOrderBeen = new ArrayList<>();
                            for (Order order : orders) {
                                MyOrderBean bean = new MyOrderBean();
                                bean.setValue(order);
                                bean.setUIType(MyOrderBean.UITYPE_RECIEVED);
                                switch (order.getOrderStatus()) {
                                    case Order.STATUS_CANCEL:
                                        bean.setType(MyOrderBean.VIEWTYPE_TOP);
                                        break;
                                    case Order.STATUS_NO_ACCEPTANCE:
                                    case Order.STATUS_RECIEVER_NO_SCORE:
                                    case Order.STATUS_NO_SCORE:
                                        bean.setType(MyOrderBean.VIEWTYPE_COMMON);
                                        break;
                                    case Order.STATUS_SENDER_STATE:
                                    case Order.STATUS_STATE:
                                        if (order.getReceiverComment() == null) {
                                            bean.setType(MyOrderBean.VIEWTYPE_TOP);
                                        }
                                        break;
                                    default:
                                        bean.setType(MyOrderBean.VIEWTYPE_COMPLETE);
                                        break;
                                }
                                myOrderBeen.add(myOrderBeen.size(), bean);
                            }
                            mAdapter.addAll(myOrderBeen);
                            if (orders.size() == 0) {
                                //暂停加载,暂时改为,停止下载则底部变成没有更多了且不可以点击
                                mAdapter.pauseMore();
                            } else {
                                //停止加载,底部变为点击加载更多
                                mAdapter.stopMore();
                            }
                        } else {
                            mView.toast("加载失败");
                            Logger.t("MySendOrderPresenter").e(result.toString());
                            //停止加载,底部变为点击加载更多
                            mAdapter.stopMore();
                        }
                    }
                });
    }

    @Override
    public void refresh() {
        mView.setOnRefresh(true);
        NetWork.getAccountOperationApi()
                .getMyReceiveOrder(ADApp.getUser().getUserId(), PAGE_SIZE, 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetMyOrderFromNet>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("OrderAllSchoolPresenter").e(e.getMessage());
                        mView.toast("刷新失败");
                        mView.setOnRefresh(false);
                        mAdapter.add(null);
                    }

                    @Override
                    public void onNext(GetMyOrderFromNet result) {
                        Logger.t("OrderAllSchoolPresenter").i("refreshOrders:" + result.toString());
                        mView.setOnRefresh(false);
                        if (result.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            List<Order> orders = result.getData();
                            Logger.t("OrderAllSchoolPresenter").i(orders.size() + "--");
                            currentPage = 2;
                            mAdapter.clear();
                            List<MyOrderBean> myOrderBeen = new ArrayList<>();
                            for (Order order : orders) {
                                MyOrderBean bean = new MyOrderBean();
                                bean.setValue(order);
                                bean.setUIType(MyOrderBean.UITYPE_RECIEVED);
                                switch (order.getOrderStatus()) {
                                    case Order.STATUS_CANCEL:
                                        bean.setType(MyOrderBean.VIEWTYPE_TOP);
                                        break;
                                    case Order.STATUS_NO_ACCEPTANCE:
                                    case Order.STATUS_RECIEVER_NO_SCORE:
                                    case Order.STATUS_NO_SCORE:
                                        bean.setType(MyOrderBean.VIEWTYPE_COMMON);
                                        break;
                                    case Order.STATUS_SENDER_STATE:
                                    case Order.STATUS_STATE:
                                        if (order.getReceiverComment() == null) {
                                            bean.setType(MyOrderBean.VIEWTYPE_TOP);
                                        }
                                        break;
                                    default:
                                        bean.setType(MyOrderBean.VIEWTYPE_COMPLETE);
                                        break;
                                }
                                myOrderBeen.add(myOrderBeen.size(), bean);
                            }
                            mAdapter.addAll(myOrderBeen);
                            mAdapter.stopMore();
                        } else {
                            mView.toast("刷新失败");
                            mAdapter.add(null);
                            Logger.t("OrderAllSchoolPresenter").e(result.toString());
                        }
                    }
                });
    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
        mSubscriptions = null;
    }

    @Override
    public void setAdapter() {
        this.mAdapter = mView.getAdapter();
    }

    @Override
    public void onItemSubClick(View view, int position) {
        switch (view.getId()) {
            case R.id.btn_my_order_button:
                Button btn = (Button) view;
                switch (btn.getText().toString()) {
                    case "评分":
                        ADApp.setOrder(mAdapter.getItem(position).getValue());
                        mView.gotoComment();
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        ADApp.setOrder(mAdapter.getItem(position).getValue());
    }

    @Override
    public void orderStatusChange(Order order) {
        if (mAdapter != null && mAdapter.getCount() > 0) {
            int position = 0;
            for (MyOrderBean orderBean : mAdapter.getAllData()) {
                Order oldOrder = orderBean.getValue();
                if (oldOrder.getOrderId() == order.getOrderId()) {
                    orderBean.setValue(order);
                    switch (order.getOrderStatus()) {
                        case Order.STATUS_NO_ACCEPTANCE:
                        case Order.STATUS_RECIEVER_NO_SCORE:
                        case Order.STATUS_NO_SCORE:
                            orderBean.setType(MyOrderBean.VIEWTYPE_COMMON);
                            break;
                        case Order.STATUS_SENDER_STATE:
                        case Order.STATUS_STATE:
                            if (order.getReceiverComment() == null) {
                                orderBean.setType(MyOrderBean.VIEWTYPE_TOP);
                            }
                            break;
                        default:
                            orderBean.setType(MyOrderBean.VIEWTYPE_COMPLETE);
                            break;
                    }
                    mAdapter.notifyItemChanged(position);
                    break;
                }
                position++;
            }
        }
    }
}
