package com.ad.aidai.fragments.me.profile.update_pay_num;

import android.content.Context;

import com.ad.aidai.bean.User;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResult;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by pure on 2017/2/5.
 */

public class UpPayNumPresenter implements UpPayNumContract.Presenter {

    private Context mContext;
    private UpPayNumContract.View mView;
    private CompositeSubscription mSubscriptions;

    public UpPayNumPresenter(Context context, UpPayNumContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        initData();

    }

    private void initData() {

    }

    /**
     * 确定支付宝账号
     *
     * @param payNum
     */
    private void ensurePayNum(final String payNum) {

        mView.showLoading();
        NetWork.getAccountOperationApi()
                .updateUserPayNum(ADApp.getUser().getUserId(), payNum)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResult>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("updateUserPayNum").e(e.getMessage());
                        //关闭加载对话框
                        mView.closeLoading(false);
                    }

                    @Override
                    public void onNext(NetResult netResult) {
                        if (netResult.getCode() != StatusCodeFromNet.CODE_SUCCESS) {
                            Logger.t("updateUserPayNum").e(netResult.toString());
                            mView.closeLoading(false);
                        } else {
                            Logger.t("updateUserPayNum").i("updateUserPayNum:" + payNum);
                            User user = ADApp.getUser();
                            user.setUserPayNum(payNum);
                            MyUserDao.getUserDao().save(user);
                            RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.USER_PAY_NUM));
                            mView.modifySuccess();
                            mView.closeLoading(true);
                        }
                    }
                });
    }

    @Override
    public void savePayNum(String payNum) {
        if (!CheckUtil.checkStrNull(payNum) && (CheckUtil.checkPhone(payNum) || CheckUtil.checkMail(payNum))) {
            ensurePayNum(payNum);
        } else {
            mView.toast("请输入有效的支付宝账号");
        }

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }
}
