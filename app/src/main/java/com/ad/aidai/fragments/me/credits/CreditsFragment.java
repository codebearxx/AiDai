package com.ad.aidai.fragments.me.credits;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by pure on 2017/2/7.
 */

public class CreditsFragment extends BaseFragment implements CreditsContract.View {
    @BindView(R.id.tv_star)
    TextView tvStar;
    @BindView(R.id.wv_star)
    WebView wvStar;

    private CreditsContract.Presenter mPresenter;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_star, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        new CreditsPresenter(mContext, this).subscribe();

    }

    @Override
    public void setPresenter(CreditsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showStar(String star) {
        tvStar.setText(star);
    }

    @Override
    public void showStarExplainContent(String url) {
        wvStar.loadUrl(url);
    }

    @OnClick({R.id.iv_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
        }
    }

    @Override
    public boolean onActivityBackPressed() {
        getFragmentManager().popBackStack();
        return false;
    }
}
