package com.ad.aidai.fragments.me.profile.update_name;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * Created by pure on 2017/2/5.
 */

public interface UpNameContract {

    interface View extends BaseView<Presenter> {

        /**
         * 修改昵称成功
         */
        void modifySuccess();

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

    }

    interface Presenter extends BasePresenter {
        /**
         * 保存昵称
         */
        void saveName(String name);

    }
}
