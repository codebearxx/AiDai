package com.ad.aidai.fragments.me.profile.update_pay_num;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * Created by pure on 2017/2/5.
 */

public interface UpPayNumContract {
    interface View extends BaseView<Presenter> {

        /**
         * 提示信息
         *
         * @param message 信息
         */
        void toast(String message);

        /**
         * 修改支付宝成功
         */
        void modifySuccess();

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

    }

    interface Presenter extends BasePresenter {
        /**
         * 保存支付宝
         */
        void savePayNum(String payNum);

    }
}
