package com.ad.aidai.fragments.me.push_setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Gravity;

import com.mingle.entity.MenuEntity;

import java.util.ArrayList;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by pure on 2017/2/11.
 */

public class PushSettingPresenter implements PushSettingContract.Presenter {

    private Context mContext;
    private PushSettingContract.View mView;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences push;
    private List<MenuEntity> pushIntervalMenuEntity;

    private String[] pInterval = {"30秒", "1分钟", "2分钟", "3分钟", "4分钟", "5分钟", "10分钟"};
    private Long[] times = {30L, 60L, 120L, 180L, 240L, 300L, 600L};

    public PushSettingPresenter(Context context, PushSettingContract.View view) {
        this.mContext = context;
        this.mView = view;
        mView.setPresenter(this);
        push = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
        initData();

    }

    private void initData() {

        pushIntervalMenuEntity = new ArrayList<>();
        for (String time : pInterval) {
            pushIntervalMenuEntity.add(new MenuEntity(time).setGravity(Gravity.CENTER));
        }

        mView.showIsPush();
        if (getIsPushLast()) {
            mView.isShowPushInterval(true);
            mView.showPushInterval(getPushIntervalValue(getPushIntervalLast()));
        } else {
            mView.isShowPushInterval(false);
        }

    }

    @Override
    public void subscribe() {
        mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void switchIsPush() {
        push.edit()
                .putBoolean("ISSETPUSH", !push.getBoolean("ISSETPUSH", true))
                .apply();
        mView.postEventPush();
    }

    @Override
    public boolean getIsPushLast() {
        return push.getBoolean("ISSETPUSH", false);
    }

    @Override
    public void setPushInterval(int interval) {
        push.edit()
                .putInt("pushInterval", interval)
                .putLong("pushTimes", times[interval])
                .apply();
        mView.showPushInterval(getPushIntervalValue(interval));
    }

    @Override
    public List<MenuEntity> getPushIntervalMenuEntity() {
        return pushIntervalMenuEntity;
    }

    @Override
    public int getPushIntervalLast() {
        return push.getInt("pushInterval", 0);
    }

    @Override
    public void switchIsLocation() {
        push.edit()
                .putBoolean("isLocation", !push.getBoolean("isLocation", true))
                .apply();
        mView.postEventLocation();
    }

    @Override
    public boolean getIsLocationLast() {
        return push.getBoolean("isLocation", true);
    }

    private String getPushIntervalValue(int interval) {
        return pInterval[interval];
    }
}
