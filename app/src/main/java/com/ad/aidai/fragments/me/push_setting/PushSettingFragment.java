package com.ad.aidai.fragments.me.push_setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.common.widgets.RippleLayout;
import com.ad.aidai.common.widgets.jelly_toggle_button.JellyToggleButton;
import com.ad.aidai.events.CommonEvent;
import com.mingle.entity.MenuEntity;
import com.mingle.sweetpick.DimEffect;
import com.mingle.sweetpick.RecyclerViewDelegate;
import com.mingle.sweetpick.SweetSheet;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by pure on 2017/2/11.
 */

public class PushSettingFragment extends BaseFragment implements PushSettingContract.View {
    @BindView(R.id.ll_push_setting)
    RelativeLayout llPushSetting;
    @BindView(R.id.s_accept_push)
    JellyToggleButton sAcceptPush;
    @BindView(R.id.tv_push_interval_value)
    TextView tvPushIntervalValue;
    @BindView(R.id.rl_push_interval)
    RippleLayout rlPushInterval;
    @BindView(R.id.v_push_tail)
    View vPushTail;

    private SweetSheet pushIntervalSweetSheet;
    private PushSettingContract.Presenter mPresenter;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_push_setting, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件

    }

    @Override
    protected void initView() {
        sAcceptPush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    isShowPushInterval(true);
                    mPresenter.setPushInterval(mPresenter.getPushIntervalLast());
                }else{
                    isShowPushInterval(false);
                }
                mPresenter.switchIsPush();
            }
        });

        pushIntervalSweetSheet = new SweetSheet(llPushSetting);
        //设置数据源 (数据源支持设置 list 数组,也支持从菜单中获取)
        pushIntervalSweetSheet.setMenuList(mPresenter.getPushIntervalMenuEntity());
        //根据设置不同的 Delegate 来显示不同的风格.
        pushIntervalSweetSheet.setDelegate(new RecyclerViewDelegate(true));
        //根据设置不同Effect 来显示背景效果BlurEffect:模糊效果.DimEffect 变暗效果
        pushIntervalSweetSheet.setBackgroundEffect(new DimEffect(6));
        //设置点击事件
        pushIntervalSweetSheet.setOnMenuItemClickListener(new SweetSheet.OnMenuItemClickListener() {
            @Override
            public boolean onItemClick(int position, MenuEntity menuEntity1) {
                mPresenter.setPushInterval(position);
                return true;
            }
        });

    }

    @Override
    protected void initData() {
        new PushSettingPresenter(mContext, this).subscribe();

    }

    @Override
    public void setPresenter(PushSettingContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back,R.id.ll_push_interval})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
            case R.id.ll_push_interval:
                pushIntervalSweetSheet.toggle();
                break;
        }
    }

    @Override
    public void showIsPush() {
        if(mPresenter.getIsPushLast()){
            sAcceptPush.toggle();
        }
    }

    @Override
    public void postEventPush() {
        RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.PUSH_SETTING_CHANGE));
    }

    @Override
    public void postEventLocation() {
        RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.LOCATION_SETTING_CHANGE));
    }

    @Override
    public void showPushInterval(String interval) {
        tvPushIntervalValue.setText(interval);
    }

    @Override
    public void isShowPushInterval(boolean isShow) {
        int visibility;
        if(isShow){
            visibility=View.VISIBLE;
        }else{
            visibility=View.GONE;
        }
        rlPushInterval.setVisibility(visibility);
        vPushTail.setVisibility(visibility);
    }

    @Override
    public boolean onActivityBackPressed() {
        if (pushIntervalSweetSheet.isShow() ) {
            pushIntervalSweetSheet.dismiss();
        } else {
            getFragmentManager().popBackStack();
        }
        return false;
    }
}
