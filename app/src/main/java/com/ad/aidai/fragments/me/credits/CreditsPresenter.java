package com.ad.aidai.fragments.me.credits;

import android.app.Activity;
import android.content.Context;

import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.network.ADUrl;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResultWithData;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import java.util.Timer;
import java.util.TimerTask;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by pure on 2017/2/7.
 */

public class CreditsPresenter implements CreditsContract.Presenter {

    private Context mContext;
    private CreditsContract.View mView;
    private CompositeSubscription mSubscriptions;

    public CreditsPresenter(Context context, CreditsContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        initData();

    }

    private void initData() {
        setStar();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setStarExplainContent();
                        loadCredits();
                    }
                });
            }
        }, 800);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    private void setStar() {
        String star = ADApp.getUser().getUserCredits().toString();
        mView.showStar(star);
    }

    @Override
    public void setStarExplainContent() {
        String url = ADUrl.BASE_URL + ADUrl.STAR_EXPLAIN;
        mView.showStarExplainContent(url);
    }

    private void loadCredits() {
        NetWork.getAccountOperationApi()
                .getUserCredits(ADApp.getUser().getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResultWithData<Integer>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("LoginPresenter").e(e.getMessage());
                        mView.toast("积分刷新失败");
                    }

                    @Override
                    public void onNext(NetResultWithData<Integer> resultWithData) {
                        if (resultWithData.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            int userCredits = resultWithData.getData();
                            mView.showStar(String.valueOf(userCredits));
                            RxBus.getInstance().postEvent(new CommonEvent<>(CommonEvent.USER_UPDATE_CREDITS));
                            ADApp.getUser().setUserCredits(userCredits);
                            MyUserDao.getUserDao().update(ADApp.getUser());
                            ADApp.setUser();
                        } else {
                            mView.toast("积分刷新失败");
                        }
                    }
                });
    }
}
