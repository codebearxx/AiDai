package com.ad.aidai.fragments.bind_school;

import android.content.Context;
import android.content.SharedPreferences;

import com.ad.aidai.bean.School;
import com.ad.aidai.bean.User;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.BindSchoolFromNet;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.content.Context.MODE_PRIVATE;

/**
 * BindSchoolPresenter
 * <p>
 * Created by pure on 2017/1/18.
 */

public class BindSchoolPresenter implements BindSchoolContract.Presenter {

    private Context mContext;
    private BindSchoolContract.View mView;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences aidaiSP;
    private User user;

    private School school;

    public BindSchoolPresenter(Context context, BindSchoolContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        aidaiSP = mContext.getSharedPreferences("AIDAI", MODE_PRIVATE);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void bindSchool(final long schoolId, final String schoolName, final String stuNum, String pass) {
        if (schoolId==0) {
            mView.toast("请先选择学校");
        } else if (CheckUtil.checkStrNull(stuNum) || CheckUtil.checkStrNull(pass)) {
            mView.toast("学号和密码不能为空");
        } else {
            mView.showLoading();
            Logger.t("bindSchool").e(ADApp.getUser().getUserId() + "--" + stuNum + "--" + pass + "--" + (int)schoolId);
            NetWork.getAccountOperationApi()
                    .bindSchool(ADApp.getUser().getUserId(), stuNum, pass, (int)schoolId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<BindSchoolFromNet>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            MobclickAgent.reportError(mContext, e);
                            Logger.t("bindSchool").e(e.getMessage());
                            //关闭加载对话框
                            mView.closeLoading(false);
                        }

                        @Override
                        public void onNext(BindSchoolFromNet bindSchoolFromNet) {
                            Logger.t("bindSchoolTest").e(bindSchoolFromNet.toString());
                            if (bindSchoolFromNet.getCode() != StatusCodeFromNet.CODE_SUCCESS) {
                                if (bindSchoolFromNet.getCode() == StatusCodeFromNet.CODE_STU_ERROR) {
                                    mView.toast("账号或密码错误");
                                }
                                if (bindSchoolFromNet.getCode() == StatusCodeFromNet.CODE_SCHOOL_HAVE_BIND) {
                                    mView.toast("此账号已被绑定");
                                }
                                Logger.t("bindSchool").e(bindSchoolFromNet.toString());
                                mView.closeLoading(false);
                            } else {
                                User u = bindSchoolFromNet.getData();
                                Logger.t("u--------").e(u.toString());
                                User user = ADApp.getUser();
                                user.setSchoolId(schoolId);
                                user.setSchoolName(schoolName);
                                user.setUserRealName(u.getUserRealName());
                                user.setUserStuNumber(u.getUserStuNumber());
                                user.setUserGrade(u.getUserGrade());
                                MyUserDao.getUserDao().save(user);
                                ADApp.loadSchoolPlace();
                                RxBus.getInstance().postEvent(new CommonEvent(CommonEvent.SCHOOL_INFO_BIND));
                                mView.closeLoading(true);
                                mView.bindSuccess();
                            }
                        }
                    });
        }
    }

    @Override
    public void selectSchool(School school) {
        this.school = school;
        mView.showSelectedSchool(school.getSchoolName());
    }
}
