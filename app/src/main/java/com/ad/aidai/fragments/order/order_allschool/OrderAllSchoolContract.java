package com.ad.aidai.fragments.order.order_allschool;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.Order;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;

/**
 * OrderAllSchoolContract
 *
 * Created by Yusxon on 16/12/24.
 */

public interface OrderAllSchoolContract {

    interface View extends BaseView<Presenter> {

        /**
         * 进入item详细信息页面
         */
        void intoItem();
    }

    interface Presenter extends BasePresenter {
        /**
         * 获取adapter
         *
         * @return
         */
        RecyclerArrayAdapter<Order> getAdapter();

        /**
         * 刷新
         */
        void refresh();

        /**
         * 订单状态改变
         *
         * @param order
         */
        void orderStatusChange(Order order);
    }
}
