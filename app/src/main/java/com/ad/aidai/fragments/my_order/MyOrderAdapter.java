package com.ad.aidai.fragments.my_order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.bean.MyOrderBean;
import com.ad.aidai.bean.Order;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.listener.RecyclerViewListener;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * MyOrderAdapter
 * <p>
 * Created by Yusxon on 17/2/2.
 */

public class MyOrderAdapter extends RecyclerArrayAdapter<MyOrderBean> {

    private RecyclerViewListener.OnRecyclerViewItemSubClickListener mOnItemSubClickListener = null;

    public void setmOnItemSubClickListener(RecyclerViewListener.OnRecyclerViewItemSubClickListener
                                                   mOnItemSubClickListener) {
        this.mOnItemSubClickListener = mOnItemSubClickListener;
    }

    public MyOrderAdapter(Context context, List<MyOrderBean> objects) {
        super(context, objects);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder;
        switch (viewType) {
            case MyOrderBean.VIEWTYPE_COMMON:
                holder = new CommonViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_my_order, parent, false));
                break;
            case MyOrderBean.VIEWTYPE_COMPLETE:
                holder = new CompleteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_my_order_complete, parent, false));
                break;
            default:
                holder = new TopViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_my_order_top, parent, false));
                break;
        }

        return holder;
    }

    @Override
    public void OnBindViewHolder(BaseViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case MyOrderBean.VIEWTYPE_TOP:
                TopViewHolder topViewHolder = (TopViewHolder) holder;
                MyOrderBean orderBean = mObjects.get(position);
                Order order = orderBean.getValue();

                topViewHolder.tvOrderType.setText(topViewHolder.orderType[order.getOrderType()]);
                topViewHolder.tvOrderTime.setText(order.getOrderSendTime());
                String status;
                if (order.getOrderStatus() == Order.STATUS_STATE) {
                    status = "被申述";
                } else {
                    if (orderBean.getUIType() == MyOrderBean.UITYPE_RECIEVED) {
                        if (order.getOrderStatus() == Order.STATUS_SENDER_STATE) {
                            status = "被申述";
                        } else {
                            status = topViewHolder.recievedOrderStatus[order.getOrderStatus()];
                        }
                    } else {
                        if (order.getOrderStatus() == Order.STATUS_RECIEVER_STATE) {
                            status = "被申述";
                        } else {
                            status = topViewHolder.sendOrderStatus[order.getOrderStatus()];
                        }
                    }
                }

                topViewHolder.tvOrderStatus.setText(status);

                topViewHolder.tvOrderContent.setText(order.getOrderContent());
                topViewHolder.tvOrderMoney.setText(String.valueOf(order.getOrderReward()));

                break;
            case MyOrderBean.VIEWTYPE_COMMON:
                CommonViewHolder commonViewHolder = (CommonViewHolder) holder;
                MyOrderBean orderBean2 = mObjects.get(position);
                Order order2 = orderBean2.getValue();

                commonViewHolder.tvOrderType.setText(commonViewHolder.orderType[order2.getOrderType()]);
                commonViewHolder.tvOrderTime.setText(order2.getOrderSendTime());

                String status2 = commonViewHolder.sendOrderStatus[order2.getOrderStatus()];
                if (orderBean2.getUIType() == MyOrderBean.UITYPE_RECIEVED) {
                    status2 = commonViewHolder.recievedOrderStatus[order2.getOrderStatus()];
                    commonViewHolder.tvPersonWho.setText("发单人：");
                    switch (order2.getOrderStatus()) {
                        case Order.STATUS_NO_SCORE:
                        case Order.STATUS_RECIEVER_NO_SCORE:
                            commonViewHolder.btnMyOrderButton.setText("评分");
                            break;

                    }
                } else {
                    switch (order2.getOrderStatus()) {
                        case Order.STATUS_NO_SCORE:
                        case Order.STATUS_SENDER_NO_SCORE:
                            commonViewHolder.btnMyOrderButton.setText("评分");
                            break;

                    }
                }

                commonViewHolder.tvOrderStatus.setText(status2);
                commonViewHolder.tvOrderContent.setText(order2.getOrderContent());
                commonViewHolder.tvOrderMoney.setText(String.valueOf(order2.getOrderReward()));
                switch (order2.getOrderStatus()) {
                    case Order.STATUS_NO_RECIEVE:
                        commonViewHolder.llOrderPerson.setVisibility(View.GONE);
                        commonViewHolder.btnMyOrderButton.setText("撤销");
                        break;
                    case Order.STATUS_CANCEL:
                        break;
                    case Order.STATUS_NO_ACCEPTANCE:
                        commonViewHolder.btnMyOrderButton.setText("完成");
                        if (order2.getOrderReceiveId() == ADApp.getUser().getUserId()) {
                            commonViewHolder.btnMyOrderButton.setVisibility(View.GONE);
                        }
                        break;

                }
                if (order2.getOrderStatus() != Order.STATUS_NO_RECIEVE) {
                    switch (orderBean2.getUIType()) {
                        case MyOrderBean.UITYPE_SEND:
                            commonViewHolder.tvOrderPerson.setText(order2.getReceiveUser().getUserName());
                            break;
                        case MyOrderBean.UITYPE_RECIEVED:
                            commonViewHolder.tvOrderPerson.setText(order2.getSendUser().getUserName());
                            break;
                        default:
                            break;
                    }
                }

                break;
            case MyOrderBean.VIEWTYPE_COMPLETE:
                CompleteViewHolder completeViewHolder = (CompleteViewHolder) holder;
                MyOrderBean orderBean3 = mObjects.get(position);
                Order order3 = orderBean3.getValue();

                completeViewHolder.tvOrderType.setText(completeViewHolder.orderType[order3.getOrderType()]);
                completeViewHolder.tvOrderTime.setText(order3.getOrderSendTime());

                String status3 = completeViewHolder.sendOrderStatus[order3.getOrderStatus()];
                int score;
                if (orderBean3.getUIType() == MyOrderBean.UITYPE_RECIEVED) {
                    status3 = completeViewHolder.recievedOrderStatus[order3.getOrderStatus()];
                    completeViewHolder.tvPersonWho.setText("发单人：");
                    completeViewHolder.tvReceivedOrderPerson.setText(order3.getSendUser().getUserName());
                    score = order3.getReceiverComment().getCommentScore();
                } else {
                    completeViewHolder.tvReceivedOrderPerson.setText(order3.getReceiveUser().getUserName());
                    score = order3.getSenderComment().getCommentScore();
                }

                for (int i = 0; i < 5 - score; ++i) {
                    completeViewHolder.stars[i].setVisibility(View.GONE);
                }
                completeViewHolder.tvOrderStatus.setText(status3);

                completeViewHolder.tvOrderContent.setText(order3.getOrderContent());
                completeViewHolder.tvOrderMoney.setText(String.valueOf(order3.getOrderReward()));

                break;
            default:
                break;
        }
    }

    @Override
    public int getViewType(int position) {
        return mObjects.get(position).getType();
    }

    class SpaceViewHolder extends BaseViewHolder {

        public SpaceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TopViewHolder extends SpaceViewHolder {

        @BindView(R.id.tv_order_type)
        TextView tvOrderType;
        @BindView(R.id.tv_order_time)
        TextView tvOrderTime;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_order_content)
        TextView tvOrderContent;
        @BindView(R.id.tv_order_money)
        TextView tvOrderMoney;

        String[] orderType = {"代拿", "代买", "代办"};
        String[] sendOrderStatus = {"未被接", "待验收", "已完成", "待评分", "待评分", "已完成", "已撤销", "已完成", "已完成", "已完成"};
        String[] recievedOrderStatus = {"", "待完成", "待评分", "已完成", "待评分", "已完成", "", "已完成", "已完成", "已完成"};

        public TopViewHolder(View itemView) {
            super(itemView);
        }
    }

    class CommonViewHolder extends TopViewHolder {

        @BindView(R.id.ll_order_person)
        LinearLayout llOrderPerson;
        @BindView(R.id.tv_order_person)
        TextView tvOrderPerson;
        @BindView(R.id.btn_my_order_button)
        Button btnMyOrderButton;
        @BindView(R.id.tv_person_who)
        TextView tvPersonWho;

        public CommonViewHolder(View itemView) {
            super(itemView);
        }

        @OnClick(R.id.btn_my_order_button)
        public void onClick() {
            if (mOnItemSubClickListener != null) {
                mOnItemSubClickListener.onItemSubClick(btnMyOrderButton, getAdapterPosition());
            }
        }
    }

    class CompleteViewHolder extends TopViewHolder {

        @BindView(R.id.ll_received_order_person)
        LinearLayout llReceivedOrderPerson;
        @BindView(R.id.tv_received_order_person)
        TextView tvReceivedOrderPerson;
        @BindView(R.id.tv_person_who)
        TextView tvPersonWho;
        @BindViews({R.id.iv_start_1, R.id.iv_start_2, R.id.iv_start_3, R.id.iv_start_4, R.id.iv_start_5})
        ImageView[] stars;

        public CompleteViewHolder(View itemView) {
            super(itemView);
        }
    }

}
