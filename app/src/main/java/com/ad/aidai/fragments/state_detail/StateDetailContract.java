package com.ad.aidai.fragments.state_detail;

import android.support.annotation.ColorRes;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * StateDetailContract
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public interface StateDetailContract {

    interface View extends BaseView<Presenter> {
        /**
         * 显示申述单号
         *
         * @param stateNumber
         */
        void showStateNumber(String stateNumber);

        /**
         * 显示申述日期
         *
         * @param stateTime
         */
        void showStateTime(String stateTime);

        /**
         * 显示订单类型
         *
         * @param orderType
         */
        void showOrderType(String orderType);

        /**
         * 显示订单时间
         *
         * @param orderTime
         */
        void showOrderTime(String orderTime);

        /**
         * 显示订单状态
         *
         * @param orderStatus
         */
        void showOrderStatus(String orderStatus);

        /**
         * 显示订单内容
         *
         * @param orderContent
         */
        void showOrderContent(String orderContent);

        /**
         * 显示订单酬劳
         *
         * @param orderMoney
         */
        void showOrderMoney(String orderMoney);

        /**
         * 显示申述内容
         *
         * @param stateContent
         */
        void showStateContent(String stateContent);

        /**
         * 显示附带图片(最多三张)
         *
         * @param urls
         */
        void showImage(String... urls);

        /**
         * 显示申述状态
         *
         * @param stateStatus
         * @param textColor
         */
        void showStateStatus(String stateStatus, @ColorRes int textColor);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 加载order成功
         */
        void loadOrderSuccess();
    }

    interface Presenter extends BasePresenter {
        /**
         * 加载申述单
         */
        void loadState();

        /**
         * 加载订单详情
         *
         * @param auto
         * @return
         */
        boolean loadOrderDetail(boolean auto);

        /**
         * 获取图片url
         *
         * @param position
         * @return
         */
        String getStateImageUrl(int position);
    }
}
