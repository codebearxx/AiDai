package com.ad.aidai.fragments.me.bell_setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.widgets.jelly_toggle_button.JellyToggleButton;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by pure on 2017/2/10.
 */

public class BellSettingFragment extends BaseFragment implements BellSettingContract.View {
    @BindView(R.id.s_voice)
    JellyToggleButton sVoice;
    @BindView(R.id.s_vibrate)
    JellyToggleButton sVibrate;

    private BellSettingContract.Presenter mPresenter;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_bell, container, false);
        mView.setClickable(true);//防止点击穿透，底层的fragment响应上层点击触摸事件

    }

    @Override
    protected void initView() {
        sVoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPresenter.switchIsHaveVoice();
            }
        });

        sVibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPresenter.switchIsHaveVibrate();
            }
        });
    }

    @Override
    protected void initData() {
        new BellSettingPresenter(mContext, this).subscribe();

    }

    @Override
    public void setPresenter(BellSettingContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @OnClick({R.id.iv_back})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
        }
    }

    @Override
    public void showIsHaveVoice() {
        if(mPresenter.getIsHaveVoiceLast()){
            sVoice.toggle();
        }
    }

    @Override
    public void showIsHaveVibrate() {
        if(mPresenter.getIsHaveVibrateLast()){
            sVibrate.toggle();
        }
    }

    @Override
    public boolean onActivityBackPressed() {
        getFragmentManager().popBackStack();
        return false;
    }
}
