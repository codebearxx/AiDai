package com.ad.aidai.fragments.create_order;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.flyco.dialog.entity.DialogMenuItem;
import com.mingle.entity.MenuEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * CreateOrderContract
 * <p>
 * Created by Yusxon on 17/1/18.
 */

public interface CreateOrderContract {

    interface View extends BaseView<Presenter> {

        /**
         * 显示订单类型
         *
         * @param type
         */
        void showOrderType(String type);

        /**
         * 获取订单内容
         *
         * @return
         */
        String getOrderContent();

        /**
         * 获取交货地址
         *
         * @return
         */
        String getOrderAddressOfDelivery();

        /**
         * 获取截止时间
         *
         * @return
         */
        String getEndTime();

        /**
         * 获取酬劳
         *
         * @return
         */
        String getRewardMoney();

        /**
         * 显示选择的地点名称
         *
         * @param placeName
         */
        void showPushPlace(String placeName);

        /**
         * 显示选择的地点名称
         *
         * @param pushTime
         */
        void showPushTime(String pushTime);

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 发单成功
         */
        void sendSuccess();

        /**
         * 显示加载没有动画对话框
         */
        void showLoadingWithoutAnim();

        /**
         * 打开推送地址选择
         */
        void openPushAddress();
    }

    interface Presenter extends BasePresenter {
        /**
         * 获取选择订单类型的子项内容
         *
         * @return
         */
        ArrayList<DialogMenuItem> getTypeMenuItem();

        /**
         * 选择了订单类型的哪一项
         *
         * @param position
         */
        void chooseTypeMenuItem(int position);

        /**
         * 提交订单
         */
        void submit();

        /**
         * 获取地点列表
         *
         * @return
         */
        List<MenuEntity> getPushPlaceMenuEntity();

        /**
         * 获取推送时间列表
         *
         * @return
         */
        List<MenuEntity> getPushTimeMenuEntity();

        /**
         * 选择了第position个地点
         *
         * @param position
         */
        void selectPushPlace(int position);

        /**
         * 选择了第position个推送时间
         *
         * @param position
         */
        void selectPushTime(int position);

        /**
         * 打开推送地址选择
         */
        void openPushAddress();
    }
}
