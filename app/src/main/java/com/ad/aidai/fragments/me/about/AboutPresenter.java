package com.ad.aidai.fragments.me.about;

import android.content.Context;

import com.ad.aidai.bean.Version;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResultWithData;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.Date;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by pure on 2017/2/10.
 */

public class AboutPresenter implements AboutContract.Presenter {

    private Context mContext;
    private AboutContract.View mView;
    private CompositeSubscription mSubscriptions;

    public AboutPresenter(Context context, AboutContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        initData();

    }

    private void initData() {
        setVersion();
        setNowYear();

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void setVersion() {
        mView.showVersion("V" + ADApp.getVersionName());
    }

    @Override
    public void setNowYear() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        Date date = new Date(System.currentTimeMillis());
        mView.showYear(format.format(date));
    }

    @Override
    public void checkUpdate() {
        mView.showLoading();
        NetWork.getAppApi()
                .getVersion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResultWithData<Version>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        if (mView != null) {
                            Logger.t("AboutPresenter").e(e.getMessage());
                            //关闭加载对话框
                            mView.closeLoading(false);
                        }
                    }

                    @Override
                    public void onNext(NetResultWithData<Version> versionNetResult) {
                        Logger.t("AboutPresenter").i(versionNetResult.toString());
                        if (versionNetResult.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            mView.closeLoading(true);
                            Version version = versionNetResult.getData();
                            if (version.getVersionId().equals(ADApp.getVersionName())) {
                                mView.toast("已是最新版本");
                            } else {
                                mView.showUpdateDialog(version.getVersionId(), version.getVersionMessage());
                            }
                        } else {
                            mView.closeLoading(false);
                        }
                    }
                });
    }

    @Override
    public void updateApp() {
        mView.toast("更新app");
    }
}
