package com.ad.aidai.fragments.reset_password;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;

/**
 * ResetPasswordContract
 * <p>
 * Created by Yusxon on 16/12/18.
 */

public interface ResetPasswordContract {


    interface View extends BaseView<Presenter> {

        /**
         * 显示加载对话框
         */
        void showLoading();

        /**
         * 关闭加载对话框
         *
         * @param success 是否加载成功
         */
        void closeLoading(boolean success);

        /**
         * 重置密码成功
         */
        void resetSuccess();
    }

    interface Presenter extends BasePresenter {
        /**
         * 重置密码
         *
         * @param newPassword 新密码
         */
        void resetPassword(String phone, String newPassword, String token);
    }

}
