package com.ad.aidai.fragments.order.order_push;

import com.ad.aidai.base.BasePresenter;
import com.ad.aidai.base.BaseView;
import com.ad.aidai.bean.Order;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;

/**
 * OrderPushContract
 * <p>
 * Created by Yusxon on 17/1/13.
 */

public interface OrderPushContract {

    interface View extends BaseView<Presenter> {

        /**
         * 进入item详细信息页面
         */
        void intoItem();

        /**
         * 是否设置推送
         */
        void isSetPush(boolean is);
    }

    interface Presenter extends BasePresenter {
        /**
         * 获取adapter
         *
         * @return
         */
        RecyclerArrayAdapter<Order> getAdapter();

        /**
         * 刷新
         */
        void refresh();

        /**
         * 是否有设置推送
         *
         * @return
         */
        boolean isSetPush();

        /**
         * 推送设置改变
         */
        void pushSettingChange();

        /**
         * 订单状态改变
         *
         * @param order
         */
        void orderStatusChange(Order order);
    }

}
