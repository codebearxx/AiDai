package com.ad.aidai.fragments.service_item;

import android.content.Context;

import com.ad.aidai.network.ADUrl;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by pure on 2017/2/15.
 */

public class ServiceItemPresenter implements ServiceItemContract.Presenter {

    private Context mContext;
    private ServiceItemContract.View mView;
    private CompositeSubscription mSubscriptions;

    public ServiceItemPresenter(Context context, ServiceItemContract.View view) {
        this.mContext = context;
        this.mView = view;
        mSubscriptions = new CompositeSubscription();
        mView.setPresenter(this);
        initData();

    }

    private void initData() {

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mContext = null;
        mView = null;
        mSubscriptions.clear();
    }

    @Override
    public void setItemContent() {
        String url = ADUrl.BASE_URL + ADUrl.SERVICE_ITEM;
        mView.showItemContent(url);
    }
}
