package com.ad.aidai.fragments.look_profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.base.BaseViewHolder;
import com.ad.aidai.bean.ProfileBean;
import com.ad.aidai.common.listener.RecyclerViewListener;
import com.ad.aidai.common.widgets.imageview.CircleImageView;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

/**
 * LookProfileAdapter
 * <p>
 * Created by pure on 2017/1/12.
 */

public class LookProfileAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<ProfileBean> datas;
    private Context mContext;

    private RecyclerViewListener.OnRecyclerViewItemClickListener mOnItemClickListener = null;
    private RecyclerViewListener.OnRecyclerViewItemLongClickListener mOnItemClickLongListener = null;
    private RecyclerViewListener.OnRecyclerViewItemSubClickListener mOnItemSubClickListener = null;

    public void setOnItemClickListener(RecyclerViewListener.OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void setOnItemLongClickListener(RecyclerViewListener.OnRecyclerViewItemLongClickListener listener) {
        this.mOnItemClickLongListener = listener;
    }

    public void setmOnItemSubClickListener(RecyclerViewListener.OnRecyclerViewItemSubClickListener
                                                   mOnItemSubClickListener) {
        this.mOnItemSubClickListener = mOnItemSubClickListener;
    }

    public LookProfileAdapter(Context context, List<ProfileBean> datas) {
        this.mContext = context;
        this.datas = datas;
    }

    //创建新View，被LayoutManager所调用
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        BaseViewHolder holder;
        switch (viewType) {
            case ProfileBean.VIEWTYPE_HEAD:
                holder = new HeadViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                        .item_profile_head, viewGroup, false));
                break;
            case ProfileBean.VIEWTYPE_STRING:
                holder = new StringViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                        .item_profile_string, viewGroup, false));
                break;
            case ProfileBean.VIEWTYPE_BUTTON:
                holder = new ButtonViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                        .item_profile_button, viewGroup, false));
                break;
            default:
                holder = new BaseViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                        .item_profile_space, viewGroup, false));
                break;
        }
        return holder;
    }

    //将数据与界面进行绑定的操作
    @Override
    public void onBindViewHolder(final BaseViewHolder viewHolder, final int position) {
        final int pos = viewHolder.getAdapterPosition();
        switch (viewHolder.getItemViewType()) {
            case ProfileBean.VIEWTYPE_HEAD:
                final HeadViewHolder headViewHolder = (HeadViewHolder) viewHolder;

                headViewHolder.tvName.setText(datas.get(position).getName());
                Glide.with(mContext)
                        .load(datas.get(position).getValue())
                        .placeholder(R.mipmap.default_image)
                        .dontAnimate()
                        .into(headViewHolder.civHead);

                headViewHolder.civHead.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemSubClickListener != null) {
                            mOnItemSubClickListener.onItemSubClick(headViewHolder.civHead, pos);
                        }
                    }
                });
                break;
            case ProfileBean.VIEWTYPE_STRING:
                StringViewHolder stringViewHolder = (StringViewHolder) viewHolder;

                stringViewHolder.tvName.setText(datas.get(position).getName());
                stringViewHolder.tvValue.setText(String.valueOf(datas.get(position).getValue()));

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onItemClick(viewHolder.itemView, pos);
                        }
                    }
                });
                viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (mOnItemClickLongListener != null) {
                            mOnItemClickLongListener.onItemLongClick(viewHolder.itemView, pos);
                        }
                        return true;
                    }
                });
                break;
            case ProfileBean.VIEWTYPE_SPACE:
                break;
            case ProfileBean.VIEWTYPE_BUTTON:
                final ButtonViewHolder buttonViewHolder = (ButtonViewHolder) viewHolder;

                buttonViewHolder.btnProfile.setText(datas.get(position).getName());
                buttonViewHolder.btnProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemSubClickListener != null) {
                            mOnItemSubClickListener.onItemSubClick(buttonViewHolder.btnProfile, pos);
                        }
                    }
                });

                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position).getType();
    }

    //获取数据的数量
    @Override
    public int getItemCount() {
        return datas.size();
    }

    class StringViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_profile_name)
        TextView tvName;
        @BindView(R.id.tv_profile_value)
        TextView tvValue;

        StringViewHolder(View view) {
            super(view);
        }
    }

    class HeadViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_profile_name)
        TextView tvName;
        @BindView(R.id.civ_profile_head)
        CircleImageView civHead;

        HeadViewHolder(View view) {
            super(view);
        }

    }

    class ButtonViewHolder extends BaseViewHolder {
        @BindView(R.id.btn_profile)
        Button btnProfile;

        ButtonViewHolder(View view) {
            super(view);
        }

    }

}
