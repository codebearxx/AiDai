package com.ad.aidai.fragments.order_detail;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ad.aidai.R;
import com.ad.aidai.activitys.order_score.OrderScoreActivity;
import com.ad.aidai.activitys.state.StateActivity;
import com.ad.aidai.base.BaseFragment;
import com.ad.aidai.common.ADApp;
import com.ad.aidai.common.RxBus;
import com.ad.aidai.common.listener.RecyclerViewListener;
import com.ad.aidai.common.widgets.loading_dialog.LoadingDialog;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.fragments.look_profile.LookProfileFragment;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideTopExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.jude.easyrecyclerview.decoration.DividerDecoration;

import butterknife.BindView;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * 订单详细页面
 * <p>
 * Created by Yusxon on 17/1/25.
 */

public class OrderDetailFragment extends BaseFragment implements OrderDetailContract.View {

    @BindView(R.id.rlv_order_detailed)
    RecyclerView rlvOrderDetailed;

    private OrderDetailContract.Presenter mPresenter;
    private OrderDetailAdapter mAdapter;

    private LoadingDialog loadingDialog;

    private NormalDialog dialog;
    private BaseAnimatorSet mBasIn;
    private BaseAnimatorSet mBasOut;

    @Override
    protected void initLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
            savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_order_detail, container, false);
        mView.setClickable(true);
    }

    @Override
    protected void initView() {
        //创建默认的线性LayoutManager
        rlvOrderDetailed.setLayoutManager(new LinearLayoutManager(getActivity()));

        //设置分割线
        DividerDecoration itemDecoration = new DividerDecoration(Color.parseColor("#F2F2F2"), 3, 0, 0);
        itemDecoration.setDrawLastItem(true);
        rlvOrderDetailed.addItemDecoration(itemDecoration);

        mBasIn = new BounceTopEnter();
        mBasOut = new SlideTopExit();
    }

    @Override
    protected void initData() {
        new OrderDetailPresenter(mContext, this).subscribe();

        mAdapter = new OrderDetailAdapter(mContext, mPresenter.getOrderData());
        rlvOrderDetailed.setAdapter(mAdapter);

        mAdapter.setmOnItemSubClickListener(new RecyclerViewListener.OnRecyclerViewItemSubClickListener() {
            @Override
            public void onItemSubClick(View view, int position) {
                mPresenter.clickBtnOrder(view, position);
            }
        });

        mAdapter.setOnItemClickListener(new RecyclerViewListener.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mPresenter.clickItem(view, position);
            }
        });

        mPresenter.loadOrderData();
    }

    @Override
    public void setPresenter(OrderDetailContract.Presenter presenter) {
        this.mPresenter = presenter;
    }


    @OnClick(R.id.iv_back)
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onActivityBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onActivityBackPressed() {
        ADApp.setOrder(null);
        getFragmentManager().popBackStack();
        return false;
    }

    @Override
    public void loadOrderDataComplete() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void gotoState() {
        mActivity.startActivity(new Intent(mActivity, StateActivity.class));
    }

    @Override
    public void gotoComment() {
        mActivity.startActivity(new Intent(mActivity, OrderScoreActivity.class));
    }

    @Override
    public void showLoading(@NonNull String text) {
        //显示加载对话框
        loadingDialog = new LoadingDialog(mActivity);
        loadingDialog.setLoadingText(text + "中...")
                .setFailedText(text + "失败")
                .setSuccessText(text + "成功")
                .show();
    }

    @Override
    public void closeLoading(boolean success) {
        if (loadingDialog != null) {
            if (success) {
                loadingDialog.loadSuccess();
            } else {
                loadingDialog.loadFailed();
            }
        }
    }

    @Override
    public void openLookProfile(long userId, boolean show, boolean showButton) {
        LookProfileFragment fragment = new LookProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("show", show);
        bundle.putLong("userId", userId);
        bundle.putBoolean("showButton", showButton);
        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fragment_slide_left_enter, 0, 0, R.anim
                        .fragment_slide_left_exit)
                .add(android.R.id.content, fragment, "lookProfile")
                .addToBackStack("lookProfile")
                .commit();
    }

    protected void subscribeEvents() {
        //订阅事件
        addSubscription(RxBus.getInstance()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        if (o instanceof CommonEvent) {
                            CommonEvent event = (CommonEvent) o;
                            switch (event.getEvent()) {
                                case CommonEvent.ORDER_STATUS_CHANGE:
                                    mPresenter.loadOrderData();
                                    break;
                            }
                        }
                    }
                })
                .subscribe(RxBus.defaultSubscriber()));
    }

    @Override
    public void showDialog(String message, final String btnText) {
        dialog = new NormalDialog(getContext());
        dialog.content(message)
                .style(NormalDialog.STYLE_TWO)
                .titleTextSize(20)
                .btnText("取消", btnText)
                .showAnim(mBasIn)
                .dismissAnim(mBasOut)
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        mPresenter.clickDialogSure(btnText);
                        dialog.dismiss();
                    }
                });
    }
}
