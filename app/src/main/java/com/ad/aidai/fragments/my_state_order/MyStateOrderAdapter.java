package com.ad.aidai.fragments.my_state_order;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.bean.Order;
import com.ad.aidai.bean.State;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * MyOrderAdapter
 * <p>
 * Created by Yusxon on 17/2/2.
 */

public class MyStateOrderAdapter extends RecyclerArrayAdapter<State> {

    private Context mContext;

    public MyStateOrderAdapter(Context context, List<State> objects) {
        super(context, objects);
        this.mContext = context;
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new StateOrderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                .item_my_state_order, parent, false));
    }

    @Override
    public void OnBindViewHolder(BaseViewHolder holder, int position) {
        StateOrderViewHolder stateOrderViewHolder = (StateOrderViewHolder) holder;
        State state = mObjects.get(position);
        Order order = state.getOrder();

        stateOrderViewHolder.tvOrderType.setText(stateOrderViewHolder.orderType[order.getOrderType()]);
        stateOrderViewHolder.tvOrderTime.setText(state.getStateSubmitTime());
        stateOrderViewHolder.tvOrderStatus.setText(state.getStateStatus() == 0 ? "待完成" : "完成");
        stateOrderViewHolder.tvOrderStatus.setTextColor(state.getStateStatus() == 0 ? ContextCompat.getColor
                (mContext, R.color.main_home_item_final_color) : ContextCompat.getColor(mContext, R.color.colorTheme));
        stateOrderViewHolder.tvOrderContent.setText(order.getOrderContent());
        stateOrderViewHolder.tvStateCause.setText(state.getStateCause());
    }

    class StateOrderViewHolder extends BaseViewHolder {

        @BindView(R.id.tv_order_type)
        TextView tvOrderType;
        @BindView(R.id.tv_order_time)
        TextView tvOrderTime;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_order_content)
        TextView tvOrderContent;
        @BindView(R.id.tv_state_cause)
        TextView tvStateCause;

        String[] orderType = {"代拿", "代买", "代办"};

        public StateOrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}