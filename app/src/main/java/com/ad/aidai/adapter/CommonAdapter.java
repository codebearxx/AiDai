package com.ad.aidai.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ad.aidai.common.utils.ListViewHolder;

import java.util.List;

/**
 * Created by pure on 2016/12/19.
 */

/**
 * 共同适配器
 */
public abstract class CommonAdapter<T> extends BaseAdapter {

    protected Context mContext;
    protected List<T> mData;
    protected int mLayoutId;
    protected List<Integer> mLayoutIds;

    // public CommonAdapter(Context context,List<T> data,int layoutId){
    // mContext = context;
    // mData = data;
    // mLayoutId = layoutId;
    // }

    public CommonAdapter(Context context, List<T> data, List<Integer> layoutIds) {
        mContext = context;
        mData = data;
        mLayoutIds = layoutIds;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public T getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int index = getLayoutIndex(position);
        // ListViewHolder holder =
        // ListViewHolder.getHolder(mContext,convertView,mLayoutId,parent,position);
        ListViewHolder holder = ListViewHolder.getHolder(mContext, convertView,
                mLayoutIds.get(index), parent, position);
        convert(holder, position);
        return holder.getConvertView();
    }

    /**
     * get holder convert
     */
    public abstract void convert(ListViewHolder holder, int position);

    /**
     * 根据位置position对应相应的界面
     */
    public abstract int getLayoutIndex(int position);

}

