package com.ad.aidai.viewholder;

import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ad.aidai.R;
import com.ad.aidai.bean.Order;
import com.ad.aidai.bean.User;
import com.ad.aidai.common.widgets.imageview.CircleImageView;
import com.ad.aidai.network.ADUrl;
import com.bumptech.glide.Glide;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * OrderViewHolder
 * <p>
 * Created by Yusxon on 16/12/24.
 */

public class OrderViewHolder extends BaseViewHolder<Order> {

    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_order_type)
    TextView tvOrderType;
    @BindView(R.id.civ_user_head)
    CircleImageView ivUserHead;
    @BindView(R.id.tv_order_reward_receive)
    TextView tvOrderRewardReceive;
    @BindView(R.id.tv_order_create_time)
    TextView tvOrderCreateTime;
    @BindView(R.id.tv_order_content)
    TextView tvOrderContent;

    private String[] orderType = {"代拿", "代买", "代办"};

    public OrderViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_order_two_line);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setData(Order data) {
        super.setData(data);
        tvUserName.setText(data.getSendUser().getUserName());
        tvOrderType.setText(orderType[data.getOrderType()]);
        tvOrderContent.setText(data.getOrderContent());
        tvOrderCreateTime.setText(data.getOrderSendTime());
        tvOrderRewardReceive.setText("已被接");
        if (data.getOrderStatus() == Order.STATUS_CANCEL) {
            tvOrderRewardReceive.setText("已撤销");
        }
        tvOrderRewardReceive.setTextColor(Color.RED);

        User user = data.getSendUser();
        String url = ADUrl.HEAD_ICON_URL.replace("{schoolId}", user.getSchoolId() + "").replace("{photoName}", user
                .getUserIcon());
        Glide.with(getContext())
                .load(url)
                .placeholder(R.mipmap.default_image)
                .dontAnimate()
                .into(ivUserHead);
    }
}
