package com.ad.aidai.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.widget.ImageView;

import com.ad.aidai.R;
import com.ad.aidai.bean.Order;
import com.ad.aidai.bean.Place;
import com.ad.aidai.bean.State;
import com.ad.aidai.bean.User;
import com.ad.aidai.common.widgets.loading_dialog.LoadingDialog;
import com.ad.aidai.common.widgets.loading_dialog.StyleManager;
import com.ad.aidai.db.DBManager;
import com.ad.aidai.db.dao.MyUserDao;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResultWithData;
import com.bumptech.glide.Glide;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.loader.ImageLoader;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.smssdk.SMSSDK;
import io.rong.imkit.RongIM;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static io.rong.imkit.utils.SystemUtils.getCurProcessName;


/**
 * application
 * <p>
 * Created by Yusxon on 16/11/26.
 */

public class ADApp extends Application {

    private static Context mContext;
    /**
     * 用户
     */
    private static User user;
    /**
     * 临时订单
     */
    private static Order order;
    /**
     * 临时申述单
     */
    private static State state;
    /**
     * 用户经纬度
     */
    private static double userLongtitude = 113.914619;
    private static double userLatitude = 22.50128;

    /**
     * 学校周边地址信息
     */
    private static List<Place> places = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this;

        MobclickAgent.startWithConfigure(new MobclickAgent.UMAnalyticsConfig(this, "58af03064ad1562648000a0a", "",
                MobclickAgent.EScenarioType.E_UM_NORMAL));

        if (getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext())) ||
                "io.rong.push".equals(getCurProcessName(getApplicationContext()))) {
            //融云 初始化
            RongIM.init(mContext);
        }
    }

    /**
     * 做一些初始化操作
     */
    public static void init() {
        Logger.init("ADApp");
        LoadingDialog.initStyle(new StyleManager()
                .Anim(true)
                .repeatTime(0)
                .contentSize(-1)
                .speed(2)
                .showTime(300)
                .intercept(true));
        //短信验证码 初始化
        //        SMSSDK.initSDK(this, "19b635c6f0318", "29625c88a07a2b7daad75afb66d4c832");
        SMSSDK.initSDK(mContext, "19af75099f58c", "6edbaa76269f100b634b0f7f996bff8e");
        //数据库 初始化
        DBManager.getDB().initDB();

        user = MyUserDao.getUserDao().query();
        if (user == null) {
            user = new User();
        }
        Logger.i(user.toString());

        ImagePicker imagePicker = ImagePicker.getInstance();
        imagePicker.setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Activity activity, String path, ImageView imageView, int width, int height) {
                Glide.with(activity)
                        .load(new File(path))
                        .placeholder(R.mipmap.default_image)
                        .error(R.mipmap.default_image)
                        .centerCrop()
                        .dontAnimate()
                        .into(imageView);
            }

            @Override
            public void clearMemoryCache() {

            }
        });
    }

    public static void loadSchoolPlace() {
        if(user.getSchoolId() == null) {
            return ;
        }
        NetWork.getOrderOperationApi()
                .getPlaceBySchoolId(ADApp.getUser().getSchoolId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResultWithData<List<Place>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(mContext, e);
                        Logger.t("ADApp").e(e.getMessage());
                    }

                    @Override
                    public void onNext(NetResultWithData<List<Place>> result) {
                        Logger.t("ADApp").i(result.toString());
                        if (result.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            places.clear();
                            places.addAll(result.getData());
                        }
                    }
                });
    }

    public static String getVersionName() {
        try {
            // 获取packagemanager的实例
            PackageManager packageManager = mContext.getPackageManager();
            // getPackageName()是你当前类的包名，0代表是获取版本信息
            PackageInfo packInfo = packageManager.getPackageInfo(mContext.getPackageName(), 0);
            return packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取全局context
     *
     * @return
     */
    public static Context getContext() {
        return mContext;
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public static User getUser() {
        return user;
    }

    public static void setUser() {
        user = MyUserDao.getUserDao().query();
    }

    public static Order getOrder() {
        return order;
    }

    public static void setOrder(Order newOrder) {
        order = newOrder;
    }

    public static State getState() {
        return state;
    }

    public static void setState(State state) {
        ADApp.state = state;
    }

    public static double getUserLongtitude() {
        return userLongtitude;
    }

    public static void setUserLongtitude(double userLongtitude) {
        ADApp.userLongtitude = userLongtitude;
    }

    public static double getUserLatitude() {
        return userLatitude;
    }

    public static void setUserLatitude(double userLatitude) {
        ADApp.userLatitude = userLatitude;
    }

    public static List<Place> getPlaces() {
        return places;
    }

    public static void setPlaces(List<Place> places) {
        ADApp.places = places;
    }
}
