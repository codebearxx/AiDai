package com.ad.aidai.common.listener;

/**
 * Created by Yusxon on 17/1/25.
 */

public interface OnActivityBackPressedListener {
    /**
     * 按了返回键
     *
     * @return true表示给activity处理，false表示自己需要处理
     */
    boolean onActivityBackPressed();
}
