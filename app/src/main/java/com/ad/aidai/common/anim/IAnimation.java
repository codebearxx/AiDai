package com.ad.aidai.common.anim;

/**
 * 动画接口
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public interface IAnimation {
    /**
     * 开始动画
     */
    void startRotateAnimation();

    /**
     * 取消动画
     */
    void cancelRotateAnimation();

    /**
     * 暂停动画
     */
    void pauseRotateAnimation();

    /**
     * 继续动画
     */
    void resumeRotateAnimation();
}
