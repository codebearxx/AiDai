package com.ad.aidai.common;

import android.content.Context;
import android.net.Uri;
import android.view.View;

import com.ad.aidai.bean.User;
import com.ad.aidai.bean.UserWithSchool;
import com.ad.aidai.common.utils.CheckUtil;
import com.ad.aidai.events.CommonEvent;
import com.ad.aidai.network.ADUrl;
import com.ad.aidai.network.NetWork;
import com.ad.aidai.network.StatusCodeFromNet;
import com.ad.aidai.network.result.NetResultWithData;
import com.orhanobut.logger.Logger;
import com.umeng.analytics.MobclickAgent;

import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.UserInfo;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static io.rong.imkit.utils.SystemUtils.getCurProcessName;

/**
 * 管理融云的
 * <p>
 * Created by Yusxon on 17/2/22.
 */

public class RongImManager {

    /**
     * 连接融云服务器
     */
    public static void connect() {
        String token = ADApp.getUser().getToken();
        Logger.i("token:" + token);

        if (ADApp.getContext().getApplicationInfo().packageName.equals(getCurProcessName(ADApp.getContext()))) {
            RongIM.connect(token, new RongIMClient.ConnectCallback() {

                /**
                 * Token 错误。可以从下面两点检查
                 * 1.  Token 是否过期，如果过期您需要向 App Server 重新请求一个新的 Token
                 * 2.  token 对应的 appKey 和工程里设置的 appKey 是否一致
                 */
                @Override
                public void onTokenIncorrect() {
                    MobclickAgent.reportError(ADApp.getContext(), "onTokenIncorrect");
                    Logger.t(this.getClass().getSimpleName()).e("userId:" + ADApp.getUser().getUserId() +
                            "--onTokenIncorrect--");
                }

                /**
                 * 连接融云成功
                 * @param userid 当前 token 对应的用户 id
                 */
                @Override
                public void onSuccess(String userid) {
                    RongIM.getInstance().enableNewComingMessageIcon(true);
                    RongIM.getInstance().enableUnreadMessageIcon(true);
                    Logger.t(this.getClass().getSimpleName()).i("--onSuccess--" + userid);
                }

                /**
                 * 连接融云失败
                 * @param errorCode 错误码，可到官网 查看错误码对应的注释
                 */
                @Override
                public void onError(RongIMClient.ErrorCode errorCode) {
                    MobclickAgent.reportError(ADApp.getContext(), "userId:" + ADApp.getUser().getUserId() + "---" +
                            errorCode.getMessage() + "--" + errorCode.getValue());
                    Logger.t(this.getClass().getSimpleName()).e(errorCode.getMessage());
                    Logger.t(this.getClass().getSimpleName()).e(errorCode.getValue() + "");
                }
            });
        }
        /**
         * 设置用户信息(融云)
         */
        RongIM.setUserInfoProvider(new RongIM.UserInfoProvider() {

            @Override
            public UserInfo getUserInfo(String userId) {
                String newUserId;
                if (!CheckUtil.checkStrNull(userId)) {
                    getUserById(userId);
                    newUserId = userId;
                } else {
                    newUserId = "0";
                }
                return new UserInfo(newUserId, "", Uri.parse(""));
            }
        }, true);
        setConversationBehaviorListener();
        setConnectionStatusListener();
    }

    private static void getUserById(String userId) {
        NetWork.getAccountOperationApi()
                .lookProfile(Long.parseLong(userId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NetResultWithData<UserWithSchool>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MobclickAgent.reportError(ADApp.getContext(), e);
                        Logger.t("lookProfile").e(e.getMessage());
                    }

                    @Override
                    public void onNext(NetResultWithData<UserWithSchool> lookProfileFromNet) {
                        Logger.t("lookProfile").i(lookProfileFromNet.toString());
                        if (lookProfileFromNet.getCode() == StatusCodeFromNet.CODE_SUCCESS) {
                            refreshUserInfoCache(lookProfileFromNet.getData());
                        }
                    }
                });
    }

    /**
     * 更新用户缓存信息(融云)
     */
    public static void refreshUserInfoCache(User user) {
        String schoolId;
        if (user.getSchoolId() == null) {
            schoolId = "0";
        } else {
            schoolId = String.valueOf(user.getSchoolId());
        }
        String url = ADUrl.HEAD_ICON_URL.replace("{schoolId}", schoolId).replace("{photoName}", user.getUserIcon());
        RongIM.getInstance().refreshUserInfoCache(new UserInfo(user.getUserId() + "", user.getUserName(), Uri.parse
                (url)));
    }

    /**
     * 监听会话界面
     */
    private static void setConversationBehaviorListener() {
        /**
         * 设置会话界面操作的监听器。
         */
        RongIM.setConversationBehaviorListener(new RongIM.ConversationBehaviorListener() {
            /**
             * 当点击用户头像后执行。
             *
             * @param context           上下文。
             * @param conversationType  会话类型。
             * @param userInfo          被点击的用户的信息。
             * @return 如果用户自己处理了点击后的逻辑，则返回 true，否则返回 false，false 走融云默认处理方式。
             */
            @Override
            public boolean onUserPortraitClick(Context context, Conversation.ConversationType conversationType,
                                               UserInfo userInfo) {
                RxBus.getInstance().postEvent(new CommonEvent<>(CommonEvent.USER_CLICK_HEAD_IN_CHAT, Long.parseLong
                        (userInfo.getUserId())));
                return true;
            }

            /**
             * 当长按用户头像后执行。
             *
             * @param context          上下文。
             * @param conversationType 会话类型。
             * @param userInfo         被点击的用户的信息。
             * @return 如果用户自己处理了点击后的逻辑，则返回 true，否则返回 false，false 走融云默认处理方式。
             */
            @Override
            public boolean onUserPortraitLongClick(Context context, Conversation.ConversationType conversationType,
                                                   UserInfo userInfo) {
                return false;
            }

            /**
             * 当点击消息时执行。
             *
             * @param context 上下文。
             * @param view    触发点击的 View。
             * @param message 被点击的消息的实体信息。
             * @return 如果用户自己处理了点击后的逻辑，则返回 true， 否则返回 false, false 走融云默认处理方式。
             */
            @Override
            public boolean onMessageClick(Context context, View view, Message message) {
                return false;
            }

            /**
             * 当点击链接消息时执行。
             *
             * @param context 上下文。
             * @param link    被点击的链接。
             * @return 如果用户自己处理了点击后的逻辑处理，则返回 true， 否则返回 false, false 走融云默认处理方式。
             */
            @Override
            public boolean onMessageLinkClick(Context context, String link) {
                return false;
            }

            /**
             * 当长按消息时执行。
             *
             * @param context 上下文。
             * @param view    触发点击的 View。
             * @param message 被长按的消息的实体信息。
             * @return 如果用户自己处理了长按后的逻辑，则返回 true，否则返回 false，false 走融云默认处理方式。
             */
            @Override
            public boolean onMessageLongClick(Context context, View view, Message message) {
                return false;
            }
        });
    }

    /**
     * 监听连接状态
     */
    private static void setConnectionStatusListener() {
        RongIM.setConnectionStatusListener(new RongIMClient.ConnectionStatusListener() {
            @Override
            public void onChanged(ConnectionStatus connectionStatus) {
                MobclickAgent.reportError(ADApp.getContext(), "userId:" + ADApp.getUser().getUserId() + "---" +
                        connectionStatus.getMessage() + "--" + connectionStatus.getValue());

                Logger.t("RongImManager").i("userId:" + ADApp.getUser().getUserId() + "---" +
                        connectionStatus.getMessage() + "--" + connectionStatus.getValue());
            }
        });
    }

//    /**
//     * 监听融云未读消息数
//     */
//    private static void addUnReadMessageCountChangedObserver() {
//        RongIM.getInstance().addUnReadMessageCountChangedObserver(new IUnReadMessageObserver() {
//            @Override
//            public void onCountChanged(int i) {
//                Logger.t("RongImManager").i("未读消息数：" + i);
//                Toast.makeText(ADApp.getContext(), "未读消息数：" + i, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//    }

    /**
     * 退出账号，断开连接
     */
    public static void exit() {
        RongIM.getInstance().logout();
        RongIM.getInstance().disconnect();
    }
}
