package com.ad.aidai.common.slideback;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

/**
 * 右滑关闭功能控制器
 * 使用该功能可以在Activity中的OnTouchEvent方法中调用该类的processEvent(MotionEvent event)方法
 * 也可以直接继承SlideBackActivity
 * <p>
 * !!!!最重要的一点,使用该功能的Activity,必须使用SlideBackTheme主题
 * 可在AndroidManifest.xml文件中配置,否则右滑时背景会是黑色的,而且会出现Activity重叠效果
 * <p>
 * Created by Yusxon on 16/12/19.
 */

public class SlideBackController {
    /**
     * 默认动画时间
     */
    public static final int ANIMATION_DURATION = 300;
    /**
     * 默认开始滑动的位置距离左边缘的距离
     */
    public static int sDistanceOfTouch = 100;
    /**
     * 手机屏幕宽度
     */
    private int mScreenWidth;
    private int mTouchSlop;
    private boolean isMoving = false;
    private float mInitX;
    private float mInitY;

    /**
     * 整个Activity视图的根布局
     */
    private ViewGroup decorView;
    /**
     * content布局
     */
    private ViewGroup contentView;
    /**
     * 用户添加的布局
     */
    private ViewGroup userView;

    private ArgbEvaluator evaluator;
    private ValueAnimator mAnimator;
    private VelocityTracker mVelTracker;

    public SlideBackController(final Activity activity) {
        mScreenWidth = activity.getResources().getDisplayMetrics().widthPixels;
        //把允许滑动范围设为屏幕的三分之一(从左往右)
        sDistanceOfTouch = mScreenWidth / 3;
        mTouchSlop = ViewConfiguration.get(activity).getScaledTouchSlop();
        evaluator = new ArgbEvaluator();

        decorView = (ViewGroup) activity.getWindow().getDecorView();
        decorView.setBackgroundColor(Color.parseColor("#00ffffff"));
        contentView = (ViewGroup) activity.findViewById(android.R.id.content);
        userView = (ViewGroup) contentView.getChildAt(0);

        mAnimator = new ValueAnimator();
        mAnimator.setDuration(ANIMATION_DURATION);
        mAnimator.setInterpolator(new DecelerateInterpolator());
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int x = (Integer) valueAnimator.getAnimatedValue();
                if (x >= mScreenWidth) {
                    activity.finish();
                }

                handleView(x);
                handleBackgroundColor(x);
            }
        });
    }

    public void handleView(int x) {
        if (x < 0) {
            x = 0;
        }
        if (x > mScreenWidth) {
            x = mScreenWidth;
        }

        userView.setTranslationX(x);
    }

    /**
     * 控制背景颜色和透明度
     *
     * @param x
     */
    private void handleBackgroundColor(float x) {
        if (x < 0) {
            x = 0;
        }
        if (x > mScreenWidth) {
            x = mScreenWidth;
        }

        int colorValue = (int) evaluator.evaluate(x / mScreenWidth, Color.parseColor("#dd000000"), Color.parseColor
                ("#00000000"));
        contentView.setBackgroundColor(colorValue);
    }

    public boolean processEvent(MotionEvent event) {
        getVelocityTracker(event);

        if (mAnimator.isRunning()) {
            return true;
        }

        int pointId = -1;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mInitX = event.getRawX();
                mInitY = event.getRawY();
                pointId = event.getPointerId(0);
                break;
            case MotionEvent.ACTION_MOVE:
                float dx = event.getRawX() - mInitX;
                float dy = event.getRawY() - mInitY;
                if (!isMoving) {
                    if (dx > mTouchSlop && dx > dy && mInitX < sDistanceOfTouch) {
                        isMoving = true;
                    }
                }
                if (isMoving) {
                    handleView((int) dx);
                    handleBackgroundColor(dx);
                }

                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                int distance = (int) (event.getRawX() - mInitX);

                mVelTracker.computeCurrentVelocity(1000);
                //获取x方向上的速度
                float velocityX = mVelTracker.getXVelocity(pointId);

                if (isMoving && Math.abs(userView.getTranslationX()) >= 0) {
                    if (velocityX > 1000f || distance >= mScreenWidth / 3 * 2) {
                        mAnimator.setIntValues(distance, mScreenWidth);
                    } else {
                        mAnimator.setIntValues(distance, 0);
                    }
                    mAnimator.start();
                    isMoving = false;
                }

                mInitX = 0;
                mInitY = 0;

                recycleVelocityTracker();
                break;
        }
        return true;
    }

    /**
     * 获取速度追踪器
     *
     * @return
     */
    private VelocityTracker getVelocityTracker(MotionEvent event) {
        if (mVelTracker == null) {
            mVelTracker = VelocityTracker.obtain();
        }
        mVelTracker.addMovement(event);
        return mVelTracker;
    }

    /**
     * 回收速度追踪器
     */
    private void recycleVelocityTracker() {
        if (mVelTracker != null) {
            mVelTracker.clear();
            mVelTracker.recycle();
            mVelTracker = null;
        }
    }
}
