package com.ad.aidai.common.widgets.loading_dialog;

import android.view.View;

/**
 * Created by Yusxon on 17/2/16.
 */

public interface FinishDrawListener {
    /**
     * 分发绘制完成事件
     *
     * @param v 绘制完成的View
     */
    void dispatchFinishEvent(View v);
}
