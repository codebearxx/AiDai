package com.ad.aidai.common.widgets.jelly_toggle_button;

/**
 * Created by Yusxon on 17/2/13.
 */

public enum State {

    LEFT(0),
    LEFT_TO_RIGHT(1),
    RIGHT(2),
    RIGHT_TO_LEFT(3);

    int v;

    State(int v) {
        this.v = v;
    }
}
