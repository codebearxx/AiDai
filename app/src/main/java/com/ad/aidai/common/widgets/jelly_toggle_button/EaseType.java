package com.ad.aidai.common.widgets.jelly_toggle_button;

/**
 * Created by Yusxon on 17/2/13.
 */

public enum EaseType {
    EaseInQuart(EaseInQuart.class);


    private Class easingType;

    /**
     * ease animation helps to make the movement more real
     * @param easingType
     */
    EaseType(Class easingType) {
        this.easingType = easingType;
    }

    public float getOffset(float offset) {
        try {
            return ((CubicBezier) easingType.getConstructor().newInstance()).getOffset(offset);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("CubicBezier init error.");
        }
    }
}
