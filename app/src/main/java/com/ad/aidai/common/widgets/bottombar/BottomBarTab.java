package com.ad.aidai.common.widgets.bottombar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.ad.aidai.R;
import com.ad.aidai.common.widgets.DragPointView;

/**
 * 底部导航栏选项卡
 * <p>
 * Created by Yusxon on 16/12/16.
 */

public class BottomBarTab extends FrameLayout {
    /**
     * 图标
     */
    private ImageView mIcon;
    /**
     * 选项卡位置
     */
    private int mTabPosition = -1;
    /**
     * 选中时的颜色
     */
    private int selectedColor;
    /**
     * 未选中时的颜色
     */
    private int unselectedColor;

    private Context mContext;

    private DragPointView mUnreadNumView;

    public BottomBarTab(Context context, @DrawableRes int icon) {
        this(context, null, icon);
    }

    public BottomBarTab(Context context, AttributeSet attrs, @DrawableRes int icon) {
        this(context, attrs, 0, icon);
    }

    public BottomBarTab(Context context, AttributeSet attrs, int defStyleAttr, @DrawableRes int icon) {
        super(context, attrs, defStyleAttr);
        init(context, icon);
    }

    private void init(Context context, int icon) {
        this.mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(
                new int[]{R.attr.selectableItemBackgroundBorderless});
        Drawable drawable = typedArray.getDrawable(0);
        setBackground(drawable);

        typedArray.recycle();

        //初始化两种状态下的颜色
        selectedColor = ContextCompat.getColor(mContext, R.color.nav_bottom_light);
        unselectedColor = ContextCompat.getColor(mContext, R.color.nav_bottom_dark);

        mIcon = new ImageView(context);
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics());
        int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources()
                .getDisplayMetrics());
        LayoutParams params = new LayoutParams(size, size);
        params.gravity = Gravity.CENTER;
        params.setMargins(0, marginTop, 0, marginTop);
        mIcon.setImageResource(icon);
        mIcon.setLayoutParams(params);
        mIcon.setColorFilter(unselectedColor);

        int size2 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, getResources().getDisplayMetrics
                ());
        mUnreadNumView = new DragPointView(mContext, null);
        LayoutParams params2 = new LayoutParams(size2, size2);
        params2.gravity = Gravity.CENTER;
        params2.setMargins(size / 2, 0, 0, size / 2);
        mUnreadNumView.setLayoutParams(params2);
        mUnreadNumView.setTextColor(Color.WHITE);
        mUnreadNumView.setGravity(Gravity.CENTER);
        int textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        mUnreadNumView.setTextSize(textSize);
        mUnreadNumView.setVisibility(INVISIBLE);

        addView(mIcon);
        addView(mUnreadNumView);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            mIcon.setColorFilter(selectedColor);
        } else {
            mIcon.setColorFilter(unselectedColor);
        }
    }

    public void setTabPosition(int position) {
        mTabPosition = position;
        if (position == 0) {
            setSelected(true);
        }
    }

    public int getTabPosition() {
        return mTabPosition;
    }

    public void setmUnreadNumViewText(String text) {
        if (text == null || "".equals(text)) {
            mUnreadNumView.setVisibility(INVISIBLE);
        } else {
            mUnreadNumView.setVisibility(VISIBLE);
            mUnreadNumView.setText(text);
        }
    }

    public void setDragListencer(DragPointView.OnDragListencer listencer) {
        mUnreadNumView.setDragListencer(listencer);
    }
}
