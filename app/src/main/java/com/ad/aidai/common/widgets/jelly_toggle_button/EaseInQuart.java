package com.ad.aidai.common.widgets.jelly_toggle_button;

/**
 * Created by Yusxon on 17/2/13.
 */

public class EaseInQuart extends CubicBezier {
    public EaseInQuart() {
        init(0.895, 0.03, 0.685, 0.22);
    }
}
