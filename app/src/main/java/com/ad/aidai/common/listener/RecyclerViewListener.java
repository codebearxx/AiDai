package com.ad.aidai.common.listener;

import android.view.View;

/**
 * recyclerView的item点击事件
 * <p>
 * Created by Yusxon on 17/1/24.
 */
public interface RecyclerViewListener {
    interface OnRecyclerViewItemClickListener {
        /**
         * 点击item
         *
         * @param view
         * @param position
         */
        void onItemClick(View view, int position);

    }

    interface OnRecyclerViewItemLongClickListener {

        /**
         * 长按item
         * @param view
         * @param position
         */
        void onItemLongClick(View view, int position);
    }

    interface OnRecyclerViewItemSubClickListener {
        /**
         * 点击item中的某一项
         *
         * @param view
         * @param position
         */
        void onItemSubClick(View view, int position);
    }
}
