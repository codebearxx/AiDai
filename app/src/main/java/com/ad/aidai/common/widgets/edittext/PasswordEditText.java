package com.ad.aidai.common.widgets.edittext;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.ad.aidai.R;
import com.ad.aidai.common.utils.BitmapUtils;

import static android.text.InputType.TYPE_CLASS_TEXT;
import static android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;

/**
 * 密码输入框
 * <p>
 * Created by Yusxon on 16/12/16.
 */

public class PasswordEditText extends AppCompatEditText {

    /**
     * 密码默认不可见
     */
    private boolean passwordVisible = false;
    /**
     * 密码框左边图标
     */
    private Drawable pwdIcon;
    /**
     * 密码框眼睛图标
     */
    private Drawable eyeIcon;
    /**
     * 密码框清除图标
     */
    private Drawable clearIcon;
    /**
     * 显示密码
     */
    private int showPasswordIcon = -1;
    /**
     * 隐藏密码
     */
    private int hidePasswordIcon = -1;
    /**
     * 图标宽度
     */
    private int iconWidth = 0;
    /**
     * 图标高度
     */
    private int iconHeight = 0;
    /**
     * 输入类型
     */
    private int inputType = -1;
    /**
     * 是否显示左边图标
     */
    private boolean showLeftIcon=true;

    public PasswordEditText(Context context) {
        this(context, null);
    }

    public PasswordEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PasswordEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PasswordEditText);

            pwdIcon = typedArray.getDrawable(R.styleable.PasswordEditText_pet_pwdIcon);
            showPasswordIcon = typedArray.getResourceId(R.styleable.PasswordEditText_pet_eyeShowPwdIcon, -1);
            hidePasswordIcon = typedArray.getResourceId(R.styleable.PasswordEditText_pet_eyeHidePwdIcon, -1);
            clearIcon = typedArray.getDrawable(R.styleable.PasswordEditText_pet_clearIcon);
            iconWidth = typedArray.getDimensionPixelSize(R.styleable.PasswordEditText_pet_iconWidth, 0);
            iconHeight = typedArray.getDimensionPixelSize(R.styleable.PasswordEditText_pet_iconHeight, 0);

            showLeftIcon=typedArray.getBoolean(R.styleable.PasswordEditText_pet_isShowEtIcon,true);

            typedArray.recycle();
        }
        if (pwdIcon == null) {
            pwdIcon = getDrawable(R.mipmap.icon_lock);
        }
        if (showPasswordIcon == -1) {
            showPasswordIcon = R.mipmap.icon_eye_open;
        }
        if (hidePasswordIcon == -1) {
            hidePasswordIcon = R.mipmap.icon_eye_close;
        }
        if (clearIcon == null) {
            clearIcon = getDrawable(R.mipmap.icon_x);
        }
        if (iconWidth == 0) {
            iconWidth = pwdIcon.getIntrinsicWidth();
        }
        if (iconHeight == 0) {
            iconHeight = pwdIcon.getIntrinsicHeight();
        }

        Bitmap temp = BitmapUtils.drawableToBitmap(pwdIcon);
        pwdIcon = new BitmapDrawable(getResources(), BitmapUtils.zoomByWH(temp, iconWidth, iconHeight));

        //获取当前输入方式
        inputType = getInputType();
        //设置图标与文字间的距离
        setCompoundDrawablePadding(20);
        eyeIcon = getDrawable(hidePasswordIcon);
        //显示各个图标
        showIcon();

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            int x = (int) event.getX();
            if (eyeIcon != null) {
                int eyeIconWidth = eyeIcon.getBounds().width();
                if (x >= getWidth() - getPaddingRight() - eyeIconWidth && x <= getWidth() - getPaddingRight()) {
                    toggleType();
                    //显示各个图标
                    showIcon();
                    event.setAction(MotionEvent.ACTION_CANCEL);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    /**
     * 切换方式(密码可见或不可见)
     */
    private void toggleType() {
        //切换是否可见
        passwordVisible = !passwordVisible;
        //眼睛图标(打开|关闭)
        eyeIcon = passwordVisible ? getDrawable(showPasswordIcon) : getDrawable(hidePasswordIcon);
        //设置输入框输入类型(密码可见|不可见)
        setInputType(passwordVisible ? TYPE_CLASS_TEXT | TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : inputType);
        //焦点跳到文本最后
        this.setSelection(getText().length());
    }

    /**
     * 显示图标
     */
    private void showIcon() {
//        setCompoundDrawablesWithIntrinsicBounds(pwdIcon, null, eyeIcon, null);

        setCompoundDrawablesWithIntrinsicBounds((showLeftIcon?pwdIcon:null), null, eyeIcon, null);

    }

    /**
     * 根据id获取drawable
     *
     * @param id
     * @return
     */
    private Drawable getDrawable(int id) {
        return ContextCompat.getDrawable(getContext(), id);
    }
}
