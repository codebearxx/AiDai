package com.ad.aidai.common.slideback;

import android.view.MotionEvent;

import com.ad.aidai.base.BaseActivity;

/**
 * 具有右滑关闭功能的Activity
 * <p>
 * Created by Yusxon on 16/12/19.
 */

public abstract class SlideBackActivity extends BaseActivity {

    /**
     * 右滑关闭控制器
     */
    SlideBackController slideBackController;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (slideBackController == null) {
            slideBackController = new SlideBackController(this);
        }

        if (slideBackController.processEvent(event)) {
            return true;
        }

        return super.onTouchEvent(event);
    }
}
