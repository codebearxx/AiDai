package com.ad.aidai.common;

import com.ad.aidai.common.listener.OnActivityBackPressedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * fragment栈，用于fragment处理Activity的back点击事件
 * <p>
 * Created by Yusxon on 17/2/26.
 */

public class ADFragmentManager {

    /**
     * fragment的标志
     */
    private static List<String> tags = new ArrayList<>();

    /**
     * fragment的按键返回接口
     */
    private static List<OnActivityBackPressedListener> listeners = new ArrayList<>();

    public static void addListener(OnActivityBackPressedListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        listeners.add(0, listener);
    }

    public static void removeListener() {
        if (listeners != null && listeners.size() > 0) {
            listeners.remove(0);
        }
    }

    public static OnActivityBackPressedListener getTopListener() {
        if (listeners == null || listeners.size() == 0) {
            return null;
        }
        return listeners.get(0);
    }

    public static void addTag(String tag) {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        tags.add(0, tag);
//        Logger.t("ADFragmentManager").i(tag);
    }

    public static void removeTag() {
        if (tags != null && tags.size() > 0) {
            tags.remove(0);
        }
    }

    public static String getTopTag() {
        if(tags == null || tags.size() == 0) {
            return null;
        }
        return tags.get(0);
    }
}
