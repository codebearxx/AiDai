package com.ad.aidai.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 检查工具类
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public class CheckUtil {

    /**
     * 检查字符串是否为空
     *
     * @param message
     * @return
     */
    public static boolean checkStrNull(String message) {
        return message == null || message.equals("");
    }

    /**
     * 检查是否为手机号码
     *
     * @param phone 手机号码
     * @return
     */
    public static boolean checkPhone(String phone) {
        Pattern p = Pattern.compile("1[3|5|7|8|][0-9]{9}");
        Matcher m = p.matcher(phone);

        return m.matches();
    }

    /**
     * 检查是否为邮箱
     *
     * @param mail 邮箱地址
     * @return
     */
    public static boolean checkMail(String mail){
        Pattern p = Pattern.compile("\\S+@\\S+\\.\\S+");
//        Pattern p = Pattern.compile
//                ("^([a-z0-9A-Z]+[-|_|\\\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\\\.)+[a-zA-Z]{2,}$");
        Matcher m = p.matcher(mail);
        return m.matches();
    }
}
