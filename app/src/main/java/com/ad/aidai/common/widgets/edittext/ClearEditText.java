package com.ad.aidai.common.widgets.edittext;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.ad.aidai.R;
import com.ad.aidai.common.utils.BitmapUtils;

/**
 * 带清除功能的EditText
 * <p>
 * Created by Yusxon on 16/12/16.
 */

public class ClearEditText extends AppCompatEditText {

    /**
     * 密码框左边图标
     */
    private Drawable leftIcon;
    /**
     * 密码框清除图标
     */
    private Drawable clearIcon;
    /**
     * 图标宽度
     */
    private int iconWidth = 0;
    /**
     * 图标高度
     */
    private int iconHeight = 0;
    /**
     * 是否显示清除图标,默认不显示
     */
    private boolean showClearIcon = false;
    /**
     * 是否显示左边图标
     */
    private boolean showLeftIcon=true;

    public ClearEditText(Context context) {
        this(context, null);
    }

    public ClearEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ClearEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ClearEditText);

            leftIcon = typedArray.getDrawable(R.styleable.ClearEditText_cet_etIcon);
            clearIcon = typedArray.getDrawable(R.styleable.ClearEditText_cet_clearIcon);
            iconWidth = typedArray.getDimensionPixelSize(R.styleable.ClearEditText_cet_iconWidth, 0);
            iconHeight = typedArray.getDimensionPixelSize(R.styleable.ClearEditText_cet_iconHeight, 0);

            showLeftIcon=typedArray.getBoolean(R.styleable.ClearEditText_cet_isShowEtIcon,true);

            typedArray.recycle();
        }
        if (leftIcon == null) {
            leftIcon = getDrawable(R.mipmap.icon_person);
        }
        if (clearIcon == null) {
            clearIcon = getDrawable(R.mipmap.icon_x);
        }
        if (iconWidth == 0) {
            iconWidth = leftIcon.getIntrinsicWidth();
        }
        if (iconHeight == 0) {
            iconHeight = leftIcon.getIntrinsicHeight();
        }

        Bitmap temp = BitmapUtils.drawableToBitmap(leftIcon);
        leftIcon = new BitmapDrawable(getResources(), BitmapUtils.zoomByWH(temp, iconWidth, iconHeight));

        temp = BitmapUtils.drawableToBitmap(clearIcon);
        clearIcon = new BitmapDrawable(getResources(), BitmapUtils.zoomByWH(temp, iconWidth, iconHeight));

        //设置图标与文字间的距离
        setCompoundDrawablePadding(20);
        //显示图标
        showIcon();

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //如果输入框中的内容长度大于0并且清除图标没有显示,则显示出来
                if (charSequence.length() > 0 && !showClearIcon) {
                    showClearIcon = true;
                    //显示各个图标
                    showIcon();
                } else if (charSequence.length() == 0 && showClearIcon) {
                    showClearIcon = false;
                    //显示各个图标
                    showIcon();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            //如果清除图标有显示,才进行以下操作
            if (showClearIcon) {
                int x = (int) event.getX();
                if (clearIcon != null) {
                    int eyeIconWidth = clearIcon.getBounds().width();
                    if (x >= getWidth() - getPaddingRight() - eyeIconWidth && x <= getWidth() - getPaddingRight()) {
                        setText("");
                        event.setAction(MotionEvent.ACTION_CANCEL);
                    }
                }
            }
        }
        return super.onTouchEvent(event);
    }

    /**
     * 显示图标
     */
    private void showIcon() {
//        setCompoundDrawablesWithIntrinsicBounds(leftIcon, null, (showClearIcon ? clearIcon : null), null);

        setCompoundDrawablesWithIntrinsicBounds((showLeftIcon?leftIcon:null),
                null, (showClearIcon ? clearIcon : null), null);

//        Logger.t(this.getClass().getSimpleName()).i(leftIcon.getIntrinsicWidth() + "--" + leftIcon.getIntrinsicHeight
//                () + "\n" + iconWidth + "---" + iconHeight);
    }

    /**
     * 根据id获取drawable
     *
     * @param id
     * @return
     */
    private Drawable getDrawable(int id) {
        return ContextCompat.getDrawable(getContext(), id);
    }
}
