package com.ad.aidai.common.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by pure on 2016/12/19.
 */

/**
 * 获取ListView视图类
 */
public class ListViewHolder {

    private SparseArray<View> mViews;
    @SuppressWarnings("unused")
    private Context mContext;
    private View mConvertView;
    @SuppressWarnings("unused")
    private int mPosition;

    /**
     * init holder
     */
    public ListViewHolder(Context context, int layoutId, ViewGroup parent,
                          int position) {
        mConvertView = LayoutInflater.from(context).inflate(layoutId, parent,
                false);
        mViews = new SparseArray<View>();
        mPosition = position;
        mConvertView.setTag(this);
        mContext=context;
    }

    /**
     * 获取viewHolder
     */
    public static ListViewHolder getHolder(Context context, View convertView,
                                           int layoutId, ViewGroup parent, int position) {
        if (convertView == null) {
            return new ListViewHolder(context, layoutId, parent, position);
        } else {
            ListViewHolder holder = (ListViewHolder) convertView.getTag();
            holder.mPosition = position;
            return holder;
        }
    }

    public View getConvertView() {
        return mConvertView;
    }

    /**
     * get view
     */
    @SuppressWarnings("unchecked")
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * set text
     */
    public ListViewHolder setText(int viewId, String text) {
        TextView tv = getView(viewId);
        tv.setText(text);
        return this;
    }

    /**
     * set color
     */
    public ListViewHolder setColor(int viewId, int color) {
        View tv = getView(viewId);
        tv.setBackgroundColor(color);
        return this;
    }

    /**
     * set image res
     */
    public ListViewHolder setImageResource(int viewId, int resId) {
        ImageView iv = getView(viewId);
        Log.d("resid", resId + "");
        iv.setImageResource(resId);
        return this;
    }

    /**
     * set image bitmap
     */
    public ListViewHolder setImageBitmap(int viewId, Bitmap bitmap) {
        ImageView iv = getView(viewId);
        iv.setImageBitmap(bitmap);
        return this;
    }

    /**
     * set image drawable
     */
    public ListViewHolder setImageDrawable(int viewId, Drawable drawable) {
        ImageView iv = getView(viewId);
        iv.setBackground(drawable);
        return this;
    }

    /**
     * get image drawable
     */
    public ImageView getImageView(int viewId) {
        return getView(viewId);
    }

    /**
     * set image visibility
     *
     * @param viewId
     *            view的id
     * @param viewStatus
     *            <br>
     *            可见状态:<br>
     *            View.VISIBLE: 可见<br>
     *            View.GONE: 不可见<br>
     * @return
     */
    public ListViewHolder setVisibility(int viewId, int viewStatus) {
        View iv = (View) getView(viewId);
        iv.setVisibility(viewStatus);
        return this;
    }
}
