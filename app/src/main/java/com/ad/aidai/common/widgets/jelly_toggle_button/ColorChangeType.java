package com.ad.aidai.common.widgets.jelly_toggle_button;

/**
 * Created by Yusxon on 17/2/13.
 */

public enum ColorChangeType {
    RGB(0),
    HSV(1);

    int v;

    ColorChangeType(int v) {
        this.v = v;
    }
}
