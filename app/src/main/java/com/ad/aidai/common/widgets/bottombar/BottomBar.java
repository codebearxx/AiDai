package com.ad.aidai.common.widgets.bottombar;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * 底部导航栏
 * <p>
 * Created by Yusxon on 16/12/16.
 */

public class BottomBar extends LinearLayout {

    private LinearLayout mTabLayout;
    private LayoutParams mTabParams;

    private int mCurrentPosition = 0;

    private OnItemClickListener itemClickListener;

    public BottomBar(Context context) {
        this(context, null);
    }

    public BottomBar(Context context, AttributeSet attrs) {

        this(context, attrs, 0);
    }

    public BottomBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setOrientation(VERTICAL);
        mTabLayout = new LinearLayout(context);
        mTabLayout.setBackgroundColor(Color.WHITE);
        mTabLayout.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        mTabLayout.setLayoutParams(lp);
        addView(mTabLayout);

        mTabParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        mTabParams.weight = 1;
    }

    /**
     * 设置导航栏背景
     *
     * @param background
     */
    public void setBackground(int background) {
        mTabLayout.setBackground(ContextCompat.getDrawable(getContext(), background));
    }

    /**
     * 添加选项卡
     *
     * @param tab
     * @return
     */
    public BottomBar addItem(final BottomBarTab tab) {
        tab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                int position = tab.getTabPosition();

                if (mCurrentPosition != position) {
                    tab.setSelected(true);
                    mTabLayout.getChildAt(mCurrentPosition).setSelected(false);
                    mCurrentPosition = position;
                    if (itemClickListener != null) {
                        itemClickListener.Click(tab, mCurrentPosition);
                    }
                }
            }
        });

        tab.setTabPosition(mTabLayout.getChildCount());
        tab.setLayoutParams(mTabParams);
        mTabLayout.addView(tab);
        return this;
    }

    /**
     * 设置当前选中的位置
     *
     * @param position
     */
    public void setCurrentItem(final int position) {
        mTabLayout.post(new Runnable() {
            @Override
            public void run() {
                mTabLayout.getChildAt(position).performClick();
            }
        });
    }

    /**
     * 获取当前选中的位置
     *
     * @return
     */
    public int getmCurrentPosition() {
        return mCurrentPosition;
    }

    /**
     * 设置item监听
     *
     * @param itemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    /**
     * 选项卡点击事件的接口
     */
    public interface OnItemClickListener {
        void Click(BottomBarTab tab, int position);
    }
}
