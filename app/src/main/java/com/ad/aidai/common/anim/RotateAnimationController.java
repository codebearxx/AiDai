package com.ad.aidai.common.anim;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * 旋转动画控制器
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public class RotateAnimationController implements IAnimation {

    private View mView;
    private ObjectAnimator mRotateAnimator;
    private long mLastAnimatorValue;
    private long duration = 10000;

    public RotateAnimationController(View view) {
        mView = view;

        init();
    }

    private void init() {

        mRotateAnimator = ObjectAnimator.ofFloat(mView, "rotation", 0f, 360f);
        mRotateAnimator.setDuration(duration);
        mRotateAnimator.setInterpolator(new LinearInterpolator());
        mRotateAnimator.setRepeatMode(ValueAnimator.RESTART);
        mRotateAnimator.setRepeatCount(ValueAnimator.INFINITE);

    }

    public void setDuration(long duration) {
        this.duration = duration;
        mRotateAnimator.setDuration(duration);
    }

    @Override
    public void startRotateAnimation() {
        mRotateAnimator.cancel();
        mRotateAnimator.start();
    }

    @Override
    public void cancelRotateAnimation() {
        mLastAnimatorValue = 0;
        mRotateAnimator.cancel();
    }

    @Override
    public void pauseRotateAnimation() {
        mLastAnimatorValue = mRotateAnimator.getCurrentPlayTime();
        mRotateAnimator.cancel();
    }

    @Override
    public void resumeRotateAnimation() {
        mRotateAnimator.start();
        mRotateAnimator.setCurrentPlayTime(mLastAnimatorValue);
    }

    public void destroy() {
        if (mRotateAnimator != null) {
            mRotateAnimator.cancel();
            mRotateAnimator = null;
        }
    }
}
