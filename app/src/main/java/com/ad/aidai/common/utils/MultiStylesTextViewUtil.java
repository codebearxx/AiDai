package com.ad.aidai.common.utils;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

/**
 * 支持多样式的TextView
 * <p>
 * Created by Yusxon on 16/12/6.
 */

public class MultiStylesTextViewUtil {

    private Context mContext;
    private TextView mTextView;
    private SpannableStringBuilder spannableStringBuilder;

    public MultiStylesTextViewUtil(Context mContext) {
        this.mContext = mContext;
        mTextView = null;
        spannableStringBuilder = new SpannableStringBuilder();
    }

    /**
     * 创建,也就是把前面设置的有样式的字符串添加进TextView
     */
    public void create() {
        if (mTextView != null) {
            mTextView.setText(spannableStringBuilder);
            mTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    /**
     * 初始化TextView
     *
     * @param textView
     * @return
     */
    public MultiStylesTextViewUtil initTextView(TextView textView) {
        this.mTextView = textView;
        return this;
    }

    /**
     * 添加普通文字
     *
     * @param text 要添加的文字
     * @return
     */
    public MultiStylesTextViewUtil appendText(String text) {
        spannableStringBuilder.append(text);
        return this;
    }

    /**
     * 添加可点击的文字
     *
     * @param text              要添加的文字
     * @param textClickListener 点击监听事件
     * @return
     */
    public MultiStylesTextViewUtil appendText(String text, OnTextClickListener textClickListener) {
        this.appendColorText(text, -1, textClickListener);

        return this;
    }

    /**
     * 添加有颜色的文字
     *
     * @param text    要添加的文字
     * @param colorId 文字颜色
     * @return
     */
    public MultiStylesTextViewUtil appendColorText(String text, @ColorRes int colorId) {
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(ContextCompat.getColor(mContext, colorId));
        int start = spannableStringBuilder.length();
        int end = start + text.length();
        spannableStringBuilder
                .append(text)
                .setSpan(colorSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return this;
    }

    /**
     * 添加可点击的有颜色的文字
     *
     * @param text              要添加的文字
     * @param colorId           文字颜色
     * @param textClickListener 点击监听事件
     * @return
     */
    public MultiStylesTextViewUtil appendColorText(String text, @ColorRes int colorId, OnTextClickListener
            textClickListener) {
        Clickable clickable = new Clickable(mContext, text, colorId, textClickListener);
        int start = spannableStringBuilder.length();
        int end = start + text.length();
        spannableStringBuilder
                .append(text)
                .setSpan(clickable, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return this;
    }

    class Clickable extends ClickableSpan {

        private Context mContext;

        private OnTextClickListener onTextClickListener;

        @ColorRes
        private int colorId;

        private String text;

        public Clickable(Context mContext, String text, OnTextClickListener
                onTextClickListener) {
            this(mContext, text, -1, onTextClickListener);
        }

        public Clickable(Context mContext, String text, @ColorRes int colorId, OnTextClickListener
                onTextClickListener) {
            super();
            this.mContext = mContext;
            this.text = text;
            this.colorId = colorId;
            this.onTextClickListener = onTextClickListener;
        }

        @Override
        public void onClick(View view) {
            if (onTextClickListener != null) {
                onTextClickListener.textClick(view, text);
            }
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            //            super.updateDrawState(ds);
            if (this.colorId != -1) {
                ds.setColor(ContextCompat.getColor(mContext, colorId));
            }
        }
    }

    /**
     * text的点击监听接口
     */
    public interface OnTextClickListener {
        /**
         * 点击事件
         *
         * @param v    被点击的view
         * @param text 被点击的文字
         */
        void textClick(View v, String text);
    }
}
