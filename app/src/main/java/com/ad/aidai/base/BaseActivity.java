package com.ad.aidai.base;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * BaseActivity抽象类
 * 如果不要沉浸式效果则需要重写setImmersiveMode()方法,把immersiveMode设置为false,默认为true
 * <p>
 * Created by Yusxon on 16/11/26.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private CompositeSubscription mSubscriptions;
    //    public boolean immersiveMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //        setImmersiveMode();

        //        if (immersiveMode) {
        //
        //            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //                //5.0及以上
        //                View decorView = getWindow().getDecorView();
        //                int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        //                decorView.setSystemUiVisibility(option);
        //                getWindow().setStatusBarColor(Color.TRANSPARENT);
        //            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        //                //4.4到5.0
        //                WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
        //                localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS |
        // localLayoutParams
        //                        .flags);
        //
        //            }
        //        }

        initView();
        subscribeEvents();
        initData();
    }

    /**
     * 设置是否沉浸状态栏
     */
    //    protected void setImmersiveMode() {
    //        immersiveMode = true;
    //    }

    /**
     * 初始化视图
     */
    protected abstract void initView();

    /**
     * 初始化一些数据
     */
    protected abstract void initData();

    protected void addSubscription(Subscription subscription) {
        if (subscription == null) {
            return;
        }
        if (mSubscriptions == null) {
            mSubscriptions = new CompositeSubscription();
        }
        mSubscriptions.add(subscription);
    }

    protected void subscribeEvents() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSubscriptions != null) {
            mSubscriptions.clear();
        }
    }

    /**
     * 重写该方法，设置默认，解决字体大小跟随系统设置变化的问题
     *
     * @return
     */
    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }

    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(this.getClass().getSimpleName());
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(this.getClass().getSimpleName());
        MobclickAgent.onPause(this);
    }
}
