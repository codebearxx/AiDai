package com.ad.aidai.base;

/**
 * BaseView<T>
 *
 * Created by Yusxon on 16/11/26.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);

    void toast(String message);
}
