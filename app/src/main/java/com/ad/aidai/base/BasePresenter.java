package com.ad.aidai.base;

/**
 * BasePresenter
 *
 * Created by Yusxon on 16/11/26.
 */

public interface BasePresenter {

    void subscribe();

    void unSubscribe();
}
