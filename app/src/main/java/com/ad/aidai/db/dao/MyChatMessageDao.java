package com.ad.aidai.db.dao;

import com.ad.aidai.bean.ChatMessage;
import com.ad.aidai.db.DBManager;
import com.ad.aidai.db.greendao.ChatMessageDao;

import java.util.List;

/**
 * 数据库操作(聊天记录表)
 * <p>
 * Created by Yusxon on 16/12/5.
 */

public class MyChatMessageDao {

    public static MyChatMessageDao sInstance;

    private MyChatMessageDao() {

    }

    public static MyChatMessageDao getUserDao() {
        if (sInstance == null) {
            synchronized (MyChatMessageDao.class) {
                if (sInstance == null) {
                    sInstance = new MyChatMessageDao();
                }
            }
        }
        return sInstance;
    }

    /**
     * 插入一条聊天信息
     *
     * @param message
     */
    public void insert(ChatMessage message) {
        if (message != null) {
            DBManager.getDB().getDaoSession().getChatMessageDao().insert(message);
        }
    }

    /**
     * 查询所有聊天信息
     *
     * @return
     */
    public List<ChatMessage> queryAll() {
        return DBManager.getDB().getDaoSession().getChatMessageDao().loadAll();
    }

    /**
     * 根据信息id查询聊天信息
     *
     * @param messageId
     * @return
     */
    public List<ChatMessage> queryByMessageId(String messageId) {
        if (messageId != null) {
            return DBManager.getDB().getDaoSession().getChatMessageDao().queryBuilder().where(ChatMessageDao.Properties
                    .ChatMessageId.eq(messageId)).list();
        }
        return null;
    }

    /**
     * 根据聊天id查聊天信息
     *
     * @param chatId
     * @return
     */
    public ChatMessage queryById(Integer chatId) {
        if (chatId != null) {
            List<ChatMessage> list = DBManager.getDB().getDaoSession().getChatMessageDao().queryBuilder().where
                    (ChatMessageDao.Properties
                            .ChatId.eq(chatId)).list();
            if (list.size() > 0) {
                return list.get(0);
            }
        }
        return null;
    }

    /**
     * 根据信息id删除聊天信息
     *
     * @param messageId
     */
    public void deleteByMessageId(String messageId) {
        if (messageId != null) {
            List<ChatMessage> list = queryByMessageId(messageId);
            if (list != null) {
                for (ChatMessage message : list) {
                    DBManager.getDB().getDaoSession().getChatMessageDao().delete(message);
                }
            }
        }
    }

    /**
     * 根据聊天id删除聊天信息
     *
     * @param chatId
     */
    public void deleteByChatId(Integer chatId) {
        if (chatId != null) {
            ChatMessage message = queryById(chatId);
            if (message != null) {
                DBManager.getDB().getDaoSession().getChatMessageDao().delete(message);
            }
        }
    }

    /**
     * 删除聊天信息
     *
     * @param message
     */
    public void delete(ChatMessage message) {
        if (message != null) {
            DBManager.getDB().getDaoSession().getChatMessageDao().delete(message);
        }
    }

    /**
     * 删除所有聊天信息
     */
    public void deleteAll() {
        DBManager.getDB().getDaoSession().getChatMessageDao().deleteAll();
    }
}
