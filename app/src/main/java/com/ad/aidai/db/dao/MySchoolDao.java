package com.ad.aidai.db.dao;

import com.ad.aidai.bean.School;
import com.ad.aidai.db.DBManager;
import com.ad.aidai.db.greendao.SchoolDao;

import java.util.List;

import static com.ad.aidai.db.DBManager.getDB;

/**
 * 数据库操作(学校表)
 * <p>
 * Created by Yusxon on 16/12/8.
 */

public class MySchoolDao {
    private static MySchoolDao sInstance;

    private MySchoolDao() {
    }

    public static MySchoolDao getSchoolDao() {
        if (sInstance == null) {
            synchronized (MySchoolDao.class) {
                if (sInstance == null) {
                    sInstance = new MySchoolDao();
                }
            }
        }
        return sInstance;
    }

    /**
     * 插入学校信息,一条
     *
     * @param school 学校对象
     */
    public void insert(School school) {
        if (school != null) {
            getDB().getDaoSession().getSchoolDao().insert(school);
        }
    }

    /**
     * 插入学校信息(同时插入多个)
     *
     * @param schools 学校列表
     */
    public void insert(List<School> schools) {
        if (schools != null && schools.size() > 0) {
            getDB().getDaoSession().getSchoolDao().insertInTx(schools);
        }
    }

    /**
     * 根据学校名称查询学校对象(包含学校id)
     *
     * @param schoolName 学校名称
     * @return
     */
    public School query(String schoolName) {
        if (schoolName != null) {
            List<School> schools = DBManager.getDB()
                    .getDaoSession()
                    .getSchoolDao()
                    .queryBuilder()
                    .where(SchoolDao
                            .Properties.SchoolName
                            .eq(schoolName))
                    .build()
                    .list();
            if (schools != null && schools.size() > 0) {
                return schools.get(0);
            }
        }

        return null;
    }

    /**
     * 根据学校id查询学校名字
     *
     * @param schoolId 学校id
     * @return
     */
    public String query(int schoolId) {
        if (schoolId != 0) {
            List<School> schools = DBManager.getDB()
                    .getDaoSession()
                    .getSchoolDao()
                    .queryBuilder()
                    .where(SchoolDao
                            .Properties.SchoolId
                            .eq(schoolId))
                    .build()
                    .list();
            if (schools != null && schools.size() > 0) {
                return schools.get(0).getSchoolName();
            }
        }

        return null;
    }

    /**
     * 根据名称模糊查询
     *
     * @param name 名称
     * @return
     */
    public List<School> queryLike(String name) {
        if (name != null) {
            List<School> schools = DBManager.getDB()
                    .getDaoSession()
                    .getSchoolDao()
                    .queryBuilder()
                    .where(SchoolDao
                            .Properties.SchoolName
                            .like(name))
                    .build()
                    .list();
            if (schools != null && schools.size() > 0) {
                return schools;
            }
        }
        return null;
    }

    /**
     * 查询所有学校信息
     *
     * @return
     */
    public List<School> queryAll() {
        List<School> schools = DBManager.getDB()
                .getDaoSession()
                .getSchoolDao()
                .queryBuilder()
                .build()
                .list();
        if (schools != null && schools.size() > 0) {
            return schools;
        }
        return null;
    }

    /**
     * 删除表中的所有学校数据
     */
    public void deleteAll() {
        DBManager.getDB().getDaoSession().getSchoolDao().deleteAll();
    }


}
