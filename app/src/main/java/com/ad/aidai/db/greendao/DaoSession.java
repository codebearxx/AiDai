package com.ad.aidai.db.greendao;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.ad.aidai.bean.ChatMessage;
import com.ad.aidai.bean.School;
import com.ad.aidai.bean.User;

import com.ad.aidai.db.greendao.ChatMessageDao;
import com.ad.aidai.db.greendao.SchoolDao;
import com.ad.aidai.db.greendao.UserDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig chatMessageDaoConfig;
    private final DaoConfig schoolDaoConfig;
    private final DaoConfig userDaoConfig;

    private final ChatMessageDao chatMessageDao;
    private final SchoolDao schoolDao;
    private final UserDao userDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        chatMessageDaoConfig = daoConfigMap.get(ChatMessageDao.class).clone();
        chatMessageDaoConfig.initIdentityScope(type);

        schoolDaoConfig = daoConfigMap.get(SchoolDao.class).clone();
        schoolDaoConfig.initIdentityScope(type);

        userDaoConfig = daoConfigMap.get(UserDao.class).clone();
        userDaoConfig.initIdentityScope(type);

        chatMessageDao = new ChatMessageDao(chatMessageDaoConfig, this);
        schoolDao = new SchoolDao(schoolDaoConfig, this);
        userDao = new UserDao(userDaoConfig, this);

        registerDao(ChatMessage.class, chatMessageDao);
        registerDao(School.class, schoolDao);
        registerDao(User.class, userDao);
    }
    
    public void clear() {
        chatMessageDaoConfig.clearIdentityScope();
        schoolDaoConfig.clearIdentityScope();
        userDaoConfig.clearIdentityScope();
    }

    public ChatMessageDao getChatMessageDao() {
        return chatMessageDao;
    }

    public SchoolDao getSchoolDao() {
        return schoolDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

}
