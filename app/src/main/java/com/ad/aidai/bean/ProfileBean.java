package com.ad.aidai.bean;

/**
 * 个人信息显示的bean
 * <p>
 * Created by Yusxon on 17/1/19.
 */

public class ProfileBean {
    /**
     * 空行(分割线)
     */
    public static final int VIEWTYPE_SPACE = 0;
    /**
     * 头像
     */
    public static final int VIEWTYPE_HEAD = 1;
    /**
     * item项
     */
    public static final int VIEWTYPE_STRING = 2;
    /**
     * 按钮
     */
    public static final int VIEWTYPE_BUTTON = 3;
    /**
     * 类型
     */
    private int type;
    /**
     * 名
     */
    private String name;
    /**
     * 值
     */
    private Object value;

    public ProfileBean() {
        type = VIEWTYPE_SPACE;
        name = "";
        value = "";
    }

    public ProfileBean(int type, String name, Object value) {
        this.type = type;
        this.name = name;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public ProfileBean setType(int type) {
        this.type = type;
        return this;
    }

    public String getName() {
        return name;
    }

    public ProfileBean setName(String name) {
        this.name = name;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public ProfileBean setValue(Object value) {
        this.value = value;
        return this;
    }
}

