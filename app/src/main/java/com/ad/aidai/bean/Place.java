package com.ad.aidai.bean;

import com.google.gson.Gson;

/**
 * 地点信息
 *
 * Created by Yusxon on 16/12/2.
 */

public class Place {
    /**
     * 地点id
     */
    private int placeId;
    /**
     * 地点所属学校的id
     */
    private int schoolId;
    /**
     * 经度
     */
    private double placeLongitude;
    /**
     * 纬度
     */
    private double placeLatitude;
    /**
     * 地点名称
     */
    private String placeName;

    public Place(int placeId, int schoolId, double placeLongitude, double placeLatitude, String placeName) {
        this.placeId = placeId;
        this.schoolId = schoolId;
        this.placeLongitude = placeLongitude;
        this.placeLatitude = placeLatitude;
        this.placeName = placeName;
    }

    public Place(int schoolId, double placeLongitude, double placeLatitude) {
        this.schoolId = schoolId;
        this.placeLongitude = placeLongitude;
        this.placeLatitude = placeLatitude;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public double getPlaceLongitude() {
        return placeLongitude;
    }

    public void setPlaceLongitude(double placeLongitude) {
        this.placeLongitude = placeLongitude;
    }

    public double getPlaceLatitude() {
        return placeLatitude;
    }

    public void setPlaceLatitude(double placeLatitude) {
        this.placeLatitude = placeLatitude;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
