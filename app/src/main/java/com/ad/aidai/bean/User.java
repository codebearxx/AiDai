package com.ad.aidai.bean;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Property;

/**
 * 用户(学生、老师)
 * <p>
 * Created by Yusxon on 16/11/27.
 */

@Entity
public class User {
    /**
     * 用户id
     */
    @Id
    @Property(nameInDb = "user_id")
    protected Long userId;
    /**
     * 用户虚拟名称
     */
    @Property(nameInDb = "user_name")
    protected String userName;
    /**
     * 用户真实姓名
     */
    @Property(nameInDb = "user_realname")
    @SerializedName("userRealname")
    protected String userRealName;
    /**
     * 用户学号
     */
    @Property(nameInDb = "user_stu_number")
    protected String userStuNumber;
    /**
     * 学校id
     */
    @Property(nameInDb = "school_id")
    protected Long schoolId;
    /**
     * 学校名
     */
    @Property(nameInDb = "school_name")
    @SerializedName("school.schoolName")
    protected String schoolName;
    /**
     * 用户毕业时间
     */
    @Property(nameInDb = "user_gra_time")
    protected String userGraTime;
    /**
     * 用户类型
     * 0--学生
     * 1--老师
     * 3--代表已经被禁止的用户
     */
    @Property(nameInDb = "user_type")
    protected Integer userType;
    /**
     * 用户电话
     */
    @Property(nameInDb = "user_tel")
    protected String userTel;
    /**
     * 用户支付宝
     */
    @Property(nameInDb = "user_pay_num")
    protected String userPayNum;
    /**
     * 用户积分
     */
    @Property(nameInDb = "user_credits")
    protected Integer userCredits;
    /**
     * 用户宿舍号
     */
    @Property(nameInDb = "user_dorm_num")
    protected String userDormNum;
    /**
     * 用户性别
     * 0--男
     * 1--女
     */
    @Property(nameInDb = "user_sex")
    protected Integer userSex;
    /**
     * 用户年级
     */
    @Property(nameInDb = "user_grade")
    protected String userGrade;
    /**
     * 用户班级
     */
    @Property(nameInDb = "user_class")
    protected String userClass;
    /**
     * 用户个人说明
     */
    @Property(nameInDb = "user_personal_note")
    protected String userPersonalNote;
    /**
     * 用户发单次数
     */
    @Property(nameInDb = "user_send_times")
    protected Integer userSendTimes;
    /**
     * 用户接单次数
     */
    @Property(nameInDb = "user_receive_times")
    protected Integer userReceiveTimes;
    /**
     * 用户头像(头像地址)
     */
    @Property(nameInDb = "user_icon")
    protected String userIcon;
    /**
     * 用户在融云的token
     */
    @Property(nameInDb = "token")
    protected String userToken;

    @Keep
    public User(Long userId, String userName, String userRealName, String userStuNumber, Long schoolId,
                String schoolName, String userGraTime, Integer userType, String userTel, String userPayNum,
                Integer userCredits, String userDormNum, Integer userSex, String userGrade, String userClass,
                String userPersonalNote, Integer userSendTimes, Integer userReceiveTimes, String userIcon, String token) {
        this.userId = userId;
        this.userName = userName;
        this.userRealName = userRealName;
        this.userStuNumber = userStuNumber;
        this.schoolId = schoolId;
        this.schoolName = schoolName;
        this.userGraTime = userGraTime;
        this.userType = userType;
        this.userTel = userTel;
        this.userPayNum = userPayNum;
        this.userCredits = userCredits;
        this.userDormNum = userDormNum;
        this.userSex = userSex;
        this.userGrade = userGrade;
        this.userClass = userClass;
        this.userPersonalNote = userPersonalNote;
        this.userSendTimes = userSendTimes;
        this.userReceiveTimes = userReceiveTimes;
        this.userIcon = userIcon;
        this.userToken = token;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public void setUser(User user) {
        this.userId = user.getUserId();
        this.userName = user.getUserName();
        this.userRealName = user.getUserRealName();
        this.userStuNumber = user.getUserStuNumber();
        this.schoolId = user.getSchoolId();
        this.userGraTime = user.getUserGraTime();
        this.userType = user.getUserType();
        this.userTel = user.getUserTel();
        this.userPayNum = user.getUserPayNum();
        this.userCredits = user.getUserCredits();
        this.userDormNum = user.getUserDormNum();
        this.userSex = user.getUserSex();
        this.userGrade = user.getUserGrade();
        this.userClass = user.getUserClass();
        this.userPersonalNote = user.getUserPersonalNote();
        this.userSendTimes = user.getUserSendTimes();
        this.userReceiveTimes = user.getUserReceiveTimes();
        this.userIcon = user.getUserIcon();
        this.userToken = user.getToken();
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRealName() {
        return this.userRealName;
    }

    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName;
    }

    public String getUserStuNumber() {
        return this.userStuNumber;
    }

    public void setUserStuNumber(String userStuNumber) {
        this.userStuNumber = userStuNumber;
    }

    public Long getSchoolId() {
        return this.schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return this.schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getUserGraTime() {
        return this.userGraTime;
    }

    public void setUserGraTime(String userGraTime) {
        this.userGraTime = userGraTime;
    }

    public Integer getUserType() {
        return this.userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserTel() {
        return this.userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public String getUserPayNum() {
        return this.userPayNum;
    }

    public void setUserPayNum(String userPayNum) {
        this.userPayNum = userPayNum;
    }

    public Integer getUserCredits() {
        return this.userCredits;
    }

    public void setUserCredits(Integer userCredits) {
        this.userCredits = userCredits;
    }

    public String getUserDormNum() {
        return this.userDormNum;
    }

    public void setUserDormNum(String userDormNum) {
        this.userDormNum = userDormNum;
    }

    public Integer getUserSex() {
        return this.userSex;
    }

    public void setUserSex(Integer userSex) {
        this.userSex = userSex;
    }

    public String getUserGrade() {
        return this.userGrade;
    }

    public void setUserGrade(String userGrade) {
        this.userGrade = userGrade;
    }

    public String getUserClass() {
        return this.userClass;
    }

    public void setUserClass(String userClass) {
        this.userClass = userClass;
    }

    public String getUserPersonalNote() {
        return this.userPersonalNote;
    }

    public void setUserPersonalNote(String userPersonalNote) {
        this.userPersonalNote = userPersonalNote;
    }

    public Integer getUserSendTimes() {
        return this.userSendTimes;
    }

    public void setUserSendTimes(Integer userSendTimes) {
        this.userSendTimes = userSendTimes;
    }

    public Integer getUserReceiveTimes() {
        return this.userReceiveTimes;
    }

    public void setUserReceiveTimes(Integer userReceiveTimes) {
        this.userReceiveTimes = userReceiveTimes;
    }

    public String getUserIcon() {
        return this.userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getToken() {
        return userToken;
    }

    public void setToken(String token) {
        this.userToken = token;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public String getUserToken() {
        return this.userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

}
