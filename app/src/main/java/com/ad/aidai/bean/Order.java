package com.ad.aidai.bean;

import com.google.gson.Gson;

/**
 * 订单
 * <p>
 * Created by Yusxon on 16/12/2.
 */

public class Order {
    /**
     * 未被接
     */
    public final static int STATUS_NO_RECIEVE = 0;
    /**
     * 已撤销
     */
    public final static int STATUS_CANCEL = 6;
    /**
     * 待验收
     */
    public final static int STATUS_NO_ACCEPTANCE = 1;
    /**
     * 待评分
     */
    public final static int STATUS_NO_SCORE = 4;
    /**
     * 发单人未评分
     */
    public final static int STATUS_SENDER_NO_SCORE = 3;
    /**
     * 接单人未评分
     */
    public final static int STATUS_RECIEVER_NO_SCORE = 2;
    /**
     * 都完成评分
     */
    public final static int STATUS_SCORE = 5;
    /**
     * 已完成
     */
    public final static int STATUS_COMPLETE = 4;
    /**
     * 待完成
     */
    public final static int STATUS_NO_COMPLETE = 1;

    /**
     * 发单人申述
     */
    public final static int STATUS_SENDER_STATE = 7;
    /**
     * 接单人申述
     */
    public final static int STATUS_RECIEVER_STATE = 8;
    /**
     * 都完成申述
     */
    public final static int STATUS_STATE = 9;

    /**
     * 订单id
     */
    private long orderId;
    /**
     * 订单发送者id
     */
    private long orderSendId;
    /**
     * 订单接收者id
     */
    private long orderReceiveId;
    /**
     * 订单类型
     */
    private int orderType;
    /**
     * 酬劳
     */
    private float orderReward;
    /**
     * 订单信息
     */
    private String orderContent;
    /**
     * 订单生成时间
     */
    private String orderTime;
    /**
     * 交货地点
     */
    private String orderExchnPlace;
    /**
     * 推送地点id
     */
    private int placeId;
    /**
     * 订单到期时间
     */
    private String orderExpireTime;
    /**
     * 订单推送时间
     */
    private String orderSendTime;
    /**
     * 违约金
     */
    private float orderWyMoney;
    /**
     * 订单状态
     */
    private int orderStatus;
    /**
     * 订单完成时间
     */
    private String orderFinishTime;
    /**
     * 发单人信息
     */
    private User sendUser;
    /**
     * 接单人信息
     */
    private User receiveUser;
    /**
     * 地点
     */
    private Place place;


    /**
     * 发单人评分
     */
    private Comment senderComment;

    /**
     * 接单人评分
     */
    private Comment receiverComment;

    public Order() {

    }

    public Order(long orderId, int orderSendId, long orderReceiveId, int orderType, float orderReward, String
            orderContent, String orderTime, String orderExchnPlace, int placeId, String orderExpireTime, String
                         orderSendTime, float orderWyMoney, int orderStatus, String orderFinishTime) {
        this.orderId = orderId;
        this.orderSendId = orderSendId;
        this.orderReceiveId = orderReceiveId;
        this.orderType = orderType;
        this.orderReward = orderReward;
        this.orderContent = orderContent;
        this.orderTime = orderTime;
        this.orderExchnPlace = orderExchnPlace;
        this.placeId = placeId;
        this.orderExpireTime = orderExpireTime;
        this.orderSendTime = orderSendTime;
        this.orderWyMoney = orderWyMoney;
        this.orderStatus = orderStatus;
        this.orderFinishTime = orderFinishTime;
    }

    public Order(int orderSendId, int orderType, float orderReward, String orderContent, String orderTime, String
            orderExchnPlace, int placeId, String orderExpireTime, String orderSendTime, float orderWyMoney, int
                         orderStatus) {
        this.orderSendId = orderSendId;
        this.orderType = orderType;
        this.orderReward = orderReward;
        this.orderContent = orderContent;
        this.orderTime = orderTime;
        this.orderExchnPlace = orderExchnPlace;
        this.placeId = placeId;
        this.orderExpireTime = orderExpireTime;
        this.orderSendTime = orderSendTime;
        this.orderWyMoney = orderWyMoney;
        this.orderStatus = orderStatus;
    }

    public Order(long orderId, int orderSendId, long orderReceiveId, int orderType, float orderReward, String
            orderContent, String orderTime, String orderExchnPlace, int placeId, String orderExpireTime, String
                         orderSendTime, float orderWyMoney, int orderStatus, String orderFinishTime, User sendUser,
                 Place place) {
        this.orderId = orderId;
        this.orderSendId = orderSendId;
        this.orderReceiveId = orderReceiveId;
        this.orderType = orderType;
        this.orderReward = orderReward;
        this.orderContent = orderContent;
        this.orderTime = orderTime;
        this.orderExchnPlace = orderExchnPlace;
        this.placeId = placeId;
        this.orderExpireTime = orderExpireTime;
        this.orderSendTime = orderSendTime;
        this.orderWyMoney = orderWyMoney;
        this.orderStatus = orderStatus;
        this.orderFinishTime = orderFinishTime;
        this.sendUser = sendUser;
        this.place = place;
    }

    public Order(long orderId, int orderSendId, long orderReceiveId, int orderType, float orderReward, String
            orderContent, String orderTime, String orderExchnPlace, int placeId, String orderExpireTime, String
                         orderSendTime, float orderWyMoney, int orderStatus, String orderFinishTime, User sendUser, User
                         receiveUser, Place place) {
        this.orderId = orderId;
        this.orderSendId = orderSendId;
        this.orderReceiveId = orderReceiveId;
        this.orderType = orderType;
        this.orderReward = orderReward;
        this.orderContent = orderContent;
        this.orderTime = orderTime;
        this.orderExchnPlace = orderExchnPlace;
        this.placeId = placeId;
        this.orderExpireTime = orderExpireTime;
        this.orderSendTime = orderSendTime;
        this.orderWyMoney = orderWyMoney;
        this.orderStatus = orderStatus;
        this.orderFinishTime = orderFinishTime;
        this.sendUser = sendUser;
        this.receiveUser = receiveUser;
        this.place = place;
    }

    public Order(long orderId, int orderSendId, long orderReceiveId, int orderType, float orderReward, String
            orderContent, String orderTime, String orderExchnPlace, int placeId, String orderExpireTime, String
                         orderSendTime, float orderWyMoney, int orderStatus, String orderFinishTime, User sendUser, User
                         receiveUser, Place place, Comment senderComment, Comment receiverComment) {
        this.orderId = orderId;
        this.orderSendId = orderSendId;
        this.orderReceiveId = orderReceiveId;
        this.orderType = orderType;
        this.orderReward = orderReward;
        this.orderContent = orderContent;
        this.orderTime = orderTime;
        this.orderExchnPlace = orderExchnPlace;
        this.placeId = placeId;
        this.orderExpireTime = orderExpireTime;
        this.orderSendTime = orderSendTime;
        this.orderWyMoney = orderWyMoney;
        this.orderStatus = orderStatus;
        this.orderFinishTime = orderFinishTime;
        this.sendUser = sendUser;
        this.receiveUser = receiveUser;
        this.place = place;
        this.senderComment = senderComment;
        this.receiverComment = receiverComment;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getOrderSendId() {
        return orderSendId;
    }

    public void setOrderSendId(long orderSendId) {
        this.orderSendId = orderSendId;
    }

    public long getOrderReceiveId() {
        return orderReceiveId;
    }

    public void setOrderReceiveId(long orderReceiveId) {
        this.orderReceiveId = orderReceiveId;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public float getOrderReward() {
        return orderReward;
    }

    public void setOrderReward(float orderReward) {
        this.orderReward = orderReward;
    }

    public String getOrderContent() {
        return orderContent;
    }

    public void setOrderContent(String orderContent) {
        this.orderContent = orderContent;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderExchnPlace() {
        return orderExchnPlace;
    }

    public void setOrderExchnPlace(String orderExchnPlace) {
        this.orderExchnPlace = orderExchnPlace;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public String getOrderExpireTime() {
        return orderExpireTime;
    }

    public void setOrderExpireTime(String orderExpireTime) {
        this.orderExpireTime = orderExpireTime;
    }

    public String getOrderSendTime() {
        return orderSendTime;
    }

    public void setOrderSendTime(String orderSendTime) {
        this.orderSendTime = orderSendTime;
    }

    public float getOrderWyMoney() {
        return orderWyMoney;
    }

    public void setOrderWyMoney(float orderWyMoney) {
        this.orderWyMoney = orderWyMoney;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderFinishTime() {
        return orderFinishTime;
    }

    public void setOrderFinishTime(String orderFinishTime) {
        this.orderFinishTime = orderFinishTime;
    }

    public User getSendUser() {
        return sendUser;
    }

    public void setSendUser(User sendUser) {
        this.sendUser = sendUser;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public User getReceiveUser() {
        return receiveUser;
    }

    public void setReceiveUser(User receiveUser) {
        this.receiveUser = receiveUser;
    }

    public Comment getSenderComment() {
        return senderComment;
    }

    public void setSenderComment(Comment senderComment) {
        this.senderComment = senderComment;
    }

    public Comment getReceiverComment() {
        return receiverComment;
    }

    public void setReceiverComment(Comment receiverComment) {
        this.receiverComment = receiverComment;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
