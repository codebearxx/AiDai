package com.ad.aidai.bean;

import java.io.Serializable;

/**
 * 选择学校的列表的item
 * <p>
 * Created by Yusxon on 17/1/23.
 */
public class ItemSchool<T> implements Serializable {

    /**
     * 字符(A,B,C...)
     */
    public static final int ITEM_CHARACTER = 0;
    /**
     * 学校数据
     */
    public static final int ITEM_DATA = 1;
    /**
     * 没有数据
     */
    public static final int ITEM_EMPTY = 2;
    /**
     * item信息
     */
    private T itemMessage;
    /**
     * item类型
     */
    private int itemType;
    /**
     * 匹配的字符串
     */
    private String matchStr;

    public ItemSchool(int itemType, T itemMessage) {
        this.itemType = itemType;
        this.itemMessage = itemMessage;
    }

    public ItemSchool(int itemType, T itemMessage, String matchStr) {
        this.itemType = itemType;
        this.itemMessage = itemMessage;
        this.matchStr = matchStr;
    }

    public T getItemMessage() {
        return itemMessage;
    }

    public void setItemMessage(T itemMessage) {
        this.itemMessage = itemMessage;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getMatchStr() {
        return matchStr;
    }

    public void setMatchStr(String matchStr) {
        this.matchStr = matchStr;
    }
}
