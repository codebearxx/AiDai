package com.ad.aidai.bean;

import android.support.annotation.DrawableRes;

/**
 * 订单详情显示的bean
 * <p>
 * Created by Yusxon on 17/1/26.
 */

public class OrderDetailedBean {

    /**
     * 空行(分割线)
     */
    public static final int VIEWTYPE_SPACE = 0;
    /**
     * 头像
     */
    public static final int VIEWTYPE_HEAD = 1;
    /**
     * item项--单行
     */
    public static final int VIEWTYPE_ONE_LINE = 2;
    /**
     * item项--双行
     */
    public static final int VIEWTYPE_TWO_LINE = 3;
    /**
     * 按钮
     */
    public static final int VIEWTYPE_BUTTON = 4;
    /**
     * 类型
     */
    private int type;
    /**
     * 图标
     */
    @DrawableRes
    private int icon;
    /**
     * 名
     */
    private String name;
    /**
     * 值
     */
    private Object value;

    public OrderDetailedBean() {
        type = VIEWTYPE_SPACE;
        this.icon = 0;
        name = "";
        value = "";
    }

    public OrderDetailedBean(int type, int icon, String name, Object value) {
        this.type = type;
        this.icon = icon;
        this.name = name;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public OrderDetailedBean setType(int type) {
        this.type = type;

        return this;
    }

    public String getName() {
        return name;
    }

    public OrderDetailedBean setName(String name) {
        this.name = name;

        return this;
    }

    public Object getValue() {
        return value;
    }

    public OrderDetailedBean setValue(Object value) {
        this.value = value;

        return this;
    }

    public int getIcon() {
        return icon;
    }

    public OrderDetailedBean setIcon(int icon) {
        this.icon = icon;

        return this;
    }
}
