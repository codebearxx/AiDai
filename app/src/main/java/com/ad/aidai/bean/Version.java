package com.ad.aidai.bean;

import com.google.gson.Gson;

/**
 * app版本
 * <p>
 * Created by Yusxon on 17/2/23.
 */

public class Version {
    /**
     * app版本号
     */
    private String versionId;
    /**
     * 该版本更新信息
     */
    private String versionMessage;
    /**
     * 该版本apk下载地址
     */
    private String versionPath;

    public Version(String versionId, String versionMessage, String versionPath) {
        this.versionId = versionId;
        this.versionMessage = versionMessage;
        this.versionPath = versionPath;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getVersionMessage() {
        return versionMessage;
    }

    public void setVersionMessage(String versionMessage) {
        this.versionMessage = versionMessage;
    }

    public String getVersionPath() {
        return versionPath;
    }

    public void setVersionPath(String versionPath) {
        this.versionPath = versionPath;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
