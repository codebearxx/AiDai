package com.ad.aidai.bean;

/**
 * 绑定学校返回的数据类型
 * Created by pure on 2017/2/18.
 */

public class BindBean {
    /**
     * 用户年级
     */
    private String userGrade;
    /**
     * 用户学号
     */
    private String userStuNumber;
    /**
     * 用户真实姓名
     */
    private String userRealName;
    /**
     * 用户班级
     */
    private String userClass;

    public BindBean(String userGrade, String userStuNumber, String userRealName, String userClass) {
        this.userGrade = userGrade;
        this.userStuNumber = userStuNumber;
        this.userRealName = userRealName;
        this.userClass = userClass;
    }

    public String getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(String userGrade) {
        this.userGrade = userGrade;
    }

    public String getUserStuNumber() {
        return userStuNumber;
    }

    public void setUserStuNumber(String userStuNumber) {
        this.userStuNumber = userStuNumber;
    }

    public String getUserRealName() {
        return userRealName;
    }

    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName;
    }

    public String getUserClass() {
        return userClass;
    }

    public void setUserClass(String userClass) {
        this.userClass = userClass;
    }
}
