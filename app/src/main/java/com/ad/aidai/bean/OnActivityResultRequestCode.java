package com.ad.aidai.bean;

/**
 * OnActivityResultRequestCode
 * <p>
 * Created by Yusxon on 17/2/6.
 */

public class OnActivityResultRequestCode {
    /**
     * 申诉单页面的选择图片
     */
    public static final int CHOOSE_STATE_IMAGE = 1000;
    /**
     * 修改头像的选择图片
     */
    public static final int CHOOSE_HEAD_IMAGE = 1001;
}
