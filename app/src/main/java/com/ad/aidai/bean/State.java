package com.ad.aidai.bean;

import com.google.gson.Gson;

/**
 * 申述单
 * <p>
 * Created by Yusxon on 16/12/2.
 */

public class State {
    /**
     * 申述单id
     */
    private int stateId;
    /**
     * 申述人id
     */
    private int userId;
    /**
     * 申述对应的订单的id
     */
    private long orderId;
    /**
     * 申述原因
     */
    private String stateCause;
    /**
     * 申述单完成时间
     */
    private String stateFinishTime;
    /**
     * 图片(最多三张, 证据)
     */
    private String stateImage1;
    private String stateImage2;
    private String stateImage3;
    /**
     * 申述单提交时间
     */
    private String stateSubmitTime;
    /**
     * 申述单状态
     */
    private int stateStatus;
    /**
     * 申述的单
     */
    private Order order;

    public State() {

    }

    public State(int stateId, int userId, long orderId, String stateCause, String stateFinishTime, String
            stateImage1, String stateImage2, String stateImage3, String stateSubmitTime, int stateStatus) {
        this.stateId = stateId;
        this.userId = userId;
        this.orderId = orderId;
        this.stateCause = stateCause;
        this.stateFinishTime = stateFinishTime;
        this.stateImage1 = stateImage1;
        this.stateImage2 = stateImage2;
        this.stateImage3 = stateImage3;
        this.stateSubmitTime = stateSubmitTime;
        this.stateStatus = stateStatus;
    }

    /**
     * 提交申述单需要的数据, 图片路径不放进来,因为图片不确定张数
     * 图片必须用setStateImagex()方法
     *
     * @param userId          用户id
     * @param orderId         订单id
     * @param stateCause      申述原因
     * @param stateSubmitTime 申述提交时间
     */
    public State(int userId, long orderId, String stateCause, String stateSubmitTime) {
        this.userId = userId;
        this.orderId = orderId;
        this.stateCause = stateCause;
        this.stateSubmitTime = stateSubmitTime;
        this.stateStatus = 0;
    }


    public State(int stateId, int userId, long orderId, String stateCause, String stateFinishTime, String
            stateImage1, String stateImage2, String stateImage3, String stateSubmitTime, int stateStatus, Order order) {
        this.stateId = stateId;
        this.userId = userId;
        this.orderId = orderId;
        this.stateCause = stateCause;
        this.stateFinishTime = stateFinishTime;
        this.stateImage1 = stateImage1;
        this.stateImage2 = stateImage2;
        this.stateImage3 = stateImage3;
        this.stateSubmitTime = stateSubmitTime;
        this.stateStatus = stateStatus;
        this.order = order;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getStateCause() {
        return stateCause;
    }

    public void setStateCause(String stateCause) {
        this.stateCause = stateCause;
    }

    public String getStateFinishTime() {
        return stateFinishTime;
    }

    public void setStateFinishTime(String stateFinishTime) {
        this.stateFinishTime = stateFinishTime;
    }

    public String getStateImage1() {
        return stateImage1;
    }

    public void setStateImage1(String stateImage1) {
        this.stateImage1 = stateImage1;
    }

    public String getStateImage2() {
        return stateImage2;
    }

    public void setStateImage2(String stateImage2) {
        this.stateImage2 = stateImage2;
    }

    public String getStateImage3() {
        return stateImage3;
    }

    public void setStateImage3(String stateImage3) {
        this.stateImage3 = stateImage3;
    }

    public String getStateSubmitTime() {
        return stateSubmitTime;
    }

    public void setStateSubmitTime(String stateSubmitTime) {
        this.stateSubmitTime = stateSubmitTime;
    }

    public int getStateStatus() {
        return stateStatus;
    }

    public void setStateStatus(int stateStatus) {
        this.stateStatus = stateStatus;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
