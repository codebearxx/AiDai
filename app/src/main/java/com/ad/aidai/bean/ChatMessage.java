package com.ad.aidai.bean;

import com.google.gson.Gson;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Property;

/**
 * 聊天信息
 *
 * Created by Yusxon on 16/12/1.
 */

@Entity
public class ChatMessage {
    /**
     * 聊天信息id(唯一)
     */
    @Property(nameInDb = "chat_id")
    private Integer chatId;
    /**
     * 聊天信息id(不唯一,相同的两个用户的聊天信息,这个id是一样的)
     */
    @Property(nameInDb = "chat_message_id")
    private String chatMessageId;
    /**
     * 信息发送者
     */
    @Property(nameInDb = "chat_sender_id")
    private Integer chatSenderId;
    /**
     * 信息接收者
     */
    @Property(nameInDb = "chat_receiver_id")
    private Integer chatReceiverId;
    /**
     * 该信息发送时间
     */
    @Property(nameInDb = "chat_time")
    private String chatTime;
    /**
     * 聊天内容
     */
    @Property(nameInDb = "chat_content")
    private String chatContent;

    @Generated(hash = 2065720871)
    public ChatMessage(Integer chatId, String chatMessageId, Integer chatSenderId, Integer chatReceiverId,
            String chatTime, String chatContent) {
        this.chatId = chatId;
        this.chatMessageId = chatMessageId;
        this.chatSenderId = chatSenderId;
        this.chatReceiverId = chatReceiverId;
        this.chatTime = chatTime;
        this.chatContent = chatContent;
    }

    @Keep
    public ChatMessage(Integer chatSenderId, Integer chatReceiverId, String chatTime, String chatContent) {
        this.chatSenderId = chatSenderId;
        this.chatReceiverId = chatReceiverId;
        this.chatTime = chatTime;
        this.chatContent = chatContent;
        if(chatSenderId < chatReceiverId) {
            this.chatMessageId = chatSenderId + "-" + chatReceiverId;
        } else {
            this.chatMessageId = chatReceiverId + "-" + chatSenderId;
        }
    }

    @Generated(hash = 2271208)
    public ChatMessage() {
    }

    public Integer getChatId() {
        return this.chatId;
    }

    public void setChatId(Integer chatId) {
        this.chatId = chatId;
    }

    public String getChatMessageId() {
        return this.chatMessageId;
    }

    public void setChatMessageId(String chatMessageId) {
        this.chatMessageId = chatMessageId;
    }

    public Integer getChatSenderId() {
        return this.chatSenderId;
    }

    public void setChatSenderId(Integer chatSenderId) {
        this.chatSenderId = chatSenderId;
    }

    public Integer getChatReceiverId() {
        return this.chatReceiverId;
    }

    public void setChatReceiverId(Integer chatReceiverId) {
        this.chatReceiverId = chatReceiverId;
    }

    public String getChatTime() {
        return this.chatTime;
    }

    public void setChatTime(String chatTime) {
        this.chatTime = chatTime;
    }

    public String getChatContent() {
        return this.chatContent;
    }

    public void setChatContent(String chatContent) {
        this.chatContent = chatContent;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
