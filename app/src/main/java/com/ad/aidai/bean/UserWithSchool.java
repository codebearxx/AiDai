package com.ad.aidai.bean;

/**
 * 带有学校对象的user
 * <p>
 * Created by Yusxon on 17/2/21.
 */

public class UserWithSchool extends User {

    private School school;

    public UserWithSchool(Long userId, String userName, String userRealName, String userStuNumber, Long schoolId,
                          String schoolName, String userGraTime, Integer userType, String userTel, String userPayNum,
                          Integer userCredits, String userDormNum, Integer userSex, String userGrade, String
                                  userClass, String userPersonalNote, Integer userSendTimes, Integer
                                  userReceiveTimes, String userIcon, String token, School school) {
        super(userId, userName, userRealName, userStuNumber, schoolId, schoolName, userGraTime, userType, userTel,
                userPayNum, userCredits, userDormNum, userSex, userGrade, userClass, userPersonalNote, userSendTimes,
                userReceiveTimes, userIcon, token);
        this.school = school;
        setSchoolName();
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
        setSchoolName();
    }

    public void setSchoolName() {
        if (this.school != null) {
            this.schoolName = school.getSchoolName();
        }
    }
}
