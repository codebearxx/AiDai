package com.ad.aidai.bean;

import com.google.gson.Gson;

/**
 * 评论
 * <p>
 * Created by Yusxon on 16/12/2.
 */

public class Comment {
    /**
     * 评论id
     */
    private long commentId;
    /**
     * 评论发送者id
     */
    private long commentSenderId;
    /**
     * 评论对象id
     */
    private long commentReceiverId;
    /**
     * 评分
     */
    private int commentScore;
    /**
     * 评论时间
     */
    private String commentTime;
    /**
     * 订单id
     */
    private long orderId;

    /**
     * 2代表发单人评分
     * 3代表接单人评分
     */
    private int commentShow;

    public Comment(long commentId, long commentSenderId, long commentReceiverId, int commentScore,
                   String commentTime, long orderId, int commentShow) {
        this.commentId = commentId;
        this.commentSenderId = commentSenderId;
        this.commentReceiverId = commentReceiverId;
        this.commentScore = commentScore;
        this.commentTime = commentTime;
        this.orderId = orderId;
        this.commentShow = commentShow;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public long getCommentSenderId() {
        return commentSenderId;
    }

    public void setCommentSenderId(long commentSenderId) {
        this.commentSenderId = commentSenderId;
    }

    public long getCommentReceiverId() {
        return commentReceiverId;
    }

    public void setCommentReceiverId(long commentReceiverId) {
        this.commentReceiverId = commentReceiverId;
    }

    public int getCommentScore() {
        return commentScore;
    }

    public void setCommentScore(int commentScore) {
        this.commentScore = commentScore;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getCommentShow() {
        return commentShow;
    }

    public void setCommentShow(int commentShow) {
        this.commentShow = commentShow;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
