package com.ad.aidai.bean;

import com.google.gson.Gson;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Property;

/**
 * 学校
 * <p>
 * Created by Yusxon on 16/12/1.
 */
@Entity
public class School {
    /**
     * 学校id
     */
    @Property(nameInDb = "school_id")
    private Integer schoolId;
    /**
     * 学校名称
     */
    @Property(nameInDb = "school_name")
    private String schoolName;

    @Generated(hash = 1515331580)
    public School(Integer schoolId, String schoolName) {
        this.schoolId = schoolId;
        this.schoolName = schoolName;
    }

    @Generated(hash = 1579966795)
    public School() {
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
