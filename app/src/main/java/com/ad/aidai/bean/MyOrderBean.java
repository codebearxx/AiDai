package com.ad.aidai.bean;

/**
 * 我的订单的bean
 * <p>
 * Created by Yusxon on 17/2/1.
 */

public class MyOrderBean {
    /**
     * 我发的单
     */
    public static final int UITYPE_SEND = 0;
    /**
     * 我接的单
     */
    public static final int UITYPE_RECIEVED = 1;
    /**
     * 顶部
     */
    public static final int VIEWTYPE_TOP = 0;
    /**
     * 完成(有评分)
     */
    public static final int VIEWTYPE_COMPLETE = 1;
    /**
     * 通用
     */
    public static final int VIEWTYPE_COMMON = 2;
    /**
     * 类型
     */
    private int type;
    /**
     * 值
     */
    private Order value;
    /**
     * 哪个界面
     */
    private int UIType;

    public MyOrderBean() {
        type = VIEWTYPE_TOP;
        UIType = UITYPE_SEND;
        value = null;
    }

    public MyOrderBean(int type, int uiType, Order value) {
        this.type = type;
        this.UIType = uiType;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Order getValue() {
        return value;
    }

    public void setValue(Order value) {
        this.value = value;
    }

    public int getUIType() {
        return UIType;
    }

    public void setUIType(int UIType) {
        this.UIType = UIType;
    }
}
