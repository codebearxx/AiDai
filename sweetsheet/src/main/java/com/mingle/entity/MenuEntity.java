package com.mingle.entity;

import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;


/**
 * @author zzz40500
 * @version 1.0
 * @date 2015/8/5.
 * @github: https://github.com/zzz40500
 */
public class MenuEntity {


    @DrawableRes
    private int iconId;
    @ColorInt
    private int titleColor;
    private CharSequence title;
    private Drawable icon;
    private int gravity;
    private float textSize;

    public MenuEntity() {
    }

    public MenuEntity(CharSequence title) {
        this.title = title;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public int getTitleColor() {
        return titleColor;
    }

    public MenuEntity setTitleColor(int titleColor) {
        this.titleColor = titleColor;
        return this;
    }

    public CharSequence getTitle() {
        return title;
    }

    public MenuEntity setTitle(CharSequence title) {
        this.title = title;
        return this;
    }

    public Drawable getIcon() {
        return icon;
    }

    public MenuEntity setIcon(Drawable icon) {
        this.icon = icon;
        return this;
    }

    public int getGravity() {
        return gravity;
    }

    public MenuEntity setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public float getTextSize() {
        return textSize;
    }

    public MenuEntity setTextSize(float textSize) {
        this.textSize = textSize;
        return this;
    }
}
